function onStepIn(cid, item, position, fromPosition, toPosition, lastPosition, actor)
	if(getHouseByPlayerGUID(getPlayerGUID(cid))) then
		doTeleportThing(cid,getHouseEntry(getHouseByPlayerGUID(getPlayerGUID(cid))))
		doSendMagicEffect(getCreaturePosition(cid),CONST_ME_TELEPORT)
	else
		doTeleportThing(cid,{x = 486, y = 161, z = 7})
		doSendMagicEffect(getCreaturePosition(cid),CONST_ME_TELEPORT)
		doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,"You do not own any house.")
	end
	return true
end