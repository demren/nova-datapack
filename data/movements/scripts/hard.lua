-- Writeln by Dragonas
local a = 7834
function onStepIn(cid, item, pos, fromPos)
	if not isPlayer(cid) then
		return true
	end
	if getPlayerStorageValue(cid, 12399) ~= 1 then
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You can't summon anything now in here. You are not powerfull enough. Only Frank did it!")
		return true
	end
	if getGlobalStorageValue(a) > os.time() then
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You will be able to summon another Hard Zarian in "..math.ceil((getGlobalStorageValue(a) - os.time())/60).." minutes.")
		return true
	else
		local monsters = 
		{
			{2, "orshabaal", 10},
			{1, "savira", 8},
			{2, "savira", 1},
			{2, "hard zarian", 3},
			{1, "frank", 2},
			{1, "verminor", 2},
			{1, "hard zarian", 74}
		}
		local monster
		local sum=0
		for _,m in ipairs(monsters) do
			sum = sum + m[3]
		end
		local rand = math.random(1,sum)
		sum = 0
		for _,m in ipairs(monsters) do
			sum = sum + m[3]
			if(rand <=sum) then
				monster = m
				break
			end
		end
		local txt = ""
		if (monster[1] == 2) then
			txt = "Whoops. There comes two "..monster[2].."s."
		elseif(monster[2] == "hard zarian") then
			txt = "Hard Zarian was summoned!"
		else
			txt="Unfortunately "..monster[2].." came out."
		end
		for i=1, monster[1] do
			doSummonCreature(monster[2], pos)
		end
		setGlobalStorageValue(a, os.time() + 4*60*60)
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, txt)
	end
	return true
end