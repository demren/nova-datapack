function onStepIn(cid, item, position, lastPosition, fromPosition, toPosition, actor)
	if(isPlayer(cid)) then
		local sm_id = getPlayerSMsByGUID(getPlayerGUID(cid))
		if(isInArray2(sm_id, 119)) then
			setSMValueplus1(cid,119,1)
		end
		if(isInArray2(sm_id, 219)) then
			setSMValueplus1(cid,219,1)
		end
	end
	return true
end
