function onStepIn(cid, item, pos) 
	if isPlayer(cid) and getPlayerStorageValue(cid, 8743) == 1 then
		pid = getCreatureByName("Ebrulf")
		if not isNpc(pid) then
			doRemoveCreature(pid)
			pid = getCreatureByName("Ebrulf")
		end
		doCreatureSay(pid, "Hey, "..getPlayerName(cid).."! Give me my trasure back first!",1)
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Ebrulf wont let you out with his treasure. Find another way!")
		doTeleportThing(cid, {x=pos.x-1, y=pos.y, z=pos.z})
	end
return true
end