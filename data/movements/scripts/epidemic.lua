function onStepIn(cid, item, position, fromPosition)
	if(isNpc(cid)) then
		return true
	end
	if(not isPlayer(cid)) then
		return true
	end
	function epidemic(pid)
		doBroadcastMessage("Player "..getPlayerName(pid).." went into city with swamp fever! This disease is spreading really fast! Whole town is epidemic now. Someone should take care of this disease, so it wont spread any further!", MESSAGE_EVENT_ADVANCE)
		return true
	end
	if(getPlayerStorageValue(cid, 9295) > os.time() and getGlobalStorageValue(9290) < os.time()) then
		if(getGlobalStorageValue(9293) > os.time()) then
			setPlayerStorageValue(cid,9295,-1)
			doSetMonsterOutfit(cid, 'feverish citizen', 11111)
		elseif(getGlobalStorageValue(9291) > os.time()) then
			addEvent(epidemic,math.random(10000,20000),cid)
			setGlobalStorageValue(9290,os.time()+(30*60))
			setGlobalStorageValue(9293,os.time()+math.random(4*60*60,26*60*60))
		elseif(fromPosition.y > position.y) then
			doCreatureSay(getCreatureByName('Ottokar'), "Hey, "..getPlayerName(cid).."! I can\'t let you in with your fever! You are not even aware of the risks! It's a very dangerous diesease and it will spread really fast and it will poison the whole city! Loot medicine pouch from swamp trolls and cure yourself!",1)
			doTeleportThing(cid, fromPosition)
		end
	end
	return true
end



