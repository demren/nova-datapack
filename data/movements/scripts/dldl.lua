function onStepIn(cid, item, pos) 
	if not isMonster(cid) then
		return true
	end
	if string.lower(getCreatureName(cid)) == "dragon lord" then
		uid = getThingfromPos({x=pos.x+1, y=pos.y, z=pos.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid
		if isPlayer(uid) then
			if getPlayerStorageValue(uid, 8548) == 1 then
				doPlayerSendTextMessage(uid, MESSAGE_STATUS_CONSOLE_BLUE, "You already did this quest.")
				return true
			else
				doPlayerSendTextMessage(uid, MESSAGE_STATUS_CONSOLE_BLUE, "Congrats! You finished this quest!")
				doPlayerAddItem(uid, 2160, 30)
				doPlayerAddItem(uid, 7967, 1)
				doPlayerAddItem(uid, 2159, 20)
				doPlayerAddItem(uid, 2472, 1)
				setPlayerStorageValue(uid, 8548, 1)
				return true
			end	
		end
	else
		return true
	end
return true
end