local arr = {
 
{
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0},
{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0},
},
 
{
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0},
{0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0},
{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
{1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1},
{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
{0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0},
{0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0},
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}
}
 
}
 
local combat = {}
for i = 1, 3 do
        combat[i] = createCombatObject()
end
 
        setCombatArea(combat[1], createCombatArea(arr[1]))
		setCombatParam(combat[1], COMBAT_PARAM_TYPE, COMBAT_POISONDAMAGE)
		setCombatArea(combat[2], createCombatArea(arr[2]))
		setCombatParam(combat[2], COMBAT_PARAM_EFFECT, CONST_ME_POISONAREA)
 
function spellCallbackStorm(param)
	if param.count > 0 or math.random(0, 1) == 1 then
		doSendMagicEffect(param.pos, CONST_ME_HITBYPOISON)
		doSendDistanceShoot({ x = param.pos.x - 7, y = param.pos.y - 7, z = param.pos.z}, param.pos, CONST_ANI_POISON)
		addEvent(doSendMagicEffect, 50, param.pos, CONST_ME_GREEN_RINGS)
	end
 
	if(param.count < 3) then
		param.count = param.count + 1
		addEvent(spellCallbackStorm, math.random(0, 500), param)
	end
end
 
function onTargetTile(cid, pos)
	local param = {}
	param.cid = cid
	param.pos = pos
	param.count = 0
	spellCallbackStorm(param)
end
 
function onTargetTile2(cid, pos)
    doCombat(cid, combat[3], positionToVariant(pos))
end
 
function onGetFormulaValues(cid, level, maglevel)
	local min = -((level/8)+(maglevel*32))
	local max = -((level/8)+(maglevel*35))
	return min, max
end
 
setCombatCallback(combat[1], CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")
setCombatCallback(combat[1], CALLBACK_PARAM_TARGETTILE, "onTargetTile")
setCombatCallback(combat[2], CALLBACK_PARAM_TARGETTILE, "onTargetTile2")
 
function onCastSpell(cid, var)
	doCombat(cid, combat[1], var)
	doCombat(cid, combat[2], var)
	return true
end