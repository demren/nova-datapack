local combat = createCombatObject()
setCombatParam(combat, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat, COMBAT_PARAM_BLOCKARMOR, TRUE)
setCombatParam(combat, COMBAT_PARAM_EFFECT, CONST_ME_DEATHSHAKER)
setCombatParam(combat, COMBAT_PARAM_USECHARGES, TRUE)
setCombatFormula(combat, COMBAT_FORMULA_SKILL, 4.6, 3.6, 3.6, 4.6)

local area = createCombatArea(AREA_CIRCLE3X3)
setCombatArea(combat, area)

function onCastSpell(cid, var)
	return doCombat(cid, combat, var)
end