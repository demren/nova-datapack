local arr = {
 
{
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0},
{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0},
},
 
{
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0},
{0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0},
{0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0},
{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
{1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1},
{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
{0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0},
{0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0},
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0}
}
}
 
local combat = {}
for i = 1, 3 do
        combat[i] = createCombatObject()
end
 
        setCombatArea(combat[1], createCombatArea(arr[1]))
		setCombatParam(combat[1], COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
		setCombatParam(combat[1], COMBAT_PARAM_BLOCKSHIELD, false)
		setCombatParam(combat[1], COMBAT_PARAM_BLOCKARMOR, false)
		setAttackFormula(combat[1], COMBAT_FORMULA_LEVELMAGIC, 4, 4, 7, 10)
		setCombatArea(combat[2], createCombatArea(arr[2]))
		setCombatParam(combat[2], COMBAT_PARAM_EFFECT, CONST_ME_FIREAREA)
		setAttackFormula(combat[2], COMBAT_FORMULA_LEVELMAGIC, 4, 4, 4, 10)
		setCombatParam(combat[3], COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_FIRE)
		setAttackFormula(combat[3], COMBAT_FORMULA_LEVELMAGIC, 4, 4, 4, 10)
 
function spellCallbackUltimate(param)
	if param.count > 0 or math.random(0, 1) == 1 then
		doSendMagicEffect(param.pos, CONST_ME_EXPLOSIONHIT)
		addEvent(doSendMagicEffect, 50, param.pos, CONST_ME_EXPLOSIONAREA)
	end
 
	if(param.count < 3) then
		param.count = param.count + 1
		addEvent(spellCallbackUltimate, math.random(0, 500), param)
	end
end
 
function onTargetTile(cid, pos)
	local param = {}
	param.cid = cid
	param.pos = pos
	param.count = 0
	spellCallbackUltimate(param)
end
 
function onTargetTile2(cid, pos)
    doCombat(cid, combat[3], positionToVariant(pos))
end
 
function onGetFormulaValues(cid, level, maglevel)
	local min = -((level/38)+(maglevel*32))
	local max = -((level/38)+(maglevel*35))
	return min, max
end
 
-- setCombatCallback(combat[1], CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")
setCombatCallback(combat[1], CALLBACK_PARAM_TARGETTILE, "onTargetTile")
setCombatCallback(combat[2], CALLBACK_PARAM_TARGETTILE, "onTargetTile2")

 
function onCastSpell(cid, var)
	doCombat(cid, combat[1], var)
	doCombat(cid, combat[2], var)
	return true
end