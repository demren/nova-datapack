local holyDeathArea = {
	createCombatArea({
		{0, 1, 0},
		{1, 2, 1},
		{0, 1, 0}
	}),
	createCombatArea({
		{0, 1, 1, 1, 0},
		{1, 1, 0, 1, 1},
		{1, 0, 2, 0, 1},
		{1, 1, 0, 1, 1},
		{0, 1, 1, 1, 0}
	}),
	createCombatArea({
		{0, 0, 1, 1, 1, 0, 0},
		{0, 1, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 2, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 1},
		{0, 1, 0, 0, 0, 1, 0},
		{0, 0, 1, 1, 1, 0, 0}
	}),
	createCombatArea({
		{0, 0, 1, 1, 1, 1, 1, 0, 0},
		{0, 1, 1, 0, 0, 0, 1, 1, 0},
		{1, 1, 0, 0, 0, 0, 0, 1, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 0, 0, 0, 2, 0, 0, 0, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 1, 0, 0, 0, 0, 0, 1, 1},
		{0, 1, 1, 0, 0, 0, 1, 1, 0},
		{0, 0, 1, 1, 1, 1, 1, 0, 0}
	})
}
 
local holyCircleArea = {
	createCombatArea({
		{1, 2}
	}),
	createCombatArea({
		{1, 0},
		{0, 2}
	}),
	createCombatArea({
		{1},
		{2}
	}),
	createCombatArea({
		{0, 1},
		{2, 0}
	}),
	createCombatArea({
		{2, 1}
	}),
	createCombatArea({
		{2, 0},
		{0, 1}
	}),
	createCombatArea({
		{2},
		{1}
	}),
	createCombatArea({
		{0, 2},
		{1, 0}
	})
}
 
 
local holyDeath = {}
for k, area in ipairs(holyDeathArea) do
	holyDeath[k] = createCombatObject()
	setCombatParam(holyDeath[k], COMBAT_PARAM_TYPE, COMBAT_HOLYDAMAGE)
	setCombatParam(holyDeath[k], COMBAT_PARAM_EFFECT, CONST_ME_HOLYDAMAGE)
	setCombatFormula(holyDeath[k], COMBAT_FORMULA_LEVELMAGIC, -17.376, -(100 * (#holyDeathArea - k)), -17.841, -(150 * (#holyDeathArea - k)))
 
	setCombatArea(holyDeath[k], area)
 
	loadstring([[onTargetTile]] .. k .. [[ = function(cid, pos)
		doSendDistanceShoot(getCreaturePosition(cid), pos, CONST_ANI_HOLY)
	end]])()
	setCombatCallback(holyDeath[k], CALLBACK_PARAM_TARGETTILE, "onTargetTile" .. k)
end
 
local holyCircle = {}
for k, area in ipairs(holyCircleArea) do
	holyCircle[k] = createCombatObject()
	setCombatParam(holyCircle[k], COMBAT_PARAM_TYPE, COMBAT_HOLYDAMAGE)
	setCombatParam(holyCircle[k], COMBAT_PARAM_EFFECT, CONST_ME_HOLYDAMAGE)
	setCombatFormula(holyCircle[k], COMBAT_FORMULA_LEVELMAGIC, -9.466, -200, -9.841, -100)
 
	setCombatArea(holyCircle[k], area)
 
	loadstring([[onTargetTile]] .. k + #holyDeath .. [[ = function(cid, pos)
		doSendDistanceShoot(getCreaturePosition(cid), pos, CONST_ANI_HOLY)
	end]])()
	setCombatCallback(holyCircle[k], CALLBACK_PARAM_TARGETTILE, "onTargetTile" .. k + #holyDeath)
end
 
loadstring([[onTargetTile]] .. #holyDeath + #holyCircle + 1 .. [[ = function(cid, pos)
	doSendDistanceShoot(getCreaturePosition(cid), pos, CONST_ANI_SMALLHOLY)
end]])()
 
local function castSpellDelay(p)
	doCombat(unpack(p))
end
 
local stepDelay = 75
local spins = 2
function onCastSpell(cid, var)
	local delay = 0
	for i = 1, spins do
		for k, area in ipairs(holyCircle) do
			addEvent(castSpellDelay, delay, {cid, holyCircle[k], var})
			delay = delay + stepDelay
		end
	end
	for k, area in ipairs(holyDeath) do
		addEvent(castSpellDelay, delay, {cid, holyDeath[k], var})
		delay = delay + stepDelay
	end
 
	return LUA_NO_ERROR
end