function onThink(interval, lastExecution)
	doWriteLogFile("global_log.txt", "info")

msg = {
		"Tip: Recommend our server to your friends!",
		"Tip: Say !bless to buy blessings",
		"Tip: Say !commands to see full commands list",
		"Did you know ... You can upgrade your armors/weapons/shields by using upgrade crystal? You can get them from quests, lottery or from our sms shop. Each item can be upgraded 7 time, so maximum bonus for attack and defence is +14.",
		"Tip: Send us information using command !report. For example /!report i found a bug in here",
		"Did you know... Houses are cleaned after 7 days of not logging in",
		"Did you know... To fix your firewalker or soft boots just use them?",
		"Tip: visit our sms shop : "..getConfigValue('url').."?subtopic=buypoints and help us develop our server!"
}
msg[#msg+1] = msg[1]
local r = math.random(1,#msg-1)
if (getGlobalStorageValue(2354) == r) then
	r=r+1
end
setGlobalStorageValue(2354, r)
doBroadcastMessage(msg[r],MESSAGE_EVENT_ADVANCE)
return TRUE
end