function onThink(interval, lastExecution, thinkInterval)
local storage = 16000
	
	local o,t = {},{}
	for i, raid in pairs(RAIDS) do
		if getNoobsNumber() >= raid.nob_min then
			if getGlobalStorageValue(storage+i) < os.time() then
				o[i]=raid
				table.insert(t,i)
			end
		end
	end
	if #t == 0 then
		return true
	end
	local i = t[math.random(1, #t)]
	local r = o[i]
	if(r.name == 'bazir' and getGlobalStorageValue(8635) <= 0) then
		setGlobalStorageValue(8635,1)
	elseif(r.name == 'bazir') then
		return true
	end
	if(r.storage_block > 0) then
		if(getGlobalStorageValue(r.storage_block) == 1) then
			return true
		end
		setGlobalStorageValue(r.storage_block,1)
	end
	executeRaid(r.name)
	setGlobalStorageValue(storage+i, (os.time() + r.min*60 + r.hours*60*60))
	return TRUE
end