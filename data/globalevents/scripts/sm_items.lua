-- ### CONFIG ###
-- message send to player by script "type" (types you can check in "global.lua")
SHOP_MSG_TYPE = 19
-- time (in seconds) between connections to SQL database by shop script
SQL_interval = 30
-- ### END OF CONFIG ###
function onThink(interval, lastExecution)
local result_plr = db.storeQuery("SELECT * FROM sm_items WHERE `received` = '0';")
if(result_plr ~= false) then
	while(true) do
		local id = tonumber(result.getDataInt(result_plr,"id"))
		local item_id = tonumber(result.getDataInt(result_plr,"item_id"))
		local cid = getCreatureByName(getPlayerNameByGUID(tonumber(result.getDataString(result_plr,"player_id"))))
		local item_name = (result.getDataString(result_plr,"name"))
		if isPlayer(cid) == TRUE then
			if((item_id>40004)) then
				new_item = doCreateItemEx(12300, 1)
			else
				new_item = doCreateItemEx(item_id, 1)
			end
			if(item_id==40005) then
				doSetItemText(new_item, 'DS;3')
				doItemSetAttribute(new_item, "description", "Upgrade Crystal (Deadly Shield +3%)")
			elseif(item_id==40006) then
				doSetItemText(new_item, 'LL;3')
				doItemSetAttribute(new_item, "description", "Upgrade Crystal (Life Leech +3%)")
			elseif(item_id==40007) then
				doSetItemText(new_item, 'CS;3')
				doItemSetAttribute(new_item, "description", "Upgrade Crystal (Critical Strike +3%)")
			elseif(item_id==40008) then
				doSetItemText(new_item, 'EX;3')
				doItemSetAttribute(new_item, "description", "Upgrade Crystal (Bonus Experience +3%)")
			elseif(item_id==40009) then
				doSetItemText(new_item, 'DS;7')
				doItemSetAttribute(new_item, "description", "Upgrade Crystal (Deadly Shield +7%)")
			elseif(item_id==40010) then
				doSetItemText(new_item, 'LL;7')
				doItemSetAttribute(new_item, "description", "Upgrade Crystal (Life Leech +7%)")
			elseif(item_id==40011) then
				doSetItemText(new_item, 'CS;7')
				doItemSetAttribute(new_item, "description", "Upgrade Crystal (Critical Strike +7%)")
			elseif(item_id==40012) then
				doSetItemText(new_item, 'EX;7')
				doItemSetAttribute(new_item, "description", "Upgrade Crystal (Bonus Experience +7%)")
			end
			received_item = doPlayerAddItemEx(cid, new_item	)
			if received_item == RETURNVALUE_NOERROR then
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, 'You received >> '.. item_name ..' << as a Mini Tasks Reward.')
				doPlayerSave(cid, true)
				db.executeQuery("UPDATE `sm_items` SET `received`='1' WHERE id = " .. id .. ";")
			else
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, '>> '.. item_name ..' << from MINI TASKS SYSTEM is waiting for you. Please make place for this item in your backpack/hands and wait about '.. SQL_interval ..' seconds to get it.')
			end
		end
		if not(result.next(result_plr)) then
			break
		end
	end
	result.free(result_plr)
end
return TRUE
end  
