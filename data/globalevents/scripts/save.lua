function onThink(interval)
	doWriteLogFile("global_log.txt", "save")
	local function executeSave(seconds)
		doBroadcastMessage("Server is saved. Enjoy the game :)")
		doSaveServer(13)
		return true
	end
	doBroadcastMessage("Full server save within 30 seconds, please mind it may freeze!")
	addEvent(executeSave, 30000)
	return true
end
