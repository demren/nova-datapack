function onStartup()
			function u(t) if type(t)=='able' then for _, tt in pairs(t) do print(tt.x,tt.y,tt.z,tt.stackpos,"\n") end return true end return print(t.x,t.y,t.z,t.stackpos) end

	local time=os.time()
	local grounds = db.storeQuery("SELECT `id`, `id_item`, `x`, `y`, `z`,`house_id` FROM `y_houses` WHERE `ground` = 1;")
	local walls = db.storeQuery("SELECT `id`, `id_item`, `x`, `y`, `z`,`house_id` FROM `y_houses` WHERE `ground` = 0;")
	local count_grounds = db.storeQuery("SELECT count(id) FROM `y_houses` WHERE `ground` = 1;").getDataInt('count(id)')
	local count_walls = db.storeQuery("SELECT count(id) FROM `y_houses` WHERE `ground` = 0;").getDataInt('count(id)')
	if(count_grounds>0) then
		repeat
			-- print("\n"..grounds.getDataInt("id_item").." => "..grounds.getDataInt("x").." => "..grounds.getDataInt("y").." => "..grounds.getDataInt("z"))
			item = doCreateItemEx(grounds.getDataInt("id_item"), 1)
			pos = {x=grounds.getDataInt("x"),y=grounds.getDataInt("y"),z=grounds.getDataInt("z"),stackpos=0}
			if(getThingFromPos(pos).actionid > 0) then
				doItemSetAttribute(getThingFromPos(pos).uid, 'aid', 0)
			end
			if (getThingFromPos({x=pos.x,y=pos.y,z=pos.z,stackpos=1}).itemid == 1548 and isInArray(WYJSCIA,grounds.getDataInt("id_item"))) then
				doRemoveItem(getThingFromPos({x=pos.x,y=pos.y,z=pos.z,stackpos=1}).uid)
			end
			doTransformItem(getThingFromPos(pos).uid,grounds.getDataInt("id_item"))
			if(isInArray(GROUNDS,grounds.getDataInt("id_item"))) then
				if(pos.z ~= 7) then
					for i=-1,1 do
						for j=-1,1 do
							if((getThingFromPos({x=pos.x+i,y=pos.y+j,z=pos.z,stackpos=1}).itemid == 1548)
								and getTileHouseInfo({x=pos.x+i,y=pos.y+j,z=pos.z,stackpos=pos.stackpos})) then
									doRemoveItem(getThingFromPos({x=pos.x+i,y=pos.y+j,z=pos.z,stackpos=1}).uid)
							end
						end
					end
				end
			end
			doDecayItem(item)
		until not(grounds:next())
		grounds.free()
	end
	if(count_walls>0) then
		repeat
			item = doCreateItemEx(walls.getDataInt("id_item"), 1)
			pos = {x=walls.getDataInt("x"),y=walls.getDataInt("y"),z=walls.getDataInt("z"),stackpos=0}
			doTileAddItemEx(pos, item)
			doDecayItem(item)
		until not(walls:next())
		walls.free()
	end
	print(">> Nakurwianie itemow ("..count_walls+count_grounds..") zajelo "..os.time() - time.." sekund.")
	return true
end