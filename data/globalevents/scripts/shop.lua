-- ### CONFIG ###
-- message send to player by script "type" (types you can check in "global.lua")
SHOP_MSG_TYPE = 19
-- time (in seconds) between connections to SQL database by shop script
SQL_interval = 30
-- ### END OF CONFIG ###
function onThink(interval, lastExecution)

    point = 0
    point2 = 0
    point3 = 0
    local result_plr = db.query("SELECT * FROM z_ots_comunication WHERE `type` = 'login';")
    if(result_plr ~= false) then
	while(true) do
            id = tonumber(result.getDataInt(resultId,"id"))
            action = tostring(result.getDataString(resultId,"action"))
            delete = tonumber(result.getDataInt(resultId,"delete_it"))
            cid = getPlayerByName(tostring(result.getDataString(resultId,"name")))
            if isPlayer(cid) == TRUE then
                pkt = db.storeQuery("SELECT `premium_points` FROM `accounts` WHERE `id` = '"..getPlayerAccountId(cid).."';")
                pkt = pkt.getDataInt("premium_points")
                if pkt > 35000 then -- tak w razie czego
                    db.executeQuery("INSERT INTO `bans` (`type`, `value`, `param`, `active`, `expires`, `added`, `admin_id`, `comment`) VALUES ( '3', '"..getPlayerAccountId(cid).."', '"..getPlayerGUID(cid).."', 1, '".. (os.time() + (9999*24*60*60)) .. "', '"..os.time().."', '".. getPlayerGUID(cid) .."', 'Zbugowana postac. Zglos to Komenda !report');")
                    doRemoveCreature(cid)
                    return true
                end
                local itemtogive_id = tonumber(result.getDataInt(resultId,"param1"))
                point3 = db.storeQuery("SELECT `points` FROM `z_shop_offer` WHERE `itemid1` = '".. itemtogive_id .."';")
                point3 = point3.getDataInt("points")
                point2 = point3
                local itemtogive_count = tonumber(result.getDataInt(resultId,"param2"))
                local container_id = tonumber(result.getDataInt(resultId,"param3"))
                local container_count = tonumber(result.getDataInt(resultId,"param4"))
                local add_item_type = tostring(result.getDataString(resultId,"param5"))
                local add_item_name = tostring(result.getDataString(resultId,"param6"))
                local received_item = 0
                local full_weight = 0
                if add_item_type == 'container' then
                    container_weight = getItemWeightById(container_id, 1)
                    if isItemRune(itemtogive_id) == TRUE then
                        items_weight = container_count * getItemWeightById(itemtogive_id, 1)
                    else
                        items_weight = container_count * getItemWeightById(itemtogive_id, itemtogive_count)
                    end
                    full_weight = items_weight + container_weight
                else
                    full_weight = getItemWeightById(itemtogive_id, itemtogive_count)
                    if isItemRune(itemtogive_id) == TRUE then
                        full_weight = getItemWeightById(itemtogive_id, 1)
                    else
                        full_weight = getItemWeightById(itemtogive_id, itemtogive_count)
                    end
		end
		local free_cap = getPlayerFreeCap(cid)
		full_weight = full_weight and full_weight or 0
		if full_weight <= free_cap then
                    if add_item_type == 'container' then
                        local new_container = doCreateItemEx(container_id, 1)
                        local iter = 0
                        while iter ~= container_count do
                            doAddContainerItem(new_container, itemtogive_id, itemtogive_count)
                            iter = iter + 1
                        end
                        received_item = doPlayerAddItemEx(cid, new_container)
                    else
                        if( (itemtogive_id==40001)or(itemtogive_id==40002)or(itemtogive_id==40003)or(itemtogive_id==40004) ) then
                            new_item = doCreateItemEx(12300, itemtogive_count)
                        else
                            new_item = doCreateItemEx(itemtogive_id, itemtogive_count)
                        end
                        if(itemtogive_id==40001) then
                            doSetItemText(new_item, 'CS;10')
                            doItemSetAttribute(new_item, "description", "Upgrade Crystal (Critical Strike +10%)")
                        elseif(itemtogive_id==40002) then
                            doSetItemText(new_item, 'LL;10')
                            doItemSetAttribute(new_item, "description", "Upgrade Crystal (Life Leech +10%)")
                        elseif(itemtogive_id==40003) then
                            doSetItemText(new_item, 'DS;10')
                            doItemSetAttribute(new_item, "description", "Upgrade Crystal (Deadly Shield +10%)")
                        elseif(itemtogive_id==40004) then
                            doSetItemText(new_item, 'EX;10')
                            doItemSetAttribute(new_item, "description", "Upgrade Crystal (Bonus Experience +10%)")
                        end
                        received_item = doPlayerAddItemEx(cid, new_item)
                    end
                    if received_item == RETURNVALUE_NOERROR then
                        doPlayerSendTextMessage(cid, SHOP_MSG_TYPE, 'You received >> '.. add_item_name ..' << from OTS shop.')
                        doPlayerSave(cid, true)
                        db.executeQuery("DELETE FROM `z_ots_comunication` WHERE `id` = " .. id .. ";")
                        db.executeQuery("UPDATE `z_shop_history_item` SET `trans_state`='realized', `trans_real`=" .. os.time() .. " WHERE id = " .. id .. ";")
                    else
                        doPlayerSendTextMessage(cid, SHOP_MSG_TYPE, '>> '.. add_item_name ..' << from OTS shop is waiting for you. Please make place for this item in your backpack/hands and wait about '.. SQL_interval ..' seconds to get it.')
                    end
		else
                    doPlayerSendTextMessage(cid, SHOP_MSG_TYPE, '>> '.. add_item_name ..' << from OTS shop is waiting for you. It weight is '.. full_weight ..' oz., you have only '.. free_cap ..' oz. free capacity. Put some items in depot and wait about '.. SQL_interval ..' seconds to get it.')
		end
            end
            if not(result:next(result_plr)) then
		break
            end
        end
        result.free(result_plr)
    end
    return TRUE
end  
