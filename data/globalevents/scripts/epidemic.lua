function onThink(interval, lastExecution, thinkInterval)
	if(getGlobalStorageValue(9290) > os.time()) then
		executeRaid('epidemic')
		local players = getPlayersOnline()
		local players_in_swamp_area={}
		for _, pid in ipairs(players) do
			if(getPlayerStorageValue(pid, 9295) > os.time()) then
				if (isInArray({7,8,9},getPlayerPosition(pid).z)) then
					if( getPlayerPosition(pid).x > 414 and
						getPlayerPosition(pid).x < 555 and
						getPlayerPosition(pid).y > 134 and
						getPlayerPosition(pid).y < 250 ) then
							doCreatureAddHealth(pid, -(math.ceil(0.33 * getCreatureHealth(pid))))
							doSetMonsterOutfit(pid, 'feverish citizen',round(getPlayerStorageValue(pid, 9295)-os.time())*1000)
					end
				end
			end
		end
	elseif(math.random(0,100) < 33) then
		local players = getPlayersOnline()
		local players_in_swamp_area={}
		for _, pid in ipairs(players) do
			if (isInArray({7,8,9},getPlayerPosition(pid).z)) then
				if( getPlayerPosition(pid).x > 442 and
					getPlayerPosition(pid).x < 530 and
					getPlayerPosition(pid).y > 288 and
					getPlayerPosition(pid).y < 350 ) then
						players_in_swamp_area[#players_in_swamp_area+1] = pid
				end
			end
		end
		if(#players_in_swamp_area > 0) then
			local rand = math.random(1,#players_in_swamp_area)
			setPlayerStorageValue(players_in_swamp_area[rand],9295,os.time()+(30*60))
			doSetMonsterOutfit(players_in_swamp_area[rand], 'feverish citizen',30*60*1000)
		end
	end
	return true
end