function onThink(interval, lastExecution)
	local players = getPlayersOnline()
	local oczyszczone = {}
	for _, tid in ipairs(players) do
		if isNoob(tid) then
			oczyszczone[#oczyszczone+1] = tid
		end
	end
	if #oczyszczone > 0 then
		for _,cid in ipairs(oczyszczone) do
			if(getPlayerLevel(cid) > 70) then
				local blessed=true
				for i=1, 5 do
					if not getPlayerBlessing(cid, i) then
						blessed = false
					end
				end
				local aol = getPlayerSlotItem(cid,CONST_SLOT_NECKLACE)
				if(aol.uid)then
					if(aol.itemid==2173)then
						blessed=true
					end
				end
				if(blessed==false) then
					doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "Watch out! You haven't purchased blessings or aol. To do this, say !bless or if you don't have enought money, please buy aol, by command !aol")
				end
			end
			local sm_id = getPlayerSMsByGUID(getPlayerGUID(cid))
			if(isInArray2(sm_id, 115)) then
				local gold = getPlayerBalance(cid)
				setExactlyValueOfSM(cid,115,1,gold)
			end
			if(isInArray2(sm_id, 215)) then
				local gold = getPlayerBalance(cid)
				setExactlyValueOfSM(cid,215,1,gold)
			end
			if(isInArray2(sm_id, 315)) then
				local gold = getPlayerBalance(cid)
				setExactlyValueOfSM(cid,315,1,gold)
			end
			if(isInArray2(sm_id, 117)) then
				if(db.storeQuery("Select count(player_id) from market_offers WHERE player_id = '"..getPlayerGUID(cid).."';").getDataInt('count(player_id)') > 0) then
					setSMValueplus1(cid,117,1)
				end
			end
			if(isInArray2(sm_id, 217)) then
				if(db.storeQuery("Select count(player_id) from market_offers WHERE player_id = '"..getPlayerGUID(cid).."';").getDataInt('count(player_id)') > 0) then
					setExactlyValueOfSM(cid,217,1,db.storeQuery("Select count(player_id) from market_offers WHERE player_id = '"..getPlayerGUID(cid).."';").getDataInt('count(player_id)'))
				end
			end
			if(isInArray2(sm_id, 118)) then
				if(db.storeQuery("Select count(player_id) from market_history WHERE player_id = '"..getPlayerGUID(cid).."' and sale='0';").getDataInt('count(player_id)') > 0) then
					setSMValueplus1(cid,118,1)
				end
			end
			if(isInArray2(sm_id, 218)) then
				if(db.storeQuery("Select count(player_id) from market_history WHERE player_id = '"..getPlayerGUID(cid).."' and sale='0';").getDataInt('count(player_id)') > 0) then
					setExactlyValueOfSM(cid,218,1,db.storeQuery("Select count(player_id) from market_history WHERE player_id = '"..getPlayerGUID(cid).."' and sale='0';").getDataInt('count(player_id)'))
				end
			end
			if(isInArray2(sm_id, 218)) then
				if(db.storeQuery("Select count(player_id) from market_history WHERE player_id = '"..getPlayerGUID(cid).."' and sale='0';").getDataInt('count(player_id)') > 0) then
					setExactlyValueOfSM(cid,218,1,db.storeQuery("Select count(player_id) from market_history WHERE player_id = '"..getPlayerGUID(cid).."' and sale='0';").getDataInt('count(player_id)'))
				end
			end
			if(isInArray2(sm_id, 125)) then
				if(getPlayerFreeCap(cid) < 1) then
					setSMValueplus1(cid,125,1)
				end
			end
			if(isInArray2(sm_id, 225)) then
				if(getPlayerFreeCap(cid) < 1) then
					setSMValueplus1(cid,225,1)
				end
			end
			if(isInArray2(sm_id, 325)) then
				if(getPlayerFreeCap(cid) < 1) then
					setSMValueplus1(cid,325,1)
				end
			end
			if(isInArray2(sm_id, 126)) then
				if(getCreatureHealth(cid) == 1) then
					setSMValueplus1(cid,126,1)
				end
			end
			if(isInArray2(sm_id, 226)) then
				if(getCreatureHealth(cid) == 666 or getCreatureMana(cid)== 666) then
					setSMValueplus1(cid,226,1)
				end
			end
			if(isInArray2(sm_id, 326)) then
				if(getCreatureHealth(cid) == 666 or getCreatureMana(cid)== 666) then
					setSMValueplus1(cid,326,1)
				end
			end
		end
	end
	return true 
end  