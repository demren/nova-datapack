function onThink(interval, lastExecution, thinkInterval)
	local players = getPlayersOnline()
	for _, cid in ipairs(players) do
	if(getPlayerStorageValue(cid, 9993)< os.time() and getPlayerStorageValue(cid, 9993) > 0) then
			if(getPlayerMount(cid,22)) then
				doPlayerRemoveMount(cid, 22)
			end
			if(getPlayerMount(cid,25)) then
				doPlayerRemoveMount(cid,25 )
			end
			if(getPlayerMount(cid,26)) then
				doPlayerRemoveMount(cid, 26)
			end
			setPlayerStorageValue(cid, 9993,0)
			doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "Your horse contract has ended. Once you dismount, you cannot ride the rented horse anymore.")
		end
	end
	if(getGlobalStorageValue(9992) < os.time()) then
		local pos = choose({x = 449, y = 192, z = 7},{x = 392, y = 158, z = 7},{x = 367, y = 190, z = 7},{x = 500, y = 197, z = 7},{x = 422, y = 175, z = 7},{x = 431, y = 225, z = 7},{x = 381, y = 240, z = 7},{x = 539, y = 143, z = 7})
		local pos2 = choose({x = 449, y = 192, z = 7},{x = 392, y = 158, z = 7},{x = 367, y = 190, z = 7},{x = 500, y = 197, z = 7},{x = 422, y = 175, z = 7},{x = 431, y = 225, z = 7},{x = 381, y = 240, z = 7},{x = 539, y = 143, z = 7})
		doRemoveCreature(getCreatureByName('war black horse'))
		doRemoveCreature(getCreatureByName('war brown horse'))
		doCreateMonster('war black horse',pos)
		doCreateMonster('war brown horse',pos2)
		doBroadcastMessage("War horses ran away from Appaloosa.", MESSAGE_EVENT_ADVANCE)
		setGlobalStorageValue(9992,os.time()+(math.random(3,14)*60*60))
	end
	return true
end