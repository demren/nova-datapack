local storage = 1345
local raids = {
	"santa helper elf",
	"grynch clan goblin"
}

function onThink(interval, lastExecution, thinkInterval)
	local aff = 0
	if getGlobalStorageValue(storage) == -1 or getGlobalStorageValue(storage) < os.time() then
		executeRaid(raids[math.random(1, #raids)])
		local h = tonumber(os.date("%H", os.time()))
		if h > 1 and h < 7 then
			aff = 160
		else
			aff = math.random(40, 80)
		end
		setGlobalStorageValue(storage, os.time() + 1 * aff * 60)
	end
	return TRUE
end