function onThink(interval, lastExecution)
	local players = getPlayersOnline()
	if(#players == 0) then
		return true
	end
	for _, cid in pairs(players) do
		-- if god then search in all reports else serach in reports only where player_id is right
		if(getPlayerAccess(cid) > 3) then
			local reports = db.storeQuery("SELECT * FROM reports WHERE admin=0 and shown=0;")
			if(reports:getID() ~= -1) then
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,"You received a messege from player. Say !cm, to read it. If you want to respond say: !report [message].")
			end
		else
			local reports = db.storeQuery("SELECT * FROM reports WHERE `player_id` = '"..getPlayerGUID(cid).."' and admin=1 and shown=0;")
			if(reports:getID() ~= -1) then
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,"You received a messege from admin. Say !cm, to read it. If you want to respond say: !report [message].")
			end
		end
	end
	return true
end