function onThink(interval, lastExecution, thinkInterval)
	if(not isHalloween()) then
		return true
	end
	if(getNoobsNumber() < 4) then
		return true
	end
	if(math.random(1,3) == 1) then
		doCreateMonster("the halloween hare", {x=481, y=150, z=7}, false)
	end
	-- if(math.random(1,4) == 1) then
		-- doCreateMonster("the mutated pumpkin", {x=481, y=150, z=7}, false)
	-- end
	executeRaid('halloween')
	return TRUE
end