local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid)			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

function creatureSayCallback(cid, type, msg)
	if(not npcHandler:isFocused(cid)) then
		return false
	end

	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	local c = 1 -- getPlayerCountry(cid)
	if(msgcontains(msg, 'cap') or msgcontains(msg, 'serce')) then
		if c == 0 then
			selfSay('You want to exchange 5 Undead hearts for a 1 cap points?', cid)
		elseif c == 1 then
			selfSay('Chcesz wymienic wszystkie Undead Hearts na cap?({tak}/{nie}) 5 serc to 1 cap points.', cid)
		elseif c==2 then
			selfSay('Voce quer trocar 5 Undead cora�oes para um cap points?', cid)
		end
		talkState[talkUser] = 1
	elseif(msgcontains(msg, 'yes') or msgcontains(msg, 'tak') and talkState[talkUser] == 1) then
		if(getPlayerItemCount(cid, 11367) >= 5) then
			local se = math.floor(getPlayerItemCount(cid, 11367) / 5)
			if(doPlayerRemoveItem(cid, 11367, se*5) == TRUE) then
				doPlayerSave(cid, true)
				local cap = db.storeQuery("SELECT `cap` FROM `players` WHERE `id` = '" .. getPlayerGUID(cid) .. "';")
				local cap = cap.getDataInt("cap")
				doPlayerSetMaxCapacity(cid, cap + se)
                doSendMagicEffect(getPlayerPosition(cid), CONST_ME_GIFT_WRAPS)
				selfSay(ZamienText(cid, {'Thanks, I love these hearts !', 'Dzieki, uwielbiam te serca!', 'Obrigado, eu amo esses cora�oes'}), cid)
			else
				if c==0 then
					selfSay('Sorry, you don\'t have enough gold.', cid)
				elseif c==1 then
					selfSay('Niestety, nie masz wystarczaj�cej ilosci zlota.', cid)
				elseif c==2 then
					selfSay('Desculpe, voce nao tem ouro suficiente.', cid)
				end
			end
		else
			selfSay(ZamienText(cid, {'Sorry, Back later when You will have 5 Undead Hearts.', 'Niestety, p�zniej kiedy bedziesz mial 5 Undead Hearts.', 'Desculpe, volta mais tarde, quando voce ter� 5 Undead Hearts.'}), cid)
		end
		talkState[talkUser] = 0
	elseif(msgcontains(msg, 'no') or msgcontains(msg, 'nie') and isInArray({1}, talkState[talkUser]) == TRUE) then
		talkState[talkUser] = 0
		selfSay('Ok then. Cya later !', cid)
	end

	return true
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())
