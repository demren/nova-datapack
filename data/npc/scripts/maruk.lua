local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local Topic = {}
local storage = 22006

function onCreatureAppear(cid)              npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid)           npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg)  npcHandler:onCreatureSay(cid, type, msg) end
function onThink()                      npcHandler:onThink() end

local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)

shopModule:addSellableItem({'sacred stone'}, 10549, 6000, 'sacred stone')

function creatureSayCallback(cid, type, msg)
    local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
    if msgcontains(msg, "hi") or msgcontains(msg, "hello") and (not npcHandler:isFocused(cid)) then
        if getPlayerStorageValue(cid, storage) < 1 then
            selfSay("Greetings, traveller! I have a {job} for you!", cid)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("Have you brought three sacred stones for me?", cid)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            selfSay("Thank you so much!", cid)
            Topic[talkUser] = 0
        end
        npcHandler:addFocus(cid)
        return true
    end
    if(not npcHandler:isFocused(cid)) then
        return false
    end
    if msgcontains(msg, "bye") or msgcontains(msg, "farewell") then
        selfSay("Good bye.", cid)
        Topic[talkUser] = 0
        npcHandler:releaseFocus(cid)
	elseif msgcontains(msg, "quest") or msgcontains(msg, "job") or msgcontains(msg, "task") or msgcontains(msg, "help") or (msgcontains(msg, "job") and getPlayerStorageValue(cid, storage) > 0) then
        if getPlayerStorageValue(cid, storage) < 1 then
        selfSay("Can you see that trash in my yard? Those are leftovers from ancients .. Oh just for those debris and find 3 pieces for me. You can find them near entrance to hell, where you can also find Morgaroth. Take this pickaxe and go!", cid)
			doPlayerAddItem(cid, 2553, 1)
            setPlayerStorageValue(cid, storage, 1)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("Have you brought three ancient stones for me?", cid)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            selfSay("Thank you again.", cid)
            Topic[talkUser] = 0
        end
    elseif Topic[talkUser] == 1 then
        if msgcontains(msg, "yes") then
            if getPlayerItemCount(cid, 10549) >= 3 then
                doPlayerRemoveItem(cid, 10549, 3)
                selfSay("Here is your reward", cid)
                doPlayerAddItem(cid, 2160, 30)
                setPlayerStorageValue(cid, storage, 2)
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You have been rewarded with 30cc!")
            else
                selfSay("Still haven\'t found them? Keep looking!", cid)
            end
        else
            selfSay("Still haven\'t found them? Keep looking!", cid)
        end
        Topic[talkUser] = 0
    end
    return TRUE
end

npcHandler:setMessage(MESSAGE_WALKAWAY, "Good bye.")
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)