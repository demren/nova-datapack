local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end
function creatureSayCallback(cid, type, msg)
if(not npcHandler:isFocused(cid)) then
return false
end
local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
if(msgcontains(msg, 'help')) then
selfSay('You can here change items {empty great potion flask}, {empty strong potion flask}, {empty normal potion flask} for winning lottery ticket.', cid)
end

if(msgcontains(msg, 'empty great potion flask')) then
selfSay('I need a 100 empty potion flask, to give you the winning lottery ticket. ({yes})', cid)
talkState[talkUser] = 1
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 1) then
if(doPlayerRemoveItem(cid, 7635, 100) == TRUE) then
doPlayerAddItem(cid, 5958, 1)
selfSay('Here you are, this is you winning lottery ticket.', cid)
else
selfSay('Sorry, you don\'t have enough items.', cid)
end
return true
end
----------------------------------------
if(msgcontains(msg, 'empty strong potion flask')) then
selfSay('I need a 100 empty potion flask, to give you the winning lottery ticket. ({yes})', cid)
talkState[talkUser] = 2
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 2) then
if(doPlayerRemoveItem(cid, 7634, 100) == TRUE) then
doPlayerAddItem(cid, 5958, 1)
selfSay('Here you are, this is you winning lottery ticket.', cid)
else
selfSay('Sorry, you don\'t have enough items.', cid)
end
return true
end
----------------------------------------
if(msgcontains(msg, 'empty normal potion flask')) then
selfSay('I need a 100 empty potion flask, to give you the winning lottery ticket. ({yes})', cid)
talkState[talkUser] = 3
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 3) then
if(doPlayerRemoveItem(cid, 7636, 100) == TRUE) then
doPlayerAddItem(cid, 5958, 1)
selfSay('Here you are, this is you winning lottery ticket.', cid)
else
selfSay('Sorry, you don\'t have items.', cid)
end
elseif(msgcontains(msg, 'no') and isInArray({1}, talkState[talkUser]) == TRUE) then
talkState[talkUser] = 0
selfSay('Ok then.', cid)
end
return true
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new()) 