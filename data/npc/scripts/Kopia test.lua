local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local Topic = {}
local storage = 21034
local storage2 = 21035

function onCreatureAppear(cid)              npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid)           npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg)  npcHandler:onCreatureSay(cid, type, msg) end
function onThink()                      npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
    local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
    if msgcontains(msg, "hi") or msgcontains(msg, "hello") and (not npcHandler:isFocused(cid)) then
        if getPlayerStorageValue(cid, storage) < 1 then
            npcHandler:say("Please {help} me!", cid, TRUE)
			setPlayerStorageValue(cid, storage2, -1)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            npcHandler:say("Have you brought a coconut shoes for me?", cid, TRUE)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            npcHandler:say("I have another mission for you. Ok?", cid, TRUE)
			setPlayerStorageValue(cid, storage, 0)
        end
        npcHandler:addFocus(cid)
        return true
	end
	if msgcontains(msg, "quest") or msgcontains(msg, "mission") or msgcontains(msg, "task") or msgcontains(msg, "help") or (msgcontains(msg, "ok") and getPlayerStorageValue(cid, storage) == 3 and getPlayerStorageValue(cid, storage2) == 0 then
		npcHandler:say("I want you to find 100 soul orbs. Ok?", cid, TRUE)
		setPlayerStorageValue(cid, storage2, 1)
	elseif msgcontains(msg, "ok") or msgcontains(msg, "yes") and getPlayerStorageValue(cid, storage2) == 1 then
		npcHandler:say("Great! Now go!", cid, TRUE)
	elseif msgcontains(msg, "quest") or msgcontains(msg, "mission") or msgcontains(msg, "task") or msgcontains(msg, "help") and getPlayerStorageValue(cid, storage2) == 0 then
		npcHandler:say("Have you brought 100 soul orbs for me?", cid, TRUE)
        Topic[talkUser] = 1
	elseif getPlayerStorageValue(cid, storage2) == 2 then
		npcHandler:say("I appreciate your help.", cid, TRUE)
    elseif Topic[talkUser] == 1 then
        if msgcontains(msg, "yes") then
            if getPlayerItemCount(cid, 5944) >= 100 then
                doPlayerRemoveItem(cid, 5944, 100)
                npcHandler:say("Oh please take this demon helmet as a reward.", cid)
                doPlayerAddItem(cid, 2493, 1)
                setPlayerStorageValue(cid, storage2, 3)
            else
                npcHandler:say("Don\'t waste my time!", cid)
            end
        else
            npcHandler:say("Don\'t waste my time!", cid)
        end
        Topic[talkUser] = 0
    end
	
    if(not npcHandler:isFocused(cid)) then
        return false
    end
    if msgcontains(msg, "bye") or msgcontains(msg, "farewell") then
        npcHandler:say("Good bye.", cid, TRUE)
        Topic[talkUser] = 0
        npcHandler:releaseFocus(cid)
	elseif msgcontains(msg, "quest") or msgcontains(msg, "mission") or msgcontains(msg, "task") or msgcontains(msg, "help") or (msgcontains(msg, "shoes") and getPlayerStorageValue(cid, storage) > 0) then
        if getPlayerStorageValue(cid, storage) == -1 then
            npcHandler:say("I need you to bring me a coconut shoes. I need them to my experiments. You can find them somewhere in apes island. Find them for me!", cid)
            setPlayerStorageValue(cid, storage, 1)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            npcHandler:say("Have you brought a coconut shoes for me?", cid, TRUE)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            npcHandler:say("Oh hello, i\'m still working on that coconut shoes.", cid, TRUE)
            Topic[talkUser] = 0
        end
    elseif Topic[talkUser] == 1 then
        if msgcontains(msg, "yes") then
            if getPlayerItemCount(cid, 9931) >= 1 then
                doPlayerRemoveItem(cid, 9931, 1)
                npcHandler:say("Oh please take this demon shield as a reward.", cid)
                doPlayerAddItem(cid, 2520, 1)
                setPlayerStorageValue(cid, storage, 2)
				setPlayerStorageValue(cid, storage2, 0)
            else
                npcHandler:say("Still haven\'t found them?1 Keep looking in apes land.", cid)
            end
        else
            npcHandler:say("Still haven\'t found them?2 Keep looking in apes land.", cid)
        end
        Topic[talkUser] = 0
    end
    return TRUE
end

npcHandler:setMessage(MESSAGE_WALKAWAY, "Good bye.")
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)