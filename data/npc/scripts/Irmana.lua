local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end
local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)
shopModule:addSellableItem({'minotaur leather','minotaur leather'},5878, 80, 'minotaur leather')
shopModule:addSellableItem({'green dragon leather','green dragon leather'},5877, 100, 'green dragon leather')
shopModule:addSellableItem({'lizard leather','lizard leather'},5876,150,'lizard leather')
shopModule:addSellableItem({'red dragon leather','red dragon leather'},5948,200,'red dragon leather')
shopModule:addSellableItem({'brown piece of cloth','brown piece of cloth'},5913,100,'brown piece of cloth')
shopModule:addSellableItem({'yellow piece of cloth','yellow piece of cloth'},5914,150,'yellow piece of cloth')
shopModule:addSellableItem({'red piece of cloth','red piece of cloth'},5911,300,'red piece of cloth')
shopModule:addSellableItem({'blue piece of cloth','blue piece of cloth'},5912,200,'blue piece of cloth')
shopModule:addSellableItem({'green piece of cloth','green piece of cloth'},5910,200,'green piece of cloth')
shopModule:addSellableItem({'white piece of cloth','white piece of cloth'},5909,100,'white piece of cloth')
npcHandler:addModule(FocusModule:new())

