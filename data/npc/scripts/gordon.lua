local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)

shopModule:addBuyableItem({'spike sword'}, 2383, 1500, 'spike sword') --24
shopModule:addBuyableItem({'wyvern fang'}, 7408, 2500, 'wyvern fang') --32
shopModule:addBuyableItem({'assassin dagger'}, 7404, 25000, 'assassin dagger') --40

shopModule:addBuyableItem({'orcish axe'}, 2428, 1500, 'orcish axe') --23
shopModule:addBuyableItem({'beastslayer axe'}, 3962, 2500, 'beastslayer axe') --35
shopModule:addBuyableItem({'noble axe'}, 7456, 20000, 'noble axe') --39
shopModule:addBuyableItem({'clerical mace'}, 2423, 1500, 'clerical mace') --28

shopModule:addBuyableItem({'dragon hammer'}, 2434, 4000, 'dragon hammer') --32
shopModule:addBuyableItem({'sapphire hammer'}, 7437, 20000, 'sapphire hammer') --37

shopModule:addBuyableItem({'guardian shield'}, 2515, 2500, 'guardian shield') --30
shopModule:addBuyableItem({'dragon shield'}, 2516, 6000, 'dragon shield') --31

shopModule:addBuyableItem({'plate legs'}, 2647, 500, 'plate legs') --7
shopModule:addBuyableItem({'knight legs'}, 2477, 7000, 'knight legs') --8
shopModule:addBuyableItem({'crown legs'}, 2488, 18000, 'crown legs') --8

shopModule:addBuyableItem({'plate armor'}, 2463, 900, 'plate armor') --10
shopModule:addBuyableItem({'knight armor'}, 2476, 6000, 'knight armor') --12
shopModule:addBuyableItem({'crown armor'}, 2487, 15000, 'crown armor') --13
shopModule:addBuyableItem({'spirit cloak'}, 8870, 1200, 'spirit cloak')
shopModule:addBuyableItem({'spellbook of enlightenment'}, 8900, 5000, 'spellbook of enlightenment')

shopModule:addBuyableItem({'enchanted small sapphire'}, 7759, 1000,'enchanted small sapphire')
shopModule:addBuyableItem({'enchanted small emerald'}, 7761, 1000, 'enchanted small emerald')
shopModule:addBuyableItem({'enchanted small ruby'}, 7760, 1000, 'enchanted small ruby')
shopModule:addBuyableItem({'enchanted small amethyst'}, 7762, 1000,'enchanted small amethyst')


function creatureSayCallback(cid, type, msg)
	if(not npcHandler:isFocused(cid)) then
		return false
	end

	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	return true
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())
