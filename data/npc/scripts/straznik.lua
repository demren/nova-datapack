local focuses = {}
local function isFocused(cid)
	for i, v in pairs(focuses) do
		if(v == cid) then
			return true
		end
	end
	return false
end
local function addFocus(cid)
	if(not isFocused(cid)) then
		table.insert(focuses, cid)
	end
end
local function removeFocus(cid)
	for i, v in pairs(focuses) do
		if(v == cid) then
			table.remove(focuses, i)
			break
		end
	end
end
local function lookAtFocus()
	for i, v in pairs(focuses) do
		if(isPlayer(v) == TRUE) then
			doNpcSetCreatureFocus(v)
			return
		end
	end
	doNpcSetCreatureFocus(0)
end
function onThingMove(creature, thing, oldpos, oldstackpos)
end
function onCreatureAppear(creature)
end
function onCreatureDisappear(cid, pos)
  	if isFocused(cid) then
          selfSay('Narazie.')
          removeFocus(cid)
  	end
end
function onCreatureTurn(creature)
end
function onCreatureSay(cid, type, msg)
if(getDistanceTo(cid) < 5) then
	msg = string.lower(msg)
	if msg == "hi" or msg == "witaj" then
		if isFocused(cid) then
			selfSay("Przeciez juz z toba rozmawiam.", cid)
		else
			selfSay("Witaj ".. getCreatureName(cid) ..". O co chodzi?", cid)
			addFocus(cid)
			setPlayerStorageValue(cid,30001,1)
			setPlayerStorageValue(cid,30005,1)
		end
	end
	if isFocused(cid) then
		if msg == "trade" or msg == "oferta" then
			selfSay("Nie mam zadnej oferty dla ciebie. Do widzenia!", cid)
			removeFocus(cid)
		elseif msg == "bye" then
			selfSay("Bywaj!", cid)
			removeFocus(cid)
		end
	end
end
end
function onCreatureChangeOutfit(creature)
end
function onThink()
	for i, focus in pairs(focuses) do
		if(isCreature(focus) == FALSE) then
			removeFocus(focus)
		else
			local distance = getDistanceTo(focus) or -1
			if((distance > 4) or (distance == -1)) then
				selfSay("Bywaj!")
				removeFocus(focus)
			end
		end
	end
	lookAtFocus()
	if(getNpcName() == "Krolewski Straznik") then
		selfTurn(SOUTH)
	end
end