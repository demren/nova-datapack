local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

-- OTServ event handling functions start
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end
-- OTServ event handling functions end

function creatureSayCallback(cid, type, msg)
-- Place all your code in here. Remember that hi, bye and all that stuff is already handled by the npcsystem, so you do not have to take care of that yourself.
if(npcHandler.focus ~= cid) then
return false
end

-- use the real conversation? (true/false)
real = true

if real == true then
tradeMsg = 'Me can make good item from item me need. Me make uth\'kean, za\'ralator, uth\'lokr, uth\'prta and nasty lil\' bolts.'
crownMsg = 'Very noble. Shiny. Me like. But breaks so fast. Me can make from shiny armour. Lil\' one want to trade?'
devilMsg = 'Hellsteel is. Cursed and evil. Dangerous to work with. Me can make from evil helmet. Lil\' one want to trade?'
dragoMsg = 'Firy steel it is. Need green ones\' breath to melt. Or red even better. Me can make from shield. Lil\' one want to trade?'
giantMsg = 'Good iron is. Me friends use it much for fight. Me can make from weapon. Lil\' one want to trade?'
souloMsg = 'Uh. Me can make some nasty lil\' bolt from soul orbs. Lil\' one want to trade all?'
hasNoMsg = 'Lil\' one no have item I want.'
noMsg = 'Lil\' one no want item?'
else
tradeMsg = 'I can forge you royal steel from crown armors, hell steel from devil helmets, draconian steel from dragon shields, crude iron from giant swords and infernal bolts from soul orbs.'
crownMsg = 'Do you want trade a crown armor for royal steel?'
devilMsg = 'Do you want trade a devil helmet for hell steel?'
dragoMsg = 'Do you want trade a dragon shield for draconian steel?'
giantMsg = 'Do you want trade a giant sword for crude iron?'
souloMsg = 'Do you want trade a soul orb for 2 infernal bolts?'
hasNoMsg = 'Come back when you have more!'
noMsg = 'Ok than.'
end
if msgcontains(msg, 'items') or msgcontains(msg, 'item') or msgcontains(msg, 'forge') then
selfSay(tradeMsg)
elseif msgcontains(msg, 'royal steel') or msgcontains(msg, 'royal') or msgcontains(msg, 'crown') or msgcontains(msg, 'crown armor') or msgcontains(msg, 'uth\'kean') then
selfSay(crownMsg)
talk_state = 1
elseif msgcontains(msg, 'hell steel') or msgcontains(msg, 'hell') or msgcontains(msg, 'devil') or msgcontains(msg, 'devil helmet') or msgcontains(msg, 'za\'ralator') then
selfSay(devilMsg)
talk_state = 2
elseif msgcontains(msg, 'draconian steel') or msgcontains(msg, 'draconian') or msgcontains(msg, 'dragon') or msgcontains(msg, 'dragon shield') or msgcontains(msg, 'uth\'lokr') then
selfSay(dragoMsg)
talk_state = 3
elseif msgcontains(msg, 'crude iron') or msgcontains(msg, 'crude') or msgcontains(msg, 'giant') or msgcontains(msg, 'gs') or msgcontains(msg, 'giant sword') or msgcontains(msg, 'uth\'prta') then
selfSay(giantMsg)
talk_state = 4
elseif msgcontains(msg, 'infernal') or msgcontains(msg, 'infernal bolt') or msgcontains(msg, 'infernal bolts') or msgcontains(msg, 'soul') or msgcontains(msg, 'soul orb') or msgcontains(msg, 'soul orbs') or msgcontains(msg, 'nasty lil\' bolt') then
selfSay(souloMsg)
talk_state = 5
------------------------------------------------ confirm yes ------------------------------------------------
elseif msgcontains(msg, 'yes') and talk_state == 1 then
if getPlayerItemCount(cid,2487) >= 1 then
if doPlayerTakeItem(cid,2487,1) == 0 then
selfSay('Here you are.')
doPlayerAddItem(cid,5887,1)
end
else
selfSay(hasNoMsg)
end
talk_state = 0
elseif msgcontains(msg, 'yes') and talk_state == 2 then
if getPlayerItemCount(cid,2462) >= 1 then
if doPlayerTakeItem(cid,2462,1) == 0 then
selfSay('Here you are.')
doPlayerAddItem(cid,5888,1)
end
else
selfSay(hasNoMsg)
end
talk_state = 0
elseif msgcontains(msg, 'yes') and talk_state == 3 then
if getPlayerItemCount(cid,2516) >= 1 then
if doPlayerTakeItem(cid,2516,1) == 0 then
selfSay('Here you are.')
doPlayerAddItem(cid,5889,1)
end
else
selfSay(hasNoMsg)
end
talk_state = 0
elseif msgcontains(msg, 'yes') and talk_state == 4 then
if getPlayerItemCount(cid,2393) >= 1 then
if doPlayerTakeItem(cid,2393,1) == 0 then
selfSay('Here you are.')
doPlayerAddItem(cid,5892,1)
end
else
selfSay(hasNoMsg)
end
talk_state = 0
elseif msgcontains(msg, 'yes') and talk_state == 5 then
if getPlayerItemCount(cid,5944) >= 1 then
if doPlayerTakeItem(cid,5944,1) == 0 then
selfSay('Here you are.')
doPlayerAddItem(cid,6529,2)
end
else
selfSay(hasNoMsg)
end
talk_state = 0
------------------------------------------------ confirm no ------------------------------------------------
elseif msgcontains(msg, 'no') and (talk_state >= 1 and talk_state <= 5) then
selfSay(noMsg)
talk_state = 0
end
-- Place all your code in here. Remember that hi, bye and all that stuff is already handled by the npcsystem, so you do not have to take care of that yourself.
return true
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())
