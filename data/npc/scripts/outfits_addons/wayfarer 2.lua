local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local talkState = {}
local quest2pp = 44775
local quest = 17038
local monsters_ilosc = {"ironblight", 100}
local outfitId = 23
local addon = 2
local addon_name = "wayfarer"
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	if(not npcHandler:isFocused(cid)) then
		return false
	elseif msgcontains(msg, "yes") and talkState[talkUser] == 1 then
		npcHandler:say("Great! I'll reward you greatly {ok}?.", cid)
		talkState[talkUser] = 2
	elseif msgcontains(msg, "ok") and talkState[talkUser] == 2 then
		npcHandler:say("Okay, Hurry up!", cid)
		setPlayerStorageValue(cid, quest2pp, 2)
		talkState[talkUser] = 0
	elseif msgcontains(msg, "mission") then
		local str = getPlayerStorageValue(cid, quest2pp)
		if(str < 2) then
			npcHandler:say("Great, an adventurer. I need you to slay "..monsters_ilosc[2].." "..monsters_ilosc[1].." for me. Well, can you slay"..monsters_ilosc[2].." "..monsters_ilosc[1].." for me?", cid) 
			talkState[talkUser] = 1
			return true
		elseif(str == 2) then
			npcHandler:say("Please come back for a reward.", cid)
		elseif(str == 3) then
			if not canPlayerWearOutfitId(cid,outfitId,0) then
				npcHandler:say("Come back when you have a basic outfit.", cid)
			else
				npcHandler:say(monsters_ilosc[2].." "..monsters_ilosc[1]..", already? You're a true dragon slayer! As I promised, here's your reward.", cid)
				doPlayerAddOutfitId(cid, 23, addon)
				soam(cid, "outfit wayfarer "..addon)
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You have been rewarded with the "..(addon == 1 and "first" or "second").." "..addon_name.." addon!")
				setPlayerStorageValue(cid, quest2pp, 4)
			end
		elseif(str == 4) then
			if(canPlayerWearOutfitId(cid,23,0)) then
				doPlayerAddOutfitId(cid, 23, addon)
			end
			npcHandler:say("You have done enough for me, I will soon plan our attack!", cid)
		end
		talkState[talkUser] = 0
	end
	return TRUE
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())