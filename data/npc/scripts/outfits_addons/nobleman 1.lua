local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local Topic = {}
local storage = 29004
local storage2 = 29003

function onCreatureAppear(cid)              npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid)           npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg)  npcHandler:onCreatureSay(cid, type, msg) end
function onThink()                      npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
    local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
    if msgcontains(msg, "hi") or msgcontains(msg, "hello") and (not npcHandler:isFocused(cid)) then
        if getPlayerStorageValue(cid, storage) < 1 then
            selfSay("Please {help} me!",cid)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("Did you destroy 5 cocoons?",cid)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            selfSay("Thank you so much",cid)
            Topic[talkUser] = 0
        end
        npcHandler:addFocus(cid)
        return true
    end
    if(not npcHandler:isFocused(cid)) then
        return false
    end
    if msgcontains(msg, "bye") or msgcontains(msg, "farewell") then
        selfSay("Good bye.",cid)
        Topic[talkUser] = 0
        npcHandler:releaseFocus(cid)
	elseif msgcontains(msg, "quest") or msgcontains(msg, "mission") or msgcontains(msg, "task") or msgcontains(msg, "help") or (msgcontains(msg, "quest") and getPlayerStorageValue(cid, storage) > 0) then
        if getPlayerStorageValue(cid, storage) < 1 then
            selfSay("Once I held the giant spiders, but one day they become very dangerous, now I want you to killed them. Now i want you to destroy 5 of their cocoons. Now go!", cid)
			doPlayerAddItem(cid, 2386, 1)
            setPlayerStorageValue(cid, storage, 1)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("Did you destroy 5 cocoons?",cid)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            selfSay("Thank you so much again.",cid)
            Topic[talkUser] = 0
        end
    elseif Topic[talkUser] == 1 then
        if msgcontains(msg, "yes") then
            if getPlayerStorageValue(cid, 29006) >= 4 then
                selfSay("Oh, please take this as a reward.", cid)
                doPlayerAddItem(cid, 5879, 10)
				doPlayerAddItem(cid, 2160, 5)
				doPlayerAddOutfit(cid, getPlayerSex(cid) == 0 and 140 or 132, 1)
				soam(cid, "outfit nobleman 1")
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You have been rewarded with the nobleman addon!")
                setPlayerStorageValue(cid, storage, 2)
            else
                selfSay("No, you didn\'t!.", cid)
            end
        else
            selfSay("Go there and kill them, for Gods sake!", cid)
        end
        Topic[talkUser] = 0
    end
    return TRUE
end

npcHandler:setMessage(MESSAGE_WALKAWAY, "Good bye.")
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)