local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local talkState = {}
local quest = 9970
local reward = 71000
	
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	if(not npcHandler:isFocused(cid)) then
		return false
	elseif msgcontains(msg, "yes") and talkState[talkUser] == 1 then
		npcHandler:say("Awesome! I'll reward you greatly {ok}?", cid)
		talkState[talkUser] = 2
	elseif msgcontains(msg, "ok") and talkState[talkUser] == 2 then
		npcHandler:say("Okay, Hurry up!", cid)
		setPlayerStorageValue(cid, quest, 2)
		talkState[talkUser] = 0
	elseif msgcontains(msg, "help") then
		local str = getPlayerStorageValue(cid, quest)
		if(str < 2) then
			npcHandler:say("Great! Yesterday, a group of Bandits attacked me and stole my potatoes. Can you find them and take my potatoes back?", cid) 
			setPlayerStorageValue(cid, quest, 2)
			talkState[talkUser] = 1
			return true
		elseif(str == 2) then
			npcHandler:say("Please come back for a reward.", cid)
		elseif(str == 3) then
			npcHandler:say("Thanks you! As a reward I will give you chef's hat.. Oops. I can't find it...  I will give you barrel!", cid)
			if doPlayerRemoveItem(cid, 8838, 50) == TRUE then
				doPlayerAddExp(cid, 80000)
				doPlayerAddOutfit(cid, getPlayerSex(cid) == 0 and 140 or 132, 2)
				soam(cid, "outfit nobleman 2")
				setPlayerStorageValue(cid, quest, 4)
			else
			npcHandler:say("Hey! But first give back my potatoes.", cid)
			end
		elseif(str == 4) then
			npcHandler:say("Thank you so much again!", cid)
		end
		talkState[talkUser] = 0
	end
	return TRUE
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())