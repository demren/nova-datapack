local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local talkState = {}
local quest = 0 -- storage
local items_config = {
[0]={},
[1]={{2536,1}, {2492,1}, {2488,1}, {2123,1}},
[2]= {{5922,50}}
}
local outfitID = 10 -- outfit id
local addons = {1,2} -- addon number (0=outfit, 3=all)
local addon_name = "wizard"
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
	if (msgcontains(msg, "hello") or msgcontains(msg, "hi")) and not npcHandler:isFocused(cid) then
		local txt0 = isInArray(addons,0) and (' ({'..addon_name..' outfit})') or ''
		local txt1 = isInArray(addons,1) and (' ({first '..addon_name..' addon})') or ''
		local txt2 = isInArray(addons,2) and (' ({second '..addon_name..' addon})') or ''
		npcHandler:say('Greetings '..getPlayerName(cid)..'. Will you help me? If you do, I\'ll reward you with outfits! Just say'..(txt0 or '')..(txt1 or '')..(txt2 or '')..'.', cid)
		npcHandler:addFocus(cid)
		return true
	end
	if(not npcHandler:isFocused(cid)) then
		return false
	end
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	local lol = tonumber(talkState[talkUser]) == nil and 0 or tonumber(talkState[talkUser])
	if msgcontains(msg, addon_name) then
		local t=string.explode(msg,' ')
		local txt=''
		if(string.lower(t[#t]) == 'outfit' and isInArray(addons, 0)) then
			local items = items_config[0]
			for k,item in ipairs(items) do
				txt = txt .. item[2]..'x '..getItemNameById(item[1])..(k == #items and "" or ", ")
			end
			talkState[talkUser]=3
			npcHandler:say("Great! Did you bring me "..txt.." for "..addon_name.." outfit? {ok}?.", cid)
		elseif(string.lower(t[1]) == 'first' and isInArray(addons, 1)) then
			local items = items_config[1]
			for k,item in ipairs(items) do
				txt = txt .. item[2]..'x '..getItemNameById(item[1])..(k == #items and "" or ", ")
			end
			talkState[talkUser]=1
			npcHandler:say("Great! Did you bring me "..txt.." for first "..addon_name.." addon? {ok}?.", cid)
		elseif(string.lower(t[1]) == 'second' and isInArray(addons, 2)) then
			local items = items_config[2]
			for k,item in ipairs(items) do
				txt = txt .. item[2]..'x '..getItemNameById(item[1])..(k == #items and "" or ", ")
			end
			talkState[talkUser]=2
			npcHandler:say("Great! Did you bring me "..txt.." for second "..addon_name.." addon? {ok}?.", cid)
		else
			talkState[talkUser]=0
			npcHandler:say('Greetings '..getPlayerName(cid)..'. Will you help me? If you do, I\'ll reward you with outfits! Just say'..txt0..txt1..txt2..'.', cid)
		end
	elseif ((msgcontains(msg, "ok")) or msgcontains(msg, "yes")) and lol > 0 then
		lol = lol == 3 and 0 or lol
		local error = 0
		if quest ~= 0 then
			if getPlayerStorageValue(cid, quest) == 1 then
				error = 1
				npcHandler:say("You have arleady done this mission.", cid)
			end
		end
		if(lol > 0) then
			if(not(canPlayerWearOutfitId(cid,outfitID))) then
				error = 2
				npcHandler:say("You must have "..addon_name.." outfit.", cid)
			end
		end
		if(canPlayerWearOutfitId(cid,outfitID,lol)) then
			error = 1
			npcHandler:say("You have arleady done this mission.", cid)
		end
		if(error == 0) then
			local items = items_config[lol]
			local give = true
			for k, v in ipairs(items) do
				if (getPlayerItemCount(cid, v[1]) < v[2]) then
					give = false
					break
				end
			end
			if(give) then
				for k, v in ipairs(items) do
					doPlayerRemoveItem(cid, v[1], v[2])
				end
				npcHandler:say("Okay, Here you are!", cid)
				if(quest > 0) then
					setPlayerStorageValue(cid, quest, 1)
				end
				doPlayerAddOutfitId(cid, outfitID, lol)
				soam(cid, "outfit "..addon_name.." "..lol)
			else
				npcHandler:say("Please come back when you be have items.", cid)
			end
		end
		talkState[talkUser] = 0
	else
		npcHandler:say('Greetings '..getPlayerName(cid)..'. Will you help me? If you do, I\'ll reward you with outfits! Just say'..txt0..txt1..txt2..'.', cid)
		talkState[talkUser] = 0
	end
return TRUE
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)