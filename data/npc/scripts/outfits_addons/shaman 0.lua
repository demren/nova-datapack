local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)
shopModule:addSellableItem({'medicine pouch'}, 13506, 1000, 'medicine pouch') -- npc kupuje
function creatureSayCallback(cid, type, msg)
	if (msgcontains(msg, "hello") or msgcontains(msg, "hi")) and not npcHandler:isFocused(cid) then
		local sm_id = getPlayerSMsByGUID(getPlayerGUID(cid))
		if(isInArray2(sm_id, 145)) then
			setSMValueplus1(cid,145,1)
		end
		if(getGlobalStorageValue(9291) > os.time()) then
			npcHandler:say('I do costume. I\'m busy. Please come back later.', cid)
		else
			npcHandler:say('Hello '..getPlayerName(cid)..'. I\'m a shaman. My {job} is very important.', cid)
			npcHandler:addFocus(cid)
		end
		return true
	end
	if(not npcHandler:isFocused(cid)) then
		return false
	end
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	local lol = tonumber(talkState[talkUser]) == nil and 0 or tonumber(talkState[talkUser])
	if(getGlobalStorageValue(9290) < os.time()) then
		if(msgcontains(msg, 'job') or msgcontains(msg, 'work')) then
			if(getGlobalStorageValue(9291) < os.time()) then
				npcHandler:say('My job is to protect this town from swamp diseases epidemy. It\'s a tuff job, beacuse one second of inattention can start spreading a disease, which will lead to an epidemy. (...)', cid)
				npcHandler:say('I\'m also buying {Medicine Pouch}es, which i need to fight with a disease. (...)', cid)
				npcHandler:say('For every citizen, who will help me with diseases will get {Belongings of a Decease} in return of those who died because of fever. If you have suitable items you can give me a job {shaman outfit}.', cid)
				talkState[talkUser] = 0
			end
		elseif(msgcontains(msg, 'medicine pouch')) then
			npcHandler:say('Thanks to medicine pouch we can cure diseases. You can loot them from swamp trolls.', cid)npcHandler:say('Dzieki medicine pouch mozna leczyc mieszkancow miasta z bagiennej choroby. Mozesz je zdobyc z swamp trolli.', cid)
			talkState[talkUser] = 0
		elseif(msgcontains(msg,'belongings of a decease')) then
			local healed = getPlayerStorageValue(cid, 9292)
			if(healed <= 0) then
				npcHandler:say('I\'m giving Belongings of a Decease only to those who helped me with the epidemy in the city. You need to heal someone first in order to get Belongings of a Decease.', cid)
			else
				local added = getPlayerStorageValue(cid, 9296)
				added = added < 0 and 0 or added
				local to_add = healed - added
				local now_add = 0
				if(to_add > 0) then
					while(doPlayerAddItem(cid, 13670,1,false)) do
						now_add = now_add + 1
						if(now_add >= to_add) then
							break
						end
					end
					setPlayerStorageValue(cid, 9296, added + now_add)
				end
				npcHandler:say('Here, take your '..now_add..'x Belongings of a Decease.'..(now_add < to_add and ' I also would like to give you '..to_add-now_add..'x Belongings of a Decease, but you dont have enough cap, come back to me later.' or ''), cid)
			end
			talkState[talkUser]=0
		elseif(msgcontains(msg, 'yes') and lol==1) then
			if(getPlayerItemCount(cid, 13506) >= 30) then
				doPlayerRemoveItem(cid,13506,30)
				local time = 2*60*math.random(50,70)
				setPlayerStorageValue(cid, 9293, os.time()+time)
				setGlobalStorageValue(9291,os.time()+time)
				npcHandler:say('Ok, I\'ll make your Shaman outfit. Come back to me in about 2 hours.', cid)
			else
				npcHandler:say('Sorry, you don\'t have enough items.', cid)
			end
			talkState[talkUser]=0
		elseif(msgcontains(msg, 'outfit')) then
			if(not isPremium(cid)) then
				npcHandler:say('I can make you a shaman outfit in exchange for 30 {Medicine Pouch}, but it\'s available only for players with premium account.', cid)
			elseif(canPlayerWearOutfitId(cid,15) or canPlayerWearOutfitId(cid,15)) then
				npcHandler:say('You arleady have shaman outfit.', cid)
			else
				if(getPlayerStorageValue(cid,9293) == -1) then
					npcHandler:say('I can make you a shaman outfit in exchange for 30 {Medicine Pouch}. This will take me 2 hours. Should I start now? ({yes})', cid)
					talkState[talkUser]=1
				elseif(getPlayerStorageValue(cid,9293) < os.time()) then
					npcHandler:say('Here you are.', cid)
					setPlayerStorageValue(cid, 9293, -1)
					--doPlayerAddOutfit(cid,getPlayerSex(cid) == 0 and 158 or 154,0)
					doPlayerAddOutfitId(cid, 15,0) -- dodaje outfit
				soam(cid, "outfit shaman 0")
					talkState[talkUser]=0
				else
					npcHandler:say('Shaman outfit is not ready yet. Come to me after some time.', cid)
					talkState[talkUser]=0
				end
			end
		elseif(msgcontains(msg, 'no') and isInArray({1}, talkState[talkUser])) then
			npcHandler:say('Ok then.', cid)
			talkState[talkUser] = 0
		end
	end
	return true
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)