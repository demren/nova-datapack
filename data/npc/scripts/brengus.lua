local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)
shopModule:addBuyableItem({'bast skirt'}, 3983, 3100, 'bast skirt')
shopModule:addBuyableItem({'fire bug'}, 5468, 100, 'fire bug')

function creatureSayCallback(cid, type, msg)
	if (msgcontains(msg, "hello") or msgcontains(msg, "hi")) and not npcHandler:isFocused(cid) then
		npcHandler:say('Hello '..getPlayerName(cid)..'. I sell Bast Skirt and fire bugs ({trade}) and I can tame {stampor}!', cid)
		npcHandler:addFocus(cid)
		return true
	end
	if(not npcHandler:isFocused(cid)) then
		return false
	end
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	local lol = tonumber(talkState[talkUser]) == nil and 0 or tonumber(talkState[talkUser])
	if(msgcontains(msg, 'stampor')) then
		npcHandler:say('I have a tame cat with you for 30 Hollow Stampor Hoof, 50 Stampor Horn and 100 Stampor Talons? ({yes})', cid)
		talkState[talkUser] = 1
	elseif(msgcontains(msg, 'yes')) then
		if(lol == 1) then
			if(getPlayerMount(cid,11)) then
					npcHandler:say('You arleady have stampor mount.', cid)
			else
				if(getPlayerItemCount(cid, 13301) >= 30 and getPlayerItemCount(cid, 13299) >= 50 and getPlayerItemCount(cid, 13300) >= 100) then
					doPlayerRemoveItem(cid,13301,30)
					doPlayerRemoveItem(cid,13299,50)
					doPlayerRemoveItem(cid,13300,100)
					doPlayerAddMount(cid,11)
					soam(cid, "mount stampor")
					npcHandler:say('Here you are.', cid)
				else
					npcHandler:say('Sorry, you don\'t have enough items.', cid)
				end
			end
		end
		talkState[talkUser] = 0
	elseif(msgcontains(msg, 'no') and isInArray({1}, talkState[talkUser])) then
		npcHandler:say('Ok then.', cid)
		talkState[talkUser] = 0
	end
	return true
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)