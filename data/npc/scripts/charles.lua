local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

function teleport(cid) 
	doTeleportThing(cid, {x = getCreaturePosition(cid):x + 2, y = getCreaturePosition(cid):y, z = getCreaturePosition(cid):z})
end

function creatureSayCallback(cid, type, msg)
	if (msgcontains(msg, "hello") or msgcontains(msg, "hi")) and not npcHandler:isFocused(cid) then
		npcHandler:addFocus(cid)
		npcHandler:say("Welcome " .. getCreatureName(cid) .. "! Are you ready to {begin} the tutorial or maybe you want to {skip} it?",cid)
		return true
	end
	if(not npcHandler:isFocused(cid)) then
		return false
	end
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	local lol = tonumber(talkState[talkUser]) == nil and 0 or tonumber(talkState[talkUser])
	if msgcontains(msg, "skip") then
		npcHandler:say("As you wish!", cid)
		local item = Item(getPlayerSlotItem(cid, CONST_SLOT_FEET))
		addEvent(teleport, 2000, cid)
	elseif msgcontains(msg, "begin") then
		npcHandler:say("Okay, Hurry up!", cid)
		setPlayerStorageValue(cid, storage1, 2)
		talkState[talkUser] = 0
	end
	return true
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)