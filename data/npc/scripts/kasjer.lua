local cenaTalonu = 150   -- Ile GP kosztuje 1 talon
local losowanieGeno = 300   -- Co ile sekund jest losowanie Geno (tibijskiego keno)
local nagrodaTalonow = 2   -- Ile razy powieksza sie ilosc wygranych talonow przy wygranej
local maxLiczbaGeno = 5   -- Do jakiej liczby sie losuje
-- Nie rusza� niczego ni�ej.
local ostatnieLosowanie = os.time()
local liczbyGeno = {}
local uczestnicyGeno = {}
local wygraniGeno = {}
local focuses = {}
local talk_state = {}
local ileZetonow = {}
local function isFocused(cid)
	for i, v in pairs(focuses) do
		if(v == cid) then
			return true
		end
	end
	return false
end
local function addFocus(cid)
	if(not isFocused(cid)) then
		table.insert(focuses, cid)
		table.insert(talk_state, cid, 0)
	end
end
local function removeFocus(cid)
	for i, v in pairs(focuses) do
		if(v == cid) then
			table.remove(focuses, i)
			table.remove(talk_state, i)
			break
		end
	end
end
local function lookAtFocus()
	for i, v in pairs(focuses) do
		if(isPlayer(v) == TRUE) then
			doNpcSetCreatureFocus(v)
			return
		end
	end
	doNpcSetCreatureFocus(0)
end
function onCreatureAppear(cid)
end
function onCreatureDisappear(cid)
	if(isFocused(cid)) then
		selfSay("Hmph!")
		removeFocus(cid)
	end
end
function onCreatureSay(cid, type, msg)
	msg = string.lower(msg)
	if((msg == "hi") and not (isFocused(cid))) then
		selfSay("Witaj, ".. getCreatureName(cid) ..".", cid, TRUE)
		selfSay("Czy chcialbys kupic troche {talon}ow? A moze przyszedles zagrac w {Geno}? Albo chcesz wiedziec jak dziala {kasyno}?", cid)
		addFocus(cid)
	elseif(isFocused(cid)) then
		if(msg == "wares" or msg == "trade") then
			selfSay("Niestety nie moge Ci nic sprzedac. A jezeli chcesz kupic talony powiedz {talon}.", cid)
		elseif(msg == "talon") then
			if(talk_state[cid] == 0) then
				selfSay("Powiedz mi ile talonow chcesz kupic?", cid)
				talk_state[cid] = 1
			end
		elseif(isNumber(msg) == TRUE) then
			if(talk_state[cid] == 1) then
				local ile = tonumber(msg)
				if(getPlayerMoney(cid) >= ile*cenaTalonu) then
					while(ile > 100) do
						doPlayerAddItem(cid,2151,100)
						ile = ile - 100
					end
					if(ile > 0) then
						doPlayerAddItem(cid,2151,ile)
						ile = 0
					end
					doPlayerRemoveMoney(cid,tonumber(msg)*cenaTalonu)
					selfSay("Oto twoje ".. msg .." talonow. Zycze milej gry.", cid)
				else
					selfSay("Nie masz tyle pieniedzy aby kupic ".. ile .." talonow. Talon kosztuje ".. cenaTalonu ..".", cid)
				end
				talk_state[cid] = 0
			elseif(talk_state[cid] == 2) then
				ileZetonow[cid] = tonumber(msg)
				selfSay("Teraz powiedz ile zetonow chcesz przeznaczyc na to losowanie Geno?", cid)
				talk_state[cid] = 3
			elseif(talk_state[cid] == 3) then
				talk_state[cid] = 0
				local ile = tonumber(msg)
				if(getPlayerItemCount(cid,2151) >= ile) then
					doPlayerRemoveItem(cid,2151,ile)
					uczestnicyGeno[cid] = {liczba = ileZetonow[cid], talony = ile}
					selfSay("Dodalem Cie do listy uczestnikow.", cid)
				else
					selfSay("Nie masz tylu zetonow.", cid)
				end
			end
		elseif(msg == "kasyno") then
			selfSay("Na gorze sa automaty do gry. Obok kazdego z nich znajdziesz instrukcje, dzieki ktorej dowiesz sie jak grac. Ja organizuje rowiez loterie {Geno}. Talony mozesz kupic u mnie. Jeden za ".. cenaTalonu .." gp.", cid)
			selfSay("Jezeli chcesz zamienic talony na zloto po prostu powiedz {zamien}, a ja wezme twoje talony i dam Ci ich rownowartosc w zlocie. Pamietaj, ze nie mozesz wyjsc z kasyna z talonami.", cid)
		elseif(msg == "zamien") then
			if(getPlayerItemCount(cid,2151) > 0) then
				local ile = getPlayerItemCount(cid,2151)
				doPlayerRemoveItem(cid,2151,ile)
				doPlayerAddMoney(cid,ile*cenaTalonu)
				selfSay("Dziekuje za zwrot talonow. Otrzymales za nie ".. cenaTalonu*ile .." zlota.", cid)
			else
				selfSay("Nie masz zadnego talonu.", cid)
			end
		elseif(msg == "geno") then
			selfSay("Geno jest to loteria, w ktorej co ".. losowanieGeno .." sekund jest losowanie i wszyscy uczestnicy, ktorzy odgadli wylosowana liczbe z zakresu 1-".. maxLiczbaGeno .." wygrywaja okreslona ilosc zetonow.", cid)
			selfSay("Aby zagrac powiedz: {gram}. Zeby dowiedziec sie ile sekund pozostalo do nastepnego losowania powiedz: {info}", cid)
		elseif(msg == "gram") then
			if(talk_state[cid] == 0) then
				if(uczestnicyGeno[cid] == nil) then
					selfSay("Powiedz jaka liczbe zaznaczasz?", cid)
					talk_state[cid] = 2
				else
					selfSay("Musisz czekac do nastepnego losowania, poniewaz juz grasz.", cid)
				end
			end
		elseif(msg == "info") then
			selfSay("Do nastepnego losowania pozostalo ".. (ostatnieLosowanie+losowanieGeno)-os.time() .." sekund.", cid)
		elseif(msg == "wyplac") then
			if(wygraniGeno[cid] ~= nil) then
				local ileDac = math.ceil(wygraniGeno[cid]*nagrodaTalonow)
				while(ileDac > 100) do
					doPlayerAddItem(cid,2151,100)
					ileDac = ileDac-100
				end
				if(ileDac > 0) then
					doPlayerAddItem(cid,2151,ileDac)
					ileDac = 0
				end
				selfSay("Prosze. Oto twoje ".. math.ceil(wygraniGeno[cid]*nagrodaTalonow) .." zetonow.", cid)
				table.remove(wygraniGeno,cid)
			else
				selfSay("Nie wygrales w loterii Geno. Sprobuj jeszcze raz.", cid)
			end
		elseif(msg == "info") then
			selfSay("ostatnieLosowanie: " .. ostatnieLosowanie .."\nlosowanieGeno: ".. losowanieGeno .."\ntime: ".. os.time(),cid)
		elseif(msg == "bye" or msg == "goodbye" or msg == "cya") then
			selfSay("Goodbye!", cid, TRUE)
			removeFocus(cid)
		end
	end
end
function onThink()
	if(ostatnieLosowanie+losowanieGeno <= os.time()) then
		ostatnieLosowanie = os.time()
		wygraniGeno = {}
		local wiadomosc = "Wygrani w loterii Geno: "
		local liczbaWylosowana = math.random(1,maxLiczbaGeno)
		for i, v in pairs(uczestnicyGeno) do
			if(v.liczba == liczbaWylosowana) then
				table.insert(wygraniGeno,i,v.talony)
				if(isPlayer(i) == TRUE) then
					wiadomosc = wiadomosc ..getCreatureName(i).. " "
				end
			end
		end
		for i, v in pairs(wygraniGeno) do
			if(isPlayer(i) == TRUE) then
				doPlayerSendTextMessage(i,MESSAGE_INFO_DESCR,"Gratulacje. Wlasnie wygrales ".. math.ceil(v*nagrodaTalonow) .." talonow w loterii Geno. Idz do kasjera i powiedz wyplac, aby dostac te talony. Spiesz sie, masz ".. losowanieGeno .." sekund.")
			end
		end
		uczestnicyGeno = {}
		if(#wygraniGeno == 0) then
			wiadomosc = "Nikt nie wygral w aktualnej turze loterii Geno."
		end
		selfSay(wiadomosc)
	end
	for i, focus in pairs(focuses) do
		if(isCreature(focus) == FALSE) then
			removeFocus(focus)
		else
			local distance = getDistanceTo(focus) or -1
			if((distance > 4) or (distance == -1)) then
				selfSay("Hmph!")
				removeFocus(focus)
			end
		end
	end
	lookAtFocus()
end