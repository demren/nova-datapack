local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local talkState = {}
local quest = 26303
	
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	local str = getPlayerStorageValue(cid, quest)
	if(not npcHandler:isFocused(cid)) then
		return false
	elseif msgcontains(msg, "misja") then
		if(str == -1) then
			npcHandler:say("Musisz zabic kazdego bossa na Rookgaardzie przynajmniej raz. Podejmujesz sie zadania? {ok}", cid)
			talkState[talkUser] = 2
		elseif(str == 1) then
			local boss_tbl = {"apprentice sheng","munster"}
			local bool = true
			local txt = ""
			local npcs = {}
			if getPlayerStorageValue(cid, 26300) ~= 3 then
				bool = false
				npcs[1] = "Inez"
			end
			if getPlayerStorageValue(cid, 26301) ~= 3 then
				bool = false
				npcs[2] = "Rosalie"
			end
			if getPlayerStorageValue(cid, 26302) ~= 3 then
				bool = false
				npcs[3] = "Dylan"
			end
			if not bool then
				txt = txt.."najpierw zrobic misje u "
				for i=1,#npcs do
					txt=txt..npcs[i]..(i+1 <= #npcs and ", " or " ")
				end
			end
			monsters = (#npcs > 0 and "oraz " or "najpierw musisz ").."zabic: "
			for i=1, #boss_tbl do
				if getPlayerKilledBossCount(cid, boss_tbl[i]) == 0 then
					bool = false
					monsters=monsters..boss_tbl[i]..(i+1 <= #boss_tbl and ", " or ".")
				end
			end
			txt=txt..monsters
			if bool then
				doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Otrzymales first Hunter addon.")
				setPlayerStorageValue(cid, quest, 2)
				npcHandler:say("Brawo!! W nagrode daje Ci first Hunter addon!", cid)
			else
				npcHandler:say("Niestety, musisz "..txt, cid)
			end
		elseif(str == 2) then
			npcHandler:say("Juz wykonales u mnie misje. Wielkie dzieki!", cid)
		end
	elseif msgcontains(msg, "ok") and str == -1 and talkState[talkUser] == 2 then
		npcHandler:say("W takim razie ruszaj!! Jak skonczysz wroc do mnie po nagrode, ktora jest first hunter addon.", cid)
		setPlayerStorageValue(cid, quest, 1)
		talkState[talkUser] = 0
	else
		npcHandler:say("Nie wiem o czym mowisz ... Napisz {misja}, aby dowiedziec sie czegos o misji.", cid)
		talkState[talkUser] = 0
	end
	return TRUE
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())