local tasks =
{
        [1] = 	{questStarted = 1541, questStorage = 63021, killsRequired = 200, raceName = "Dworces", rewards = 			{first = {enable = true, type = "exp", values = 30000}, 		second = {enable = true, type = "money", values = 100000}, 		third = {enable = true, type = "item", values = {2322, 1}}}},
        [2] = 	{questStarted = 1542, questStorage = 63022, killsRequired = 300, raceName = "Apes", rewards = 				{first = {enable = true, type = "exp", values = 50000}, 		second = {enable = true, type = "money", values = 100000}, 		third = {enable = true, type = "item", values = {5883, 15}}}},
        [3] = 	{questStarted = 1543, questStorage = 63023, killsRequired = 350, raceName = "Lizards", rewards = 			{first = {enable = true, type = "exp", values = 200000}, 		second = {enable = true, type = "money", values = 200000}, 		third = {enable = true, type = "item", values = {11336, 1}}}},
        [4] = 	{questStarted = 1544, questStorage = 63024, killsRequired = 80, raceName = "Bonelords", rewards = 			{first = {enable = true, type = "exp", values = 150000}, 		second = {enable = true, type = "money", values = 50000}, 			third = {enable = false, type = "item", values = {9955, 1}}}},
        [5] = 	{questStarted = 1545, questStorage = 63025, killsRequired = 200, raceName = "Slugs", rewards = 				{first = {enable = true, type = "exp", values = 150000}, 		second = {enable = true, type = "money", values = 200000}, 			third = {enable = false, type = "item", values = {9955, 1}}}},
        [6] = 	{questStarted = 1546, questStorage = 63026, killsRequired = 200, raceName = "Crawlers", rewards = 			{first = {enable = true, type = "exp", values = 600000}, 		second = {enable = true, type = "money", values = 350000}, 		third = {enable = true, type = "item", values = {18398, 1}}}},
        [7] = 	{questStarted = 1547, questStorage = 63027, killsRequired = 200, raceName = "Deepling monsters", rewards = 	{first = {enable = true, type = "exp", values = 1500000}, 		second = {enable = true, type = "money", values = 450000}, 		third = {enable = true, type = "item", values = {15432, 1}}}},
        [8] = 	{questStarted = 1548, questStorage = 63028, killsRequired = 250, raceName = "Shamans", rewards = 			{first = {enable = true, type = "exp", values = 5200000}, 		second = {enable = true, type = "money", values = 800000}, 		third = {enable = true, type = "item", values = {12434, 1}}}},
        [9] = 	{questStarted = 1549, questStorage = 63029, killsRequired = 200, raceName = "Askarak demons", rewards = 	{first = {enable = true, type = "exp", values = 3500000}, 		second = {enable = true, type = "money", values = 300000}, 		third = {enable = true, type = "item", values = {18411, 1}}}},
        [10] = 	{questStarted = 1550, questStorage = 63030, killsRequired = 400, raceName = "Mutated Demons", rewards = 	{first = {enable = true, type = "exp", values = 10000000}, 		second = {enable = true, type = "money", values = 1000000}, 	third = {enable = true, type = "item", values = {13570, 1}}}},
        [11] = 	{questStarted = 1551, questStorage = 63031, killsRequired = 450, raceName = "War Golems", rewards = 		{first = {enable = true, type = "exp", values = 750000}, 		second = {enable = true, type = "money", values = 750000}, 		third = {enable = true, type = "item", values = {10522, 1}}}},
        [12] = 	{questStarted = 1552, questStorage = 63032, killsRequired = 200, raceName = "Dragonlings", rewards = 		{first = {enable = true, type = "exp", values = 1000000}, 		second = {enable = true, type = "money", values = 650000}, 		third = {enable = true, type = "item", values = {7967, 1}}}},
        [13] = 	{questStarted = 1553, questStorage = 63033, killsRequired = 150, raceName = "Basilisks", rewards = 			{first = {enable = true, type = "exp", values = 17500000}, 		second = {enable = true, type = "money", values = 1000000}, 	third = {enable = true, type = "item", values = {18527, 1}}}},
        [14] = 	{questStarted = 1554, questStorage = 63034, killsRequired = 300, raceName = "Stampors", rewards = 			{first = {enable = true, type = "item", values = {13300, 5}}, 	second = {enable = true, type = "item", values = {13299, 10}}, 	third = {enable = true, type = "item", values = {13300, 20}}}}
}

local storage = 64523

local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}
local voc = {}

function onCreatureAppear(cid)                          npcHandler:onCreatureAppear(cid)                        end
function onCreatureDisappear(cid)                       npcHandler:onCreatureDisappear(cid)                     end
function onCreatureSay(cid, type, msg)                  npcHandler:onCreatureSay(cid, type, msg)                end
function onThink()                                      npcHandler:onThink()                                    end

function creatureSayCallback(cid, type, msg)

        local s = getPlayerStorageValue(cid, storage)

        if(not npcHandler:isFocused(cid)) then
                return false
        end
		local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
		local lol = tonumber(talkState[talkUser]) == nil and 0 or tonumber(talkState[talkUser])
		if(s < 0) or (s == "-0") then
			doPlayerSetStorageValue(cid, storage, 0)
			s=0
		end
		if (lol > 0 and msgcontains(msg, 'yes')) then
			doPlayerSetStorageValue(cid, tasks[lol].questStarted, 1)
			doPlayerSetStorageValue(cid, storage, lol)
			selfSay('You have started the task! In this task you need to kill ' .. tasks[lol].killsRequired .. ' ' .. tasks[lol].raceName .. '. Come back when you kill ' .. tasks[lol].killsRequired .. ' ' .. tasks[lol].raceName .. '!', cid)
			talkState[talkUser] = 0
		elseif(msgcontains(msg, 'task')) then
			if(string.lower(msg) == 'task') then
				selfSay('You can choose from the following missions:', cid)
				local bool = false
				for _,t in ipairs(tasks) do
					if(getPlayerStorageValue(cid, t.questStarted) <= 0) then
						bool = true
						selfSay('Say: {task '..t.raceName..'}, to accept the mission to kill '..t.killsRequired..' '..t.raceName..'.', cid)
					end
				end
				if bool == false then
					selfSay("You have done all tasks. Good work!",cid)
					doPlayerSetStorageValue(cid, 4380, 1)
				end
			elseif (s <= 0) then
				for k, t in ipairs(tasks) do
					if(string.lower("task "..t.raceName) == string.lower(msg)) then
						what_task = k
						break
					end
				end
				if(what_task) then
					if(getPlayerStorageValue(cid, tasks[what_task].questStarted) == 1) then
						selfSay('You are currently making task about ' .. tasks[what_task].raceName .. '.', cid)
					elseif(getPlayerStorageValue(cid, tasks[what_task].questStarted) == 2) then
						selfSay('You have done task about ' .. tasks[what_task].raceName .. '.', cid)
					else
						talkState[talkUser] = what_task
						selfSay('Are you sure you want to do this task? In this task you need to kill ' .. tasks[what_task].killsRequired .. ' ' .. tasks[what_task].raceName .. '.? ({yes})', cid)
					end
					return true
				else
					selfSay('Say: {task}.', cid)
				end
			elseif(getPlayerStorageValue(cid, tasks[s].questStarted) == 1) then
				selfSay('You are currently making task about ' .. tasks[s].raceName .. '.', cid)
			else
				selfSay('You have done task about ' .. tasks[s].raceName .. '.', cid)
			end
        elseif msgcontains(msg, 'report') then
                if tasks[s] and getPlayerStorageValue(cid,tasks[s].questStarted) == 1 then

                        if(getPlayerStorageValue(cid, tasks[s].questStorage) < 0) then
                                doPlayerSetStorageValue(cid, tasks[s].questStorage, 0)
                        end

                        if(getPlayerStorageValue(cid, tasks[s].questStorage) >= tasks[s].killsRequired) then
                                selfSay('Great!... you have finished the task about ' .. tasks[s].raceName .. '. Good job.', cid)
                                doPlayerSetStorageValue(cid, storage, 0)
                                doPlayerSetStorageValue(cid, tasks[s].questStarted, 2)
                                if(tasks[s].rewards.first.enable) then
                                        if(tasks[s].rewards.first.type == "boss") then
                                                doTeleportThing(cid, tasks[s].rewards.first.values)
                                        elseif(tasks[s].rewards.first.type == "exp") then
                                                doPlayerAddExperience(cid, tasks[s].rewards.first.values)
                                        elseif(tasks[s].rewards.first.type == "item") then
                                                doPlayerAddItem(cid, tasks[s].rewards.first.values[1], tasks[s].rewards.first.values[2])
                                        elseif(tasks[s].rewards.first.type == "money") then
                                                doPlayerAddMoney(cid, tasks[s].rewards.first.values)
                                        elseif(tasks[s].rewards.first.type == "storage") then
                                                doPlayerSetStorageValue(cid, tasks[s].rewards.first.values[1], tasks[s].rewards.first.values[2])
                                        end
                                end
                                if(tasks[s].rewards.second.enable) then
                                        if(tasks[s].rewards.second.type == "boss") then
                                                doTeleportThing(cid, tasks[s].rewards.second.values)
                                        elseif(tasks[s].rewards.second.type == "exp") then
                                                doPlayerAddExperience(cid, tasks[s].rewards.second.values)
                                        elseif(tasks[s].rewards.second.type == "item") then
                                                doPlayerAddItem(cid, tasks[s].rewards.second.values[1], tasks[s].rewards.second.values[2])
                                        elseif(tasks[s].rewards.second.type == "money") then
                                                doPlayerAddMoney(cid, tasks[s].rewards.second.values)
                                        elseif(tasks[s].rewards.second.type == "storage") then
                                                doPlayerSetStorageValue(cid, tasks[s].rewards.second.values[1], tasks[s].rewards.second.values[2])
                                        end
                                end
                                if(tasks[s].rewards.third.enable) then
                                        if(tasks[s].rewards.third.type == "boss") then
                                                doTeleportThing(cid, tasks[s].rewards.third.values)
                                        elseif(tasks[s].rewards.third.type == "exp") then
                                                doPlayerAddExperience(cid, tasks[s].rewards.third.values)
                                        elseif(tasks[s].rewards.third.type == "item") then
                                                doPlayerAddItem(cid, tasks[s].rewards.third.values[1], tasks[s].rewards.third.values[2])
                                        elseif(tasks[s].rewards.third.type == "money") then
                                                doPlayerAddMoney(cid, tasks[s].rewards.third.values)
                                        elseif(tasks[s].rewards.third.type == "storage") then
                                                doPlayerSetStorageValue(cid, tasks[s].rewards.third.values[1], tasks[s].rewards.third.values[2])
                                        end
                                end  
                        else
                                selfSay('Current ' .. getPlayerStorageValue(cid, tasks[s].questStorage) .. ' ' .. tasks[s].raceName .. ' killed, you need to kill ' .. tasks[s].killsRequired .. '.', cid)
                        end
				elseif tasks[s] and getPlayerStorageValue(cid,tasks[s].questStarted) == 2 then
						selfSay('You have done this task.', cid)
                else
                        selfSay('You do not have started any task.', cid)
                end
        end

end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new()) 