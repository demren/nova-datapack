	local apos = {x=328, y=188, z=11}
	local bpos = {x=131, y=82, z=8}
	local monster
random_monster = {
{rat}, 
{spider}, 
{bug}, 
{bat}, 
{minotaur}, 
{troll},
{'minotaur guard'},
{'minotaur mage'},
{deer}, 
{amazon}, 
{cyclops}, 
{valkirie}, 
{Necromancer}, 
{tarantule}, 
{mummy}, 
{Ghoul},
{rat}, 
{spider}, 
{bug}, 
{bat}, 
{minotaur}, 
{troll},
{'minotaur guard'},
{'minotaur mage'},
{deer}, 
{amazon}, 
{cyclops}, 
{valkirie}, 
{Necromancer}, 
{tarantule}, 
{mummy}, 
{Ghoul},
{rat}, 
{spider}, 
{bug}, 
{bat}, 
{minotaur}, 
{troll},
{'minotaur guard'},
{'minotaur mage'},
{deer}, 
{amazon}, 
{cyclops}, 
{valkirie}, 
{Necromancer}, 
{tarantule}, 
{mummy}, 
{Ghoul}, 
{rat}, 
{spider}, 
{bug}, 
{bat}, 
{minotaur}, 
{troll},
{'minotaur guard'},
{'minotaur mage'},
{deer}, 
{amazon}, 
{cyclops}, 
{valkirie}, 
{Necromancer}, 
{tarantule}, 
{mummy}, 
{Ghoul}, 
{rat}, 
{spider}, 
{bug}, 
{bat}, 
{minotaur}, 
{troll},
{'minotaur guard'},
{'minotaur mage'},
{deer}, 
{amazon}, 
{cyclops}, 
{valkirie}, 
{Necromancer}, 
{tarantule}, 
{mummy}, 
{Ghoul}, 
{rat}, 
{spider}, 
{bug}, 
{bat}, 
{minotaur}, 
{troll},
{'minotaur guard'},
{'minotaur mage'},
{deer}, 
{amazon}, 
{cyclops}, 
{valkirie}, 
{Necromancer}, 
{tarantule}, 
{mummy}, 
{Ghoul}, 
{'Dragon Hatchling'}, 
{Dragon}, 
{Wyvern}, 
{'Dragon Lord'}, 
} 
local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

-- OTServ event handling functions start
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end
-- OTServ event handling functions end
function creatureSayCallback(cid, type, msg)
if(not npcHandler:isFocused(cid)) then
return false
end
if msgcontains(msg == "tak" or msg == "yes") then
selfSay('{Summon}owalem {potwory}, ktore zagrazaly Rookstayejerom.')
    if(msgcontains(msg, 'summon'))then

	if(math.random == random_monster(1,100) == monster) then
		if (monster == 'dragon lord') then
			doSummonCreature(monster, bpos)
			selfSay('Udalo mi sie stworzyc Dragon Lorda.. Uciekaj! (...) Ohh nie ma go. Tak, pojawil sie gdzie indziej. Znajdz i zabij go szybko, nie chce zeby ktos sie zorietowal ze Ci go wyczarowalem.')
	
		if (monster ~= 'dragon lord') then

			doSummonCreature(monster, apos)
			selfSay('Udalo mi sie stworzyc .. monster .. . Musisz zabic go szybko, nie chce zeby ktos sie zorietowal ze Ci go wyczarowalem.')
end
end
end
else
selfSay(havent_item)
end




elseif msgcontains(msg, 'no') and (talk_state >= 1 and talk_state <= 5) then
selfSay('Ok than.')
talk_state = 0
end
-- Place all your code in here. Remember that hi, bye and all that stuff is already handled by the npcsystem, so you do not have to take care of that yourself.
return true
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new()) 
