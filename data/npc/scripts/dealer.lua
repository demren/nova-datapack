local ile_za_jointa = 4500  --w gp, cena jednego jointa
local focuses = {}
local talkState = {}
--Focuses
local function isFocused(cid)
   for i, v in pairs(focuses) do
      if(v == cid) then
         return true
      end
   end
   return false
end
local function addFocus(cid)
   if(not isFocused(cid)) then
      table.insert(focuses, cid)
   end
end
local function removeFocus(cid)
   for i, v in pairs(focuses) do
      if(v == cid) then
         table.remove(focuses, i)
         break
      end
   end
end
local function lookAtFocus()
   for i, v in pairs(focuses) do
      if(isPlayer(v) == TRUE) then
         doNpcSetCreatureFocus(v)
         return
      end
   end
   doNpcSetCreatureFocus(0)
end
function onCreatureSay(cid, type, msg)
    local msg = string.lower(msg)
    if(msgcontains(msg, "hi")) then
        if(isFocused(cid) == false) then
            selfSay("Czego chcesz?", cid, True)
        else
            selfSay("Pojebany jestes? Witales sie juz.", cid, True)
        end
    elseif(msgcontains(msg, "blant") or msgcontains(msg, "joint") or msgcontains(msg, "trawa") or msgcontains(msg, "marihuana")) then
        selfSay("Chcesz kupic? No to musisz zaplacic. 1 joint == "..ile_za_jointa.." gp. Kupujesz?", cid, True)
        talkState[cid] = 1
    elseif(msgcontains(msg, "tak") or msgcontains(msg, "no") or msgcontains(msg, "jasne")) then
        if(getPlayerMoney(cid) >= ile_za_jointa) then
            doPlayerRemoveMoney(cid, ile_za_jointa)
            doPlayerAddItem(cid, 7499, 1)
            selfSay("No, to mi sie podoba. Mam nadzieje, ze zostaniesz stalym klientem. Dzieki, nara.", cid, True)
        else
            selfSay("Co ty cwiczysz? Myslisz, ze sie nie zczaje, ze nie masz floty..?", cid, True)
            doCreatureAddHealth(cid, -100)
            doSendMagicEffect(getCreaturePosition(cid), CONST_ME_MORT_AREA)
        end
        removeFocus(cid)
        talkState[cid] = 0
    end
end
function onThink()
   for i, focus in pairs(focuses) do
        if(isCreature(focus) == FALSE) then
            removeFocus(focus)
        else
            local distance = getDistanceTo(focus) or -1
            if((distance > 4) or (distance == -1)) then
                selfSay("Spadaj, frajer.")
                removeFocus(focus)
            end
        end
    end
    lookAtFocus()
end