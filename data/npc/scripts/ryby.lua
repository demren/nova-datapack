local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
-- OTServ event handling functions start
function onCreatureAppear(cid)                npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid)             npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg)     npcHandler:onCreatureSay(cid, type, msg) end
function onThink()                         npcHandler:onThink() end
-- OTServ event handling functions end
local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)
shopModule:addBuyableItem({'ryba', 'fish'},                         2667, 10,          'ryby')
shopModule:addBuyableItem({'sandacz', 'green perch'},                     7159, 20,          'zielony sandacz')
shopModule:addBuyableItem({'szczupak', 'northern pike'},                 2669, 20,          'szczupak')
shopModule:addBuyableItem({'pstrag', 'rainbow trout'},                     7158, 25,          'tenczowy pstrag')
shopModule:addBuyableItem({'krewetka', 'shrimp'},                     2670, 30,          'krewetki')
npcHandler:addModule(FocusModule:new())