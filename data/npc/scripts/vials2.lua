local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end
function creatureSayCallback(cid, type, msg)
if(not npcHandler:isFocused(cid)) then
return false
end
local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
addon_need_premium = 'Sorry, you need a premium account to get addons.', cid
addon_have_already = 'Sorry, you already have this addon.', cid
addon_have_not_items = 'Sorry, you don\'t have these items.', cid
addon_give = 'Here you are.', cid
player_gold = getPlayerItemCount(cid,2148)
player_plat = getPlayerItemCount(cid,2152)*100
player_crys = getPlayerItemCount(cid,2160)*10000
player_money = player_gold + player_plat + player_crys
if msgcontains(msg, 'help') then
selfSay('You can here change items{empty great potion flask}, {empty strong potion flask}, {empty potion flask}.?', cid)
elseif msgcontains(msg, 'empty great potion flask') then
if getPlayerItemCount(cid,7635) >= 100 then
selfSay('Did you bring me the 100 empty great potion flask ?', cid)
talk_state = 1
else
selfSay('I need a 100 empty great potion flask, to give you lottery ticket. Come back when you have them.', cid)
talk_state = 0
end
elseif msgcontains(msg, 'yes') and talk_state == 1 then
talk_state = 0
if getPlayerItemCount(cid,7635) >= 100 then
if doPlayerRemoveItem(cid,7635, 100) == TRUE then
doPlayerAddItem(cid, 5957, 1)
selfSay('Here u are.', cid)
end
else
selfSay(havent_item)
end
elseif msgcontains(msg, 'empty strong potion flask') then
if getPlayerItemCount(cid,2393) >=100 then
selfSay('Did you bring me the 100 empty strong potion flask ?', cid)
talk_state = 2
else
selfSay('I need a 100 empty strong potion flask to give you the lottery ticket. Come back when you have them.', cid)
talk_state = 0
end
elseif msgcontains(msg, 'yes') and talk_state == 2 then
talk_state = 0
if getPlayerItemCount(cid,7634) >= 100 then
if doPlayerRemoveItem(cid,7634,100) == 100 then
selfSay(addon_give)
doPlayerAddItem(cid,5947,1)
end
else
selfSay(havent_item)
end
elseif msgcontains(msg, 'empty potion flask') then
if getPlayerItemCount(cid,7636) >= 100 then
selfSay('Did you bring me 100 empty potion flask ?', cid)
talk_state = 3
else
selfSay('I need empty potion flask, to give you the lottery ticket. Come back when you have them.', cid)
talk_state = 0
end
elseif msgcontains(msg, 'yes') and talk_state == 3 then
talk_state = 0
if getPlayerItemCount(cid,7636) >= 100 then
if doPlayerRemoveItem(cid,7636,100) == 100 then
doPlayerAddItem(cid,5947,1)
selfSay('Here u are.', cid)
end
else
selfSay(havent_item)
end

elseif msgcontains(msg, 'no') and (talk_state >= 1 and talk_state <= 5) then
selfSay('Ok than.')
talk_state = 0
end
-- Place all your code in here. Remember that hi, bye and all that stuff is already handled by the npcsystem, so you do not have to take care of that yourself.
return true
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new()) 