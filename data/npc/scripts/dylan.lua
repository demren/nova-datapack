local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local talkState = {}
local quest = 26302
	
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	local str = getPlayerStorageValue(cid, quest)
	if(not npcHandler:isFocused(cid)) then
		return false
	elseif msgcontains(msg, "misja") then
		if(str == -1) then
			npcHandler:say("Musisz zabic 200 Minotaurow. {ok}?", cid)
			talkState[talkUser] = 2
		elseif(str == 1) then
			npcHandler:say("Idz zabij 200 minotaurow.", cid)
		elseif(str == 2) then
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Otrzymales Obsidian knife i 5000 expa.")
			doPlayerAddItem(cid, 5908,1)
			doPlayerAddExperience(cid, 5000)
			setPlayerStorageValue(cid, quest, 3)
			npcHandler:say("Brawo!! W nagrode daje Ci 5000 expa i oto ten Obsidian knife, dzieki ktoremu mozesz wycinac skory z Minotaurow. Jednak na Rookgaardzie o wiele rzadziej sie to udaje.", cid)
			npcHandler:say("Jesli wykonales juz misje u Inez i Rosalie to polecam Ci misje u Lillian.", cid)
		elseif(str == 3) then
			npcHandler:say("Juz wykonales u mnie misje. Wielkie dzieki!", cid)
		end
	elseif msgcontains(msg, "ok") and str == -1 and talkState[talkUser] == 2 then
		npcHandler:say("W takim razie ruszaj!! Jak skonczysz wroc do mnie po nagrode.", cid)
		setPlayerStorageValue(cid, quest, 1)
		talkState[talkUser] = 0
	else
		npcHandler:say("Nie wiem o czym muwisz ... Napisz {misja}, aby dowiedziec sie czegos o misji.", cid)
		talkState[talkUser] = 0
	end
	return TRUE
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())