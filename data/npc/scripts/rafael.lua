local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local Topic = {}
local storage = 20011

function onCreatureAppear(cid)              npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid)           npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg)  npcHandler:onCreatureSay(cid, type, msg) end
function onThink()                      npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
    local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
    if msgcontains(msg, "hi") or msgcontains(msg, "hello") and (not npcHandler:isFocused(cid)) then
        if getPlayerStorageValue(cid, storage) < 1 then
            selfSay("Hey, you! Frown now on you are my henchman! I have first {task} for you!", cid)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("Have you brought a crown for me, my henchman?", cid)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("I\'m a king of the world!", cid)
            Topic[talkUser] = 0
        end
        npcHandler:addFocus(cid)
        return true
    end
    if(not npcHandler:isFocused(cid)) then
        return false
    end
    if msgcontains(msg, "bye") or msgcontains(msg, "farewell") then
        selfSay("Good bye.", cid)
        Topic[talkUser] = 0
        npcHandler:releaseFocus(cid)
    elseif msgcontains(msg, "quest") or msgcontains(msg, "mission") or msgcontains(msg, "task") or msgcontains(msg, "quest") or (msgcontains(msg, "crown") and getPlayerStorageValue(cid, storage) > 0) then
        if getPlayerStorageValue(cid, storage) < 1 then
            selfSay("Sneaky dragon\'s stole my crown! Now, my henchman, go to the dragon\'s lair near town and bring me back my crown!", cid)
            setPlayerStorageValue(cid, storage, 1)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("Have you brought a crown for me, my henchman?", cid)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("I\'m a king of the world!", cid)
            Topic[talkUser] = 0
        end
    elseif Topic[talkUser] == 1 then
        if msgcontains(msg, "yes") then
            if getPlayerItemCount(cid, 2128) >= 1 then
                doPlayerRemoveItem(cid, 2128, 1)
                selfSay("Thank you, my henchman!", cid)
				setCreatureMaxHealth(cid, (getCreatureMaxHealth(cid) + 100))
				doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You received 100 health points.")
                setPlayerStorageValue(cid, storage, 2)
            else
                selfSay("You don\'t have my crown! Get lost!", cid)
            end
        else
            selfSay("You don\'t have my crown? Keep looking in dragon\'s lair near town.", cid)
        end
        Topic[talkUser] = 0
    end
    return TRUE
end
npcHandler:setMessage(MESSAGE_WALKAWAY, "Good bye.")
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)