local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local Topic = {}
local storage = 21014

function onCreatureAppear(cid)              npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid)           npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg)  npcHandler:onCreatureSay(cid, type, msg) end
function onThink()                      npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
    local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
    if msgcontains(msg, "hi") or msgcontains(msg, "hello") and (not npcHandler:isFocused(cid)) then
        if getPlayerStorageValue(cid, storage) < 1 then
            selfSay("Hey you! I\'m gonna need your {help}!", cid)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("Have you brought my paw amulet?", cid)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            selfSay("Thank you again.", cid)
            Topic[talkUser] = 0
        end
        npcHandler:addFocus(cid)
        return true
    end
    if(not npcHandler:isFocused(cid)) then
        return false
    end
    if msgcontains(msg, "bye") or msgcontains(msg, "farewell") then
        selfSay("Farewell.", cid)
        Topic[talkUser] = 0
        npcHandler:releaseFocus(cid)
	elseif msgcontains(msg, "quest") or msgcontains(msg, "mission") or msgcontains(msg, "task") or msgcontains(msg, "help") or (msgcontains(msg, "shoes") and getPlayerStorageValue(cid, storage) > 0) then
        if getPlayerStorageValue(cid, storage) < 1 then
            selfSay("During my last fight with behemoths i\'ve lost my paw amulet! You need to find it for me!", cid)
            setPlayerStorageValue(cid, storage, 1)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("Have you brought my paw amulet?", cid)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            selfSay("Thank you again for your help.", cid)
            Topic[talkUser] = 0
        end
    elseif Topic[talkUser] == 1 then
        if msgcontains(msg, "yes") then
            if getPlayerItemCount(cid, 2218) >= 1 then
                doPlayerRemoveItem(cid, 2218, 1)
                selfSay("Oh please take this crossbow as a reward.", cid)
                doPlayerAddItem(cid, 8851, 1)
                setPlayerStorageValue(cid, storage, 2)
            else
                selfSay("I already told you. Behemoths took it from me. You have to take it back.", cid)
            end
        else
            selfSay("I already told you. Behemoths took it from me. You have to take it back.", cid)
        end
        Topic[talkUser] = 0
    end
    return TRUE
end

npcHandler:setMessage(MESSAGE_WALKAWAY, "Good bye.")
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)