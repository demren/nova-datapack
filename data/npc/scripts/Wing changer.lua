function creatureSayCallback(cid, type, msg)
if(not npcHandler:isFocused(cid)) then
return false
end
local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
if msgcontains(msg, 'help') then
selfSay('Here you can change 100 chicken feathers for 1 enchanted chicken wing.', cid)
elseif msgcontains(msg, 'enchanted chicken wing') then
selfSay('Do you want to change 100 chicken feathers for 1 enchanted chicken wing?', cid)
talkState[talkUser] = 1
elseif (msgcontains(msg, 'yes') and talkState[talkUser] == 1) then
if getPlayerItemCount(cid,5890) >= 1 and getPlayerItemCount(cid,5890) >= 1 then
if doPlayerRemoveItem(cid,5890, 100) == 1 and doPlayerRemoveItem(cid,5890, 100) == 1 then
doPlayerAddItem(cid, 5891, 1)
selfSay('Here you are.', cid)
end
else
selfSay('Sorry, you don\'t have the item.', cid)
end
talkState[talkUser] = 0
elseif(msgcontains(msg, 'no') and isInArray({1}, talkState[talkUser]) == TRUE) then
talkState[talkUser] = 0
selfSay('Ok then.', cid)
end
return 1
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new()) 