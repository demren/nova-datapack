local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local talkState = {}
local quest = 26300
	
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	local str = getPlayerStorageValue(cid, quest)
	if(not npcHandler:isFocused(cid)) then
		return false
	elseif msgcontains(msg, "misja") then
		if(str == -1) then
			npcHandler:say("Szczury! Ble.. Odkad pamietam cale podziemia rookgaardu sa zapelnione tymi gryzoniami! Aby otrzymac nagrode musisz isc do podziemi i zabic 20 szczurow. {ok}?", cid)
			talkState[talkUser] = 2
		elseif(str == 1) then
			npcHandler:say("Idz zabij 20 szczurow w podziemiach miasta i wroc do mnie, dostaniesz 150 expa, 200gp, 5 mikstur zycia, line i lopate.", cid)
		elseif(str == 2) then
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Otrzymales 150 expa, 200gp, 5 mikstur zycia, line i lopate.")
			doPlayerAddExperience(cid, 150)
			doPlayerAddMoney(cid, 200)
			setPlayerStorageValue(cid, quest, 3)
			doPlayerAddItem(cid, 2120,1)
			doPlayerAddItem(cid, 8704, 5)
			doPlayerAddItem(cid, 2554,1)
			npcHandler:say("Wielkie dzieki!! Teraz polecam Ci misje u Rosalie na zabijanie Trollow. Zaznaczam Ci na mapie miejsce gdzie znajdziesz NPC Rosalie. Pieniadze ktore otrzymales ode mnie polecam Ci wydac w sklepie na zbroje i bron.", cid)
			npcHandler:say("Zaznaczam Ci na mapie sklep ze zbroja i bronia. Mozesz tam rowniez sprzedawac zdobyte zbroje.", cid)
			doPlayerAddMapMark(cid, {x=2127-1282,y=1876-1422,z=7}, 1, "NPC Rosalie quest")
			doPlayerAddMapMark(cid, {x=2102-1282,y=1884-1422,z=7}, 13, "Sklep.")
		elseif(str == 3) then
			npcHandler:say("Juz wykonales u mnie misje. Teraz polecam Ci misje u {Rosalie} na zabijanie Trollow. Zaznaczam Ci na mapie miejsce gdzie znajdziesz Rosalie.", cid)
			doPlayerAddMapMark(cid, {x=2127-1282,y=1876-1422,z=7}, 1, "NPC Rosalie")
		end
	elseif msgcontains(msg, "ok") and str == -1 and talkState[talkUser] == 2 then
		npcHandler:say("Okey, idz je zabijaj i wroc do mnie jak skonczysz. :)", cid)
		setPlayerStorageValue(cid, quest, 1)
		talkState[talkUser] = 0
	else
		npcHandler:say("Nie wiem o czym mowisz ... Napisz {misja}, aby dowiedziec sie czegos o misji.", cid)
		talkState[talkUser] = 0
	end
	return TRUE
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())