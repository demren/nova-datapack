local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)
shopModule:addSellableItem({'medicine pouch'}, 13506, 1000, 'medicine pouch') -- npc kupuje
local cost = {
			[1] = 300, -- kharos
			[2] = 50, -- tamoris
			[3] = 200, -- ridia
			[4] = 200, -- calmia
			[5] = 420, -- port fimao
			[6] = 0, -- rookgaard
			[7] = 350, -- nefaros
			[8] = 300, -- vilysia
			[9] = 280 -- xoris
			}
			local blocked_boats = {4,6}
function creatureSayCallback(cid, type, msg)
	local names = ''
	for i=1, 9 do
		if not isInArray(blocked_boats, i) then
			if (isInArray(blocked_boats, #cost) and i==#cost-1) or i==#cost then
				names=names.."{"..getTownName(i).."}"
			elseif (isInArray(blocked_boats, #cost-1) and i==#cost-2) or i==#cost-1 then
				names=names.."{"..getTownName(i).."} or "
			else
				names = names .. "{"..getTownName(i) .."}, "
			end
		end
	end
	if (msgcontains(msg, "hello") or msgcontains(msg, "hi")) and not npcHandler:isFocused(cid) then
		npcHandler:say('Hello '..getPlayerName(cid)..'. If you don\'t know where to flow, say {travel}.', cid)
		npcHandler:addFocus(cid)
		return true
	end
	if(not npcHandler:isFocused(cid)) then
		return false
	end
	
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	local lol = tonumber(talkState[talkUser]) == nil and 0 or tonumber(talkState[talkUser])
	if(msgcontains(msg, 'travel')) then
		npcHandler:say('I can take you to '..names..' for just a small fee. {Kharos} is the capital city.', cid)
	elseif(cost[getTownId(string.lower(msg))] and not isInArray(blocked_boats,getTownId(string.lower(msg)))) then
		npcHandler:say('Do you wish to travel to '..getTownName(getTownId(string.lower(msg)))..' for '..cost[getTownId(string.lower(msg))]..' gold coins? ({yes})',cid)
		talkState[talkUser]=getTownId(string.lower(msg))
	elseif(msgcontains(msg, 'yes') and lol>0 and not isInArray(blocked_boats,getTownId(string.lower(lol)))) then
		if(getPlayerMoney(cid) >= cost[lol]) then
			if getCreatureSkullType(cid) == SKULL_NONE or getCreatureSkullType(cid) ~= SKULL_GREEN then
				doPlayerRemoveMoney(cid,cost[lol])
				doTeleportThing(cid, CITY_POS.boat[lol])
				doSendMagicEffect(CITY_POS.boat[lol], CONST_ME_TELEPORT)
				local sm_id = getPlayerSMsByGUID(getPlayerGUID(cid))
				if(lol == 5 and isInArray2(sm_id, 241)) then --port fimao
					setSMValueplus1(cid,241,1)
				end
				if(lol == 7 and isInArray2(sm_id, 241)) then --nefaros
					setSMValueplus1(cid,241,1)
				end
				if(lol == 8 and isInArray2(sm_id, 241)) then --vilysia
					setSMValueplus1(cid,241,1)
				end
			else
				selfSay('Sorry. You have Skull.', cid)
			end
		else
			selfSay('Sorry, you don\'t have enough gold.', cid)
		end
		talkState[talkUser]=0
	else
		npcHandler:say('I can take you to '..names..' for just a small fee. {Kharos} is the capital city.', cid)
		talkState[talkUser]=0
	end
	return true
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)