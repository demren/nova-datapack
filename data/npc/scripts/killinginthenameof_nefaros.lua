local tasks =
{
        [1] = 	{questStarted = 1521, questStorage = 64021, killsRequired = 200, raceName = "Tortoises", rewards = 			{first = {enable = true, type = "exp", values = 10000}, 		second = {enable = true, type = "money", values = 100000}, third = {enable = true, type = "item", values = {5899, 20}}}},
        [2] = 	{questStarted = 1522, questStorage = 64022, killsRequired = 200, raceName = "Scarabs", rewards = 			{first = {enable = true, type = "exp", values = 10000}, 		second = {enable = true, type = "money", values = 10000}, third = {enable = true, type = "item", values = {2159, 100}}}},
        [3] = 	{questStarted = 1523, questStorage = 64023, killsRequired = 150, raceName = "Mummies", rewards = 			{first = {enable = true, type = "exp", values = 100000}, 		second = {enable = true, type = "money", values = 200000}, third = {enable = true, type = "item", values = {11207, 1}}}},
        [4] = 	{questStarted = 1524, questStorage = 64024, killsRequired = 80, raceName = "Vampires", rewards = 			{first = {enable = true, type = "exp", values = 150000}, 		second = {enable = true, type = "money", values = 50000}, third = {enable = true, type = "item", values = {9955, 1}}}},
        [5] = 	{questStarted = 1525, questStorage = 64025, killsRequired = 100, raceName = "Bonebeasts", rewards = 		{first = {enable = true, type = "exp", values = 200000}, 		second = {enable = true, type = "money", values = 150000}, third = {enable = false, type = "item", values = {5461, 1}}}},
        [6] = 	{questStarted = 1526, questStorage = 64026, killsRequired = 200, raceName = "Wyverns", rewards =			{first = {enable = true, type = "exp", values = 500000}, 		second = {enable = true, type = "money", values = 150000}, third = {enable = false, type = "item", values = {2520, 1}}}},
        [7] = 	{questStarted = 1527, questStorage = 64027, killsRequired = 50, raceName = "Nomads", rewards = 				{first = {enable = true, type = "exp", values = 16000}, 		second = {enable = true, type = "money", values = 10000}, third = {enable = false, type = "item", values = {5886, 1}}}},
        [8] = 	{questStarted = 1528, questStorage = 64028, killsRequired = 200, raceName = "Pirates", rewards = 			{first = {enable = true, type = "item", values = {6126, 35}}, second = {enable = true, type = "item", values = {6097, 35}}, third = {enable = true, type = "item", values = {6098, 35}}}},
        [9] = 	{questStarted = 1529, questStorage = 64029, killsRequired = 200, raceName = "Tomb Servants", rewards = 		{first = {enable = true, type = "exp", values = 100000}, 		second = {enable = false, type = "money", values = 200000}, third = {enable = true, type = "item", values = {13498, 1}}}},
        [10] = 	{questStarted = 1530, questStorage = 64030, killsRequired = 300, raceName = "Sandstone Scorpions", rewards = {first = {enable = true, type = "exp", values = 500000}, 		second = {enable = false, type = "money", values = 150000}, third = {enable = true, type = "item", values = {13535, 1}}}},
        [11] = 	{questStarted = 1531, questStorage = 64031, killsRequired = 300, raceName = "Golden Servants", rewards = 	{first = {enable = true, type = "exp", values = 800000}, 		second = {enable = true, type = "money", values = 250000}, third = {enable = true, type = "item", values = {13938, 1}}}},
        [12] = 	{questStarted = 1532, questStorage = 64032, killsRequired = 450, raceName = "Lava Golems", rewards = 		{first = {enable = true, type = "exp", values = 2500000}, 		second = {enable = true, type = "money", values = 350000}, third = {enable = true, type = "item", values = {18414, 10}}}},
        [13] = 	{questStarted = 1533, questStorage = 64033, killsRequired = 666, raceName = "Armadile", rewards = 			{first = {enable = true, type = "exp", values = 2500000}, 		second = {enable = true, type = "item", values = {18393, 1}}, third = {enable = true, type = "item", values = {18415, 35}}}}
}

local storage = 64522

local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}
local voc = {}

function onCreatureAppear(cid)                          npcHandler:onCreatureAppear(cid)                        end
function onCreatureDisappear(cid)                       npcHandler:onCreatureDisappear(cid)                     end
function onCreatureSay(cid, type, msg)                  npcHandler:onCreatureSay(cid, type, msg)                end
function onThink()                                      npcHandler:onThink()                                    end

function creatureSayCallback(cid, type, msg)

        local s = getPlayerStorageValue(cid, storage)

        if(not npcHandler:isFocused(cid)) then
                return false
        end
		local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
		local lol = tonumber(talkState[talkUser]) == nil and 0 or tonumber(talkState[talkUser])
		if(s < 0) or (s == "-0") then
			doPlayerSetStorageValue(cid, storage, 0)
			s=0
		end
		if (lol > 0 and msgcontains(msg, 'yes')) then
			doPlayerSetStorageValue(cid, tasks[lol].questStarted, 1)
			doPlayerSetStorageValue(cid, storage, lol)
			selfSay('You have started the task! In this task you need to kill ' .. tasks[lol].killsRequired .. ' ' .. tasks[lol].raceName .. '. Come back when you kill ' .. tasks[lol].killsRequired .. ' ' .. tasks[lol].raceName .. '!', cid)
			talkState[talkUser] = 0
		elseif(msgcontains(msg, 'task')) then
			if(string.lower(msg) == 'task') then
				selfSay('You can choose from the following missions:', cid)
				local bool = false
				for _,t in ipairs(tasks) do
					if(getPlayerStorageValue(cid, t.questStarted) <= 0) then
						bool = true
						selfSay('Say: {task '..t.raceName..'}, to accept the mission to kill '..t.killsRequired..' '..t.raceName..'.', cid)
					end
				end
				if bool == false then
					selfSay("You have done all tasks. Good work!",cid)
					doPlayerSetStorageValue(cid, 4379, 1)
				end
			elseif (s <= 0) then
				for k, t in ipairs(tasks) do
					if(string.lower("task "..t.raceName) == string.lower(msg)) then
						what_task = k
						break
					end
				end
				if(what_task) then
					if(getPlayerStorageValue(cid, tasks[what_task].questStarted) == 1) then
						selfSay('You are currently making task about ' .. tasks[what_task].raceName .. '.', cid)
					elseif(getPlayerStorageValue(cid, tasks[what_task].questStarted) == 2) then
						selfSay('You have done task about ' .. tasks[what_task].raceName .. '.', cid)
					else
						talkState[talkUser] = what_task
						selfSay('Are you sure you want to do this task? In this task you need to kill ' .. tasks[what_task].killsRequired .. ' ' .. tasks[what_task].raceName .. '.? ({yes})', cid)
					end
					return true
				else
					selfSay('Say: {task}.', cid)
				end
			elseif(getPlayerStorageValue(cid, tasks[s].questStarted) == 1) then
				selfSay('You are currently making task about ' .. tasks[s].raceName .. '.', cid)
			else
				selfSay('You have done task about ' .. tasks[s].raceName .. '.', cid)
			end
        elseif msgcontains(msg, 'report') then
                if tasks[s] and getPlayerStorageValue(cid,tasks[s].questStarted) == 1 then

                        if(getPlayerStorageValue(cid, tasks[s].questStorage) < 0) then
                                doPlayerSetStorageValue(cid, tasks[s].questStorage, 0)
                        end

                        if(getPlayerStorageValue(cid, tasks[s].questStorage) >= tasks[s].killsRequired) then
                                selfSay('Great!... you have finished the task about ' .. tasks[s].raceName .. '. Good job.', cid)
                                doPlayerSetStorageValue(cid, storage, 0)
                                doPlayerSetStorageValue(cid, tasks[s].questStarted, 2)
                                if(tasks[s].rewards.first.enable) then
                                        if(tasks[s].rewards.first.type == "boss") then
                                                doTeleportThing(cid, tasks[s].rewards.first.values)
                                        elseif(tasks[s].rewards.first.type == "exp") then
                                                doPlayerAddExperience(cid, tasks[s].rewards.first.values)
                                        elseif(tasks[s].rewards.first.type == "item") then
                                                doPlayerAddItem(cid, tasks[s].rewards.first.values[1], tasks[s].rewards.first.values[2])
                                        elseif(tasks[s].rewards.first.type == "money") then
                                                doPlayerAddMoney(cid, tasks[s].rewards.first.values)
                                        elseif(tasks[s].rewards.first.type == "storage") then
                                                doPlayerSetStorageValue(cid, tasks[s].rewards.first.values[1], tasks[s].rewards.first.values[2])
                                        end
                                end
                                if(tasks[s].rewards.second.enable) then
                                        if(tasks[s].rewards.second.type == "boss") then
                                                doTeleportThing(cid, tasks[s].rewards.second.values)
                                        elseif(tasks[s].rewards.second.type == "exp") then
                                                doPlayerAddExperience(cid, tasks[s].rewards.second.values)
                                        elseif(tasks[s].rewards.second.type == "item") then
                                                doPlayerAddItem(cid, tasks[s].rewards.second.values[1], tasks[s].rewards.second.values[2])
                                        elseif(tasks[s].rewards.second.type == "money") then
                                                doPlayerAddMoney(cid, tasks[s].rewards.second.values)
                                        elseif(tasks[s].rewards.second.type == "storage") then
                                                doPlayerSetStorageValue(cid, tasks[s].rewards.second.values[1], tasks[s].rewards.second.values[2])
                                        end
                                end
                                if(tasks[s].rewards.third.enable) then
                                        if(tasks[s].rewards.third.type == "boss") then
                                                doTeleportThing(cid, tasks[s].rewards.third.values)
                                        elseif(tasks[s].rewards.third.type == "exp") then
                                                doPlayerAddExperience(cid, tasks[s].rewards.third.values)
                                        elseif(tasks[s].rewards.third.type == "item") then
                                                doPlayerAddItem(cid, tasks[s].rewards.third.values[1], tasks[s].rewards.third.values[2])
                                        elseif(tasks[s].rewards.third.type == "money") then
                                                doPlayerAddMoney(cid, tasks[s].rewards.third.values)
                                        elseif(tasks[s].rewards.third.type == "storage") then
                                                doPlayerSetStorageValue(cid, tasks[s].rewards.third.values[1], tasks[s].rewards.third.values[2])
                                        end
                                end  
                        else
                                selfSay('Current ' .. getPlayerStorageValue(cid, tasks[s].questStorage) .. ' ' .. tasks[s].raceName .. ' killed, you need to kill ' .. tasks[s].killsRequired .. '.', cid)
                        end
				elseif tasks[s] and getPlayerStorageValue(cid,tasks[s].questStarted) == 2 then
						selfSay('You have done this task.', cid)
                else
                        selfSay('You do not have started any task.', cid)
                end
        end

end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new()) 