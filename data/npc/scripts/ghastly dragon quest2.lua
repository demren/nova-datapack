local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid)			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

function creatureSayCallback(cid, type, msg)
	if(not npcHandler:isFocused(cid)) then
		return false
	end

	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid

	if(msgcontains(msg, 'mission') or msgcontains(msg, 'serce')) then
		selfSay('Do you have 425 Ghastly Dragon Heads ?', cid)
		talkState[talkUser] = 1
	elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 1) then
		if getPlayerStorageValue(cid, 3578) ~= 2 then
		if(getPlayerItemCount(cid, 11366) >= 425) then
			if(doPlayerRemoveItem(cid, 11366, 425) == TRUE) then
				doPlayerSave(cid, true)
				local cap = db.storeQuery("SELECT `cap` FROM `players` WHERE `id` = '" .. getPlayerGUID(cid) .. "';")
				local cap = cap.getDataInt("cap")
				doPlayerSetMaxCapacity(cid, cap + 30)
                doSendMagicEffect(getPlayerPosition(cid), CONST_ME_GIFT_WRAPS)
				doPlayerAddItem(cid, 2160, 45)
				doPlayerAddItem(cid, 11367, 100)
				doPlayerAddExp(cid, 8500000)
				setCreatureMaxHealth(cid, (getCreatureMaxHealth(cid) + 20))
				doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You received 20 max health points, 8500000 exp, 45cc, 100x"..getItemNameById(11367)..",  and 30 capacity points.")
				selfSay('THX.', cid)
				setPlayerStorageValue(cid, 3578, 2)
			else
				selfSay('Sorry, you don\'t have enough gold.', cid)
			end
		else
			selfSay('Oh no ! You don\'t have enought heads !', cid)
		end
		else 
					selfSay('You done it already, Thanks !', cid)
end
		talkState[talkUser] = 0
	elseif(msgcontains(msg, 'no') and isInArray({1}, talkState[talkUser]) == TRUE) then
		talkState[talkUser] = 0
		selfSay('Ok, keep it for themselves...', cid)
	end

	return true
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())
