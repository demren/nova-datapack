local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local talkState = {}
local quest = 70002
local reward = 70000
	
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	if(not npcHandler:isFocused(cid)) then
		return false
	elseif getPlayerStorageValue(cid, 70001) > 0 then
		npcHandler:say("You are already working for dwarves! Get lost!", cid)
		talkState[talkUser] = 0
		return 0
	elseif msgcontains(msg, "yes") and talkState[talkUser] == 1 then
		npcHandler:say("Great! Go and find them in their disgusting mines! I'll reward you greatly {ok}?.", cid)
		talkState[talkUser] = 2
	elseif msgcontains(msg, "ok") and talkState[talkUser] == 2 then
		npcHandler:say("Okay, Hurry up!", cid)
		setPlayerStorageValue(cid, quest, 2)
		talkState[talkUser] = 0
	elseif msgcontains(msg, "mission") then
		local queststatus = getPlayerStorageValue(cid, quest)
		if(queststatus < 2) then
			npcHandler:say("Great, an adventurer. I need you to slay 200 dwarfs for me. Well, can you slay 200 dwarfs for me?", cid) 
			talkState[talkUser] = 1
			return true
		elseif(queststatus == 2) then
			npcHandler:say("You need to kill more dwarfs.", cid)
		elseif(queststatus == 3) then
			npcHandler:say("200 dwarfs, already? You're a true dwarfs slayer! As I promised, here's your reward.", cid)
			doPlayerAddItem(cid, 2160, 15)
			doPlayerAddItem(cid, 5921, 15)
			doPlayerAddExp(cid, 100000)
			setCreatureMaxHealth(cid, (getCreatureMaxHealth(cid) + 20))
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You received 20 health points.")
			setPlayerStorageValue(cid, quest, 4)
		elseif(queststatus == 4) then
			npcHandler:say("You have done enough for me, I will soon plan our attack!", cid)
		end
		talkState[talkUser] = 0
	end
	return TRUE
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())