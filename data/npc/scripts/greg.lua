local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

function creatureSayCallback(cid, type, msg)
	local storage1 = 4321
	local storage2 = 4320
	if (msgcontains(msg, "hello") or msgcontains(msg, "hi") or msgcontains(msg, "mission")) and not npcHandler:isFocused(cid) then
		if getCreatureHealth(cid) < 65 then
			npcHandler:say("Hello, "..getCreatureName(cid).."! You are looking really bad. Let me heal your wounds.", cid)
			doCreatureAddHealth(cid, 65 - getCreatureHealth(cid))
			doSendMagicEffect(getCreaturePosition(cid), 12)
		else
			npcHandler:say("Hello, "..getCreatureName(cid).."! I'll {heal} you if you are badly injured or poisoned.", cid)
		end
		if(getPlayerStorageValue(cid, storage1) < 4) then
			npcHandler:say("I have a {mission} for you.",cid)
		end
		npcHandler:addFocus(cid)
		return true
	end
	if(not npcHandler:isFocused(cid)) then
		return false
	end
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	local lol = tonumber(talkState[talkUser]) == nil and 0 or tonumber(talkState[talkUser])
	if msgcontains(msg, "yes") and lol == 1 then
		npcHandler:say("Awesome! I'll reward you greatly {ok}?", cid)
		talkState[talkUser] = 2
	elseif msgcontains(msg, "ok") and lol == 2 then
		npcHandler:say("Okay, Hurry up!", cid)
		setPlayerStorageValue(cid, storage1, 2)
		talkState[talkUser] = 0
	elseif msgcontains(msg, "mission") then
		local str = getPlayerStorageValue(cid, storage1)
		if(str < 2) then
			npcHandler:say("Great! Swamp trols have very valuable medicine pouch. Can you kill 50 swamp trolls for me? ({yes})", cid)
			talkState[talkUser] = 1
			return true
		elseif(str == 2) then
			npcHandler:say("Please come back for a reward.", cid)
		elseif(str == 3) then
			npcHandler:say("50 swamp trolls, already? Oh God thank you! As I promised, here's your reward. Now you should visit Gordon to get your armor and a weapon.", cid)
			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"Shop was marked on toyr mini map. You can buy armors and weapons there.")
			doPlayerAddMapMark(cid, {x=496, y=192,z=7}, 8, "Gordon. You can buy armors and weapons here.")
			doPlayerAddItem(cid, 2160, 2)
			setPlayerStorageValue(cid, storage1, 4)
		elseif(str == 4) then
			npcHandler:say("Thank you so much again!", cid)
		end
	elseif msgcontains(msg, "heal") then
		if getCreatureCondition(cid, CONDITION_FIRE) == TRUE then
			npcHandler:say("You are burning. I will help you.", cid)
			doRemoveCondition(cid, CONDITION_FIRE)
			doSendMagicEffect(getCreaturePosition(cid), 14)
		elseif getCreatureCondition(cid, CONDITION_POISON) == TRUE then
			npcHandler:say("You are poisoned. I will help you.", cid)
			doRemoveCondition(cid, CONDITION_POISON)
			doSendMagicEffect(getCreaturePosition(cid), 13)
		elseif getCreatureHealth(cid) < 65 then
			npcHandler:say("You are looking really bad. Let me heal your wounds.", cid)
			doCreatureAddHealth(cid, 65 - getCreatureHealth(cid))
			doSendMagicEffect(getCreaturePosition(cid), 12)
		else
			npcHandler:say("You aren't looking really bad, " .. getCreatureName(cid) .. ". I only help in cases of real emergencies. Raise your health simply by eating {food}.", cid)
		end
	end
	return true
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)