local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local talkState = {}
local quest = 2112
	
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	if(not npcHandler:isFocused(cid)) then
		return false
	elseif msgcontains(msg, "yes") and talkState[talkUser] == 1 then
		npcHandler:say("Great! I want you to bring me a Magic Plate Armor. {Ok}?.", cid)
		talkState[talkUser] = 2
	elseif msgcontains(msg, "ok") and talkState[talkUser] == 2 then
		npcHandler:say("Okay, Hurry up!", cid)
		setPlayerStorageValue(cid, quest, 2)
		talkState[talkUser] = 0
	elseif msgcontains(msg, "mission") then
		local str = getPlayerStorageValue(cid, quest)
		if(str < 2) then
			npcHandler:say("If you really want to enter this castle you will have to get blessed machete. I have one but you will have to do something for me first. Understood?{yes}", cid) 
			talkState[talkUser] = 1
			return true
		elseif str == 2 then
			if getPlayerItemCount(cid, 2472) >= 1 then
				doPlayerRemoveItem(cid, 2472, 1)
				npcHandler:say("Thank you! Here is your blessed machete.", cid)
				doItemSetAttribute(doPlayerAddItem(cid, 2420, 1), "aid", 2111)
				setPlayerStorageValue(cid, quest, 4)
			else
				npcHandler:say("You don't have it!", cid)
			end
		elseif(str == 4) then
			npcHandler:say("I can't help you.", cid)
		end
		talkState[talkUser] = 0
	end
	return TRUE
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())