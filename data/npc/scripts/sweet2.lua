local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

-- OTServ event handling functions start
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end
-- OTServ event handling functions end
function creatureSayCallback(cid, type, msg)
if(not npcHandler:isFocused(cid)) then
return false
end
if msgcontains(msg, 'help') then
selfSay('I can change "piece of draconian steel" for obsidian knife" .')

elseif msgcontains(msg, 'piece of draconian steel') then
if getPlayerItemCount(cid,5889) >= 1 then
selfSay('Did you bring me the piece of draconian steel ?')
talk_state = 1
else
selfSay('I need a piece of draconian steel, to give you the obsidian knife. Come back when you have them.')
talk_state = 0
end

elseif msgcontains(msg, 'yes') and talk_state == 1 then
talk_state = 0
if getPlayerItemCount(cid,5889) >= 1 then
if doPlayerRemoveItem(cid,5889, 1) == TRUE then
doPlayerAddItem(cid, 5908, 1)
selfSay('Here u are.')
end
else
selfSay(havent_item)
end




elseif msgcontains(msg, 'no') and (talk_state >= 1 and talk_state <= 5) then
selfSay('Ok than.')
talk_state = 0
end
-- Place all your code in here. Remember that hi, bye and all that stuff is already handled by the npcsystem, so you do not have to take care of that yourself.
return true
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new()) 