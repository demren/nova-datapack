local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid)			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

function creatureSayCallback(cid, type, msg)
	if(not npcHandler:isFocused(cid)) then
		return false
	end

	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid


	if msgcontains(msg, 'inquisitor') then
		selfSay('The churches of the gods entrusted me with the enormous and responsible task to lead the inquisition. I leave the field work to inquisitors who I recruit from fitting people that cross my way. If you wanna {join} in the inquisition, ask me why!' ,cid)
		talkState[talkUser] = 1
	
	elseif talkState[talkUser] == 1 then
		if msgcontains(msg, 'join') then
		selfSay('Do you want to join the inquisition?', cid)
		talkState[talkUser] = 2
		end
		
	elseif msgcontains(msg, 'mission') then
		if getPlayerStorageValue(cid,15200) == 1 then
			selfSay('Your mission is simple,You must enter in the retreat and destruct the Shadow Nexus. Then report to me about your mission.', cid)
			setPlayerStorageValue(cid,15200,2)
			setPlayerStorageValue(cid,15202,1)
		elseif getPlayerStorageValue(cid,15200) == 2 then
			selfSay('Are you so crazy? You don\'t have finished this mission.', cid)
		elseif getPlayerStorageValue(cid,15200) == 3 then
			selfSay('Did you destructed the shadow nexus?', cid)
			setPlayerStorageValue(cid,15200,4)
		elseif getPlayerStorageValue(cid,15200) == 5 then
			selfSay('Sorry, no missions yet!', cid)
		end

		elseif msgcontains(msg, 'yes') then
			if talkState[talkUser] == 2 then
			selfSay('So be it. Now you are a member of the inquisition. You might ask me for a {mission} to raise in my esteem.', cid)
			setPlayerStorageValue(cid,15200,1)
		elseif getPlayerStorageValue(cid,15200) == 4 then
			selfSay('Thank you! Now you can get just one reward in the north room.', cid)
			setPlayerStorageValue(cid,15200,5)
			setPlayerStorageValue(cid,15203,1)
		elseif talkState[talkUser] == 5 then
			selfSay('Ok, Good Looky. Then report to me about your mission.', cid)
			setPlayerStorageValue(cid,15201,4)
		end

	end

	return true
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())

