local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
-- OTServ event handling functions start
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end
-- OTServ event handling functions end
function creatureSayCallback(cid, type, msg)
if(not npcHandler:isFocused(cid)) then
return false
end
	if(msgcontains(msg, 'wooden stake')) then
		selfSay('Making a stake from a chair? Are you insane??! I won\'t waste my chairs on you for free! You will have to pay for it, but since I consider your plan a blasphemy, it will cost 10000 gold pieces. Okay? ', cid)
		talk_state = 1
	elseif(msgcontains(msg, 'yes') and talk_state == 1) then
			if(doPlayerRemoveMoney(cid, 10000) == TRUE) then
				doPlayerAddItem(cid, 5941)
				selfSay('Argh... my heart aches! Alright... a promise is a promise. Here - take this wooden stake, and now get lost.', cid)
			else
				selfSay('Sorry, you don\'t have enough gold.', cid)
			end
		talk_state = 0
	elseif(msgcontains(msg, 'no') and isInArray({1}, talk_state) == TRUE) then
        talk_state = 0
		selfSay('Ok then.', cid)
	end
	return true
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())