local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}
function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid)			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end
function creatureSayCallback(cid, type, msg)
	if(not npcHandler:isFocused(cid)) then
		return false
	end
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	if(msgcontains(msg, 'beer')) then
		selfSay('Do you want to buy beer? Will pay only 2gp!', cid)
		talkState[talkUser] = 1
	elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 1) then
		if(doPlayerRemoveMoney(cid, 2) == TRUE) then
			doPlayerAddItem(cid, 2006, 3)
				selfSay('Here you are, this is you beer. You can also {order} something else or {buy} food.', cid)
			else
				selfSay('Sorry, you don\'t have enough gold.', cid)
             end
	elseif(msgcontains(msg, 'no') and talkState[talkUser] == 1) then
				selfSay('Ok then, maybe you want to {order} different drink or {buy} something to eat?', cid)
				talkState[talkUser] = 0
			     		 
return true
end
----------------------------------------
	if(msgcontains(msg, 'wine')) then
		selfSay('Do you want to buy wine? Will pay only 3gp!', cid)
		talkState[talkUser] = 2
	elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 2) then
		if(doPlayerRemoveMoney(cid, 3) == TRUE) then
			doPlayerAddItem(cid, 2006, 15)
				selfSay('Here you are, this is you wine. You can also {order} something else or {buy} food.', cid)
			else
				selfSay('Sorry, you don\'t have enough gold.', cid)
		     end
	elseif(msgcontains(msg, 'no') and talkState[talkUser] == 2) then
				selfSay('Ok then, maybe you want to {order} different drink or {buy} something to eat?', cid)
				talkState[talkUser] = 0	     
return true
end
----------------------------------------
	if(msgcontains(msg, 'milk')) then
		selfSay('Do you want to buy milk? Will pay only 4gp!', cid)
		talkState[talkUser] = 3
	elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 3) then
		if(doPlayerRemoveMoney(cid, 4) == TRUE) then
			doPlayerAddItem(cid, 2006, 6)
				selfSay('Here you are, this is you milk. You can also {order} something else or {buy} food.', cid)
			else
				selfSay('Sorry, you don\'t have enough gold.', cid)
		    end
	elseif(msgcontains(msg, 'no') and talkState[talkUser] == 3) then
				selfSay('Ok then, maybe you want to {order} different drink or {buy} something to eat?', cid)
				talkState[talkUser] = 0		
return true
end
----------------------------------------
	if(msgcontains(msg, 'lemonade')) then
		selfSay('Do you want to buy lemonade? Will pay only 2gp!', cid)
		talkState[talkUser] = 4
	elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 4) then
		if(doPlayerRemoveMoney(cid, 2) == TRUE) then
			doPlayerAddItem(cid, 2006, 5)
				selfSay('Here you are, this is you lemonade. You can also {order} something else or {buy} food.', cid)
			else
				selfSay('Sorry, you don\'t have enough gold.', cid)
		     end
	elseif(msgcontains(msg, 'no') and talkState[talkUser] == 4) then
			selfSay('Ok then, maybe you want to {order} different drink or {buy} something to eat?', cid)
			talkState[talkUser] = 0
return true
end
----------------------------------------
	if(msgcontains(msg, 'water')) then
		selfSay('Do you want to buy water? Will pay only 1gp!', cid)
		talkState[talkUser] = 5
	elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 5) then
		if(doPlayerRemoveMoney(cid, 1) == TRUE) then
			doPlayerAddItem(cid, 2006, 1)
				selfSay('Here you are, this is you water. You can also {order} something else or {buy} food.', cid)
			else
				selfSay('Sorry, you don\'t have enough gold.', cid)
		end
	elseif(msgcontains(msg, 'no') and talkState[talkUser] == 5) then
			selfSay('Ok then, maybe you want to {order} different drink or {buy} something to eat?', cid)
			talkState[talkUser] = 0
	     end
return true
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())
