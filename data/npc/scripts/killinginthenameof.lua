local tasks =
{
        [1] = 	{questStarted = 1501, questStorage = 65001, killsRequired = 30, raceName = "Trolls", rewards = 					{first = {enable = true, type = "exp", values = 1000}, 		second = {enable = true, type = "money", values = 10000}, third = {enable = true, type = "item", values = {2463, 1}}}},
        [2] = 	{questStarted = 1502, questStorage = 65002, killsRequired = 50, raceName = "Rotworms", rewards = 				{first = {enable = true, type = "exp", values = 5000}, 		second = {enable = true, type = "money", values = 20000}, third = {enable = false, type = "storage", values = {66600, 15}}}},
        [3] = 	{questStarted = 1503, questStorage = 65003, killsRequired = 80, raceName = "Cyclops", rewards = 				{first = {enable = true, type = "exp", values = 10000}, 	second = {enable = true, type = "money", values = 30000}, third = {enable = false, type = "storage", values = {66600, 15}}}},
        [4] = 	{questStarted = 1504, questStorage = 65004, killsRequired = 100, raceName = "Dragons", rewards = 				{first = {enable = true, type = "exp", values = 120000}, 	second = {enable = true, type = "money", values = 50000}, third = {enable = true, type = "item", values = {5461, 1}}}},
        [5] = 	{questStarted = 1505, questStorage = 65005, killsRequired = 100, raceName = "Quaras", rewards = 				{first = {enable = true, type = "exp", values = 150000}, 	second = {enable = true, type = "money", values = 60000}, third = {enable = true, type = "item", values = {2520, 1}}}},
        [6] = 	{questStarted = 1506, questStorage = 65006, killsRequired = 150, raceName = "Giant Spiders", rewards = 			{first = {enable = true, type = "exp", values = 160000}, 	second = {enable = true, type = "money", values = 180000}, third = {enable = true, type = "item", values = {5886, 1}}}},
        [7] = 	{questStarted = 1507, questStorage = 65007, killsRequired = 200, raceName = "Hydras", rewards = 				{first = {enable = true, type = "exp", values = 200000}, 	second = {enable = true, type = "money", values = 200000}, third = {enable = false, type = "storage", values = {66600, 25}}}},
        [8] = 	{questStarted = 1508, questStorage = 65008, killsRequired = 200, raceName = "Serpent Spawns", rewards = 		{first = {enable = true, type = "exp", values = 800000}, 	second = {enable = true, type = "money", values = 150000}, third = {enable = true, type = "item", values = {2470, 1}}}},
        [9] = 	{questStarted = 1509, questStorage = 65009, killsRequired = 300, raceName = "Behemoths", rewards = 				{first = {enable = true, type = "exp", values = 800000}, 	second = {enable = true, type = "money", values = 350000}, third = {enable = true, type = "item", values = {7396, 1}}}},
        [10] = 	{questStarted = 1510, questStorage = 65010, killsRequired = 250, raceName = "Medusas", rewards = 				{first = {enable = true, type = "exp", values = 1000000}, 	second = {enable = true, type = "money", values = 350000}, third = {enable = true, type = "item", values = {6578, 1}}}},
        [11] = 	{questStarted = 1511, questStorage = 65011, killsRequired = 666, raceName = "Demons", rewards = 				{first = {enable = true, type = "exp", values = 6660000}, 	second = {enable = true, type = "item", values = {2495, 1}}, third = {enable = true, type = "item", values = {10518, 1}}}}
}

local storage = 64521

local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}
local voc = {}

function onCreatureAppear(cid)                          npcHandler:onCreatureAppear(cid)                        end
function onCreatureDisappear(cid)                       npcHandler:onCreatureDisappear(cid)                     end
function onCreatureSay(cid, type, msg)                  npcHandler:onCreatureSay(cid, type, msg)                end
function onThink()                                      npcHandler:onThink()                                    end

function creatureSayCallback(cid, type, msg)

        local s = getPlayerStorageValue(cid, storage)

        if(not npcHandler:isFocused(cid)) then
                return false
        end
		local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
		local lol = tonumber(talkState[talkUser]) == nil and 0 or tonumber(talkState[talkUser])
		if(s < 0) or (s == "-0") then
			doPlayerSetStorageValue(cid, storage, 0)
			s=0
		end
		if (lol > 0 and msgcontains(msg, 'yes')) then
			doPlayerSetStorageValue(cid, tasks[lol].questStarted, 1)
			doPlayerSetStorageValue(cid, storage, lol)
			selfSay('You have started the task! In this task you need to kill ' .. tasks[lol].killsRequired .. ' ' .. tasks[lol].raceName .. '. Come back when you kill ' .. tasks[lol].killsRequired .. ' ' .. tasks[lol].raceName .. '!', cid)
			talkState[talkUser]=0
		elseif(msgcontains(msg, 'task')) then
			if(string.lower(msg) == 'task') then
				selfSay('You can choose from the following missions:', cid)
				local bool = false
				for _,t in ipairs(tasks) do
					if(getPlayerStorageValue(cid, t.questStarted) <= 0) then
						bool = true
						selfSay('Say: {task '..t.raceName..'}, to accept the mission to kill '..t.killsRequired..' '..t.raceName..'.', cid)
					end
				end
				if bool == false then
					selfSay("You have done all tasks. Good work!",cid)
					doPlayerSetStorageValue(cid, 4378, 1)
				end
			elseif (s <= 0) then
				for k, t in ipairs(tasks) do
					if(string.lower("task "..t.raceName) == string.lower(msg)) then
						what_task = k
						break
					end
				end
				if(what_task) then
					if(getPlayerStorageValue(cid, tasks[what_task].questStarted) == 1) then
						selfSay('You are currently making task about ' .. tasks[what_task].raceName .. '.', cid)
					elseif(getPlayerStorageValue(cid, tasks[what_task].questStarted) == 2) then
						selfSay('You have done task about ' .. tasks[what_task].raceName .. '.', cid)
					else
						talkState[talkUser] = what_task
						selfSay('Are you sure you want to do this task? In this task you need to kill ' .. tasks[what_task].killsRequired .. ' ' .. tasks[what_task].raceName .. '.? ({yes})', cid)
					end
					return true
				else
					selfSay('Say: {task}.', cid)
				end
			elseif(getPlayerStorageValue(cid, tasks[s].questStarted) == 1) then
				selfSay('You are currently making task about ' .. tasks[s].raceName .. '.', cid)
			else
				selfSay('You have done task about ' .. tasks[s].raceName .. '.', cid)
			end
        elseif msgcontains(msg, 'report') then
                if tasks[s] and getPlayerStorageValue(cid,tasks[s].questStarted) == 1 then

                        if(getPlayerStorageValue(cid, tasks[s].questStorage) < 0) then
                                doPlayerSetStorageValue(cid, tasks[s].questStorage, 0)
                        end

                        if(getPlayerStorageValue(cid, tasks[s].questStorage) >= tasks[s].killsRequired) then
                                selfSay('Great!... you have finished the task about ' .. tasks[s].raceName .. '. Good job.', cid)
                                doPlayerSetStorageValue(cid, storage, 0)
                                doPlayerSetStorageValue(cid, tasks[s].questStarted, 2)
                                if(tasks[s].rewards.first.enable) then
                                        if(tasks[s].rewards.first.type == "boss") then
                                                doTeleportThing(cid, tasks[s].rewards.first.values)
                                        elseif(tasks[s].rewards.first.type == "exp") then
                                                doPlayerAddExperience(cid, tasks[s].rewards.first.values)
                                        elseif(tasks[s].rewards.first.type == "item") then
                                                doPlayerAddItem(cid, tasks[s].rewards.first.values[1], tasks[s].rewards.first.values[2])
                                        elseif(tasks[s].rewards.first.type == "money") then
                                                doPlayerAddMoney(cid, tasks[s].rewards.first.values)
                                        elseif(tasks[s].rewards.first.type == "storage") then
                                                doPlayerSetStorageValue(cid, tasks[s].rewards.first.values[1], tasks[s].rewards.first.values[2])
                                        end
                                end
                                if(tasks[s].rewards.second.enable) then
                                        if(tasks[s].rewards.second.type == "boss") then
                                                doTeleportThing(cid, tasks[s].rewards.second.values)
                                        elseif(tasks[s].rewards.second.type == "exp") then
                                                doPlayerAddExperience(cid, tasks[s].rewards.second.values)
                                        elseif(tasks[s].rewards.second.type == "item") then
                                                doPlayerAddItem(cid, tasks[s].rewards.second.values[1], tasks[s].rewards.second.values[2])
                                        elseif(tasks[s].rewards.second.type == "money") then
                                                doPlayerAddMoney(cid, tasks[s].rewards.second.values)
                                        elseif(tasks[s].rewards.second.type == "storage") then
                                                doPlayerSetStorageValue(cid, tasks[s].rewards.second.values[1], tasks[s].rewards.second.values[2])
                                        end
                                end
                                if(tasks[s].rewards.third.enable) then
                                        if(tasks[s].rewards.third.type == "boss") then
                                                doTeleportThing(cid, tasks[s].rewards.third.values)
                                        elseif(tasks[s].rewards.third.type == "exp") then
                                                doPlayerAddExperience(cid, tasks[s].rewards.third.values)
                                        elseif(tasks[s].rewards.third.type == "item") then
                                                doPlayerAddItem(cid, tasks[s].rewards.third.values[1], tasks[s].rewards.third.values[2])
                                        elseif(tasks[s].rewards.third.type == "money") then
                                                doPlayerAddMoney(cid, tasks[s].rewards.third.values)
                                        elseif(tasks[s].rewards.third.type == "storage") then
                                                doPlayerSetStorageValue(cid, tasks[s].rewards.third.values[1], tasks[s].rewards.third.values[2])
                                        end
                                end  
                        else
                                selfSay('Current ' .. getPlayerStorageValue(cid, tasks[s].questStorage) .. ' ' .. tasks[s].raceName .. ' killed, you need to kill ' .. tasks[s].killsRequired .. '.', cid)
                        end
				elseif tasks[s] and getPlayerStorageValue(cid,tasks[s].questStarted) == 2 then
						selfSay('You have done this task.', cid)
                else
                        selfSay('You do not have started any task.', cid)
                end
        end

end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new()) 