local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local Topic = {}
local storage = 21004

function onCreatureAppear(cid)              npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid)           npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg)  npcHandler:onCreatureSay(cid, type, msg) end
function onThink()                      npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
    local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
    if msgcontains(msg, "hi") or msgcontains(msg, "hello") and (not npcHandler:isFocused(cid)) then
        if getPlayerStorageValue(cid, storage) < 1 then
            npcHandler:say("Please {help} me!", cid, TRUE)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            npcHandler:say("Have you brought a coconut shoes for me?", cid, TRUE)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            npcHandler:say("Oh hello, i\'m still working on that coconut shoes.", cid, TRUE)
            Topic[talkUser] = 0
        end
        npcHandler:addFocus(cid)
        return true
    end
    if(not npcHandler:isFocused(cid)) then
        return false
    end
    if msgcontains(msg, "bye") or msgcontains(msg, "farewell") then
        npcHandler:say("Good bye.", cid, TRUE)
        Topic[talkUser] = 0
        npcHandler:releaseFocus(cid)
	elseif msgcontains(msg, "quest") or msgcontains(msg, "mission") or msgcontains(msg, "task") or msgcontains(msg, "help") or (msgcontains(msg, "shoes") and getPlayerStorageValue(cid, storage) > 0) then
        if getPlayerStorageValue(cid, storage) < 1 then
            npcHandler:say("I need you to bring me a coconut shoes. I need them to my experiments. You can find them somewhere in dworces island. Find them for me!", cid)
            setPlayerStorageValue(cid, storage, 1)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            npcHandler:say("Have you brought a coconut shoes for me?", cid, TRUE)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            npcHandler:say("Oh hello, i\'m still working on that coconut shoes.", cid, TRUE)
            Topic[talkUser] = 0
        end
    elseif Topic[talkUser] == 1 then
        if msgcontains(msg, "yes") then
            if getPlayerItemCount(cid, 9931) >= 1 then
                doPlayerRemoveItem(cid, 9931, 1)
                npcHandler:say("Oh please take this demon shield as a reward.", cid)
                doPlayerAddItem(cid, 2520, 1)
                setPlayerStorageValue(cid, storage, 2)
            else
                npcHandler:say("Still haven\'t found them? Keep looking in dworces land.", cid)
            end
        else
            npcHandler:say("Still haven\'t found them? Keep looking in dworces land.", cid)
        end
        Topic[talkUser] = 0
    end
    return TRUE
end

npcHandler:setMessage(MESSAGE_WALKAWAY, "Good bye.")
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)