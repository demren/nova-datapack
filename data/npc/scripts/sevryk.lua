local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local Topic = {}
local storage = 22005

function onCreatureAppear(cid)              npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid)           npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg)  npcHandler:onCreatureSay(cid, type, msg) end
function onThink()                      npcHandler:onThink() end

local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)

shopModule:addSellableItem({'marijuana'}, 5953, 5500, 'marijuana')
shopModule:addSellableItem({'mystic herb'}, 11245, 4000, 'mystic herb')

function creatureSayCallback(cid, type, msg)
    local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
    if msgcontains(msg, "hi") or msgcontains(msg, "hello") and (not npcHandler:isFocused(cid)) then
        if getPlayerStorageValue(cid, storage) < 1 then
            selfSay("Hello there! I have a {job} for you!", cid)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("Did you bring three mystical herbs for me?", cid)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            selfSay("Thank you so much!", cid)
            Topic[talkUser] = 0
        end
        npcHandler:addFocus(cid)
        return true
    end
    if(not npcHandler:isFocused(cid)) then
        return false
    end
    if msgcontains(msg, "bye") or msgcontains(msg, "farewell") then
        selfSay("Good bye.", cid)
        Topic[talkUser] = 0
        npcHandler:releaseFocus(cid)
	elseif msgcontains(msg, "job") or msgcontains(msg, "mission") or msgcontains(msg, "task") or msgcontains(msg, "help") or msgcontains(msg, "job") and getPlayerStorageValue(cid, storage) > 0 then
        if getPlayerStorageValue(cid, storage) < 1 then
           selfSay("Have you seen those flowers in my yard? Exactly the same grow in earth elementals ungergrounds. Find them and get some for me. You have to use golden sickle to cut them!", cid)
            setPlayerStorageValue(cid, storage, 1)
            Topic[talkUser] = 0
        elseif getPlayerStorageValue(cid, storage) == 1 then
            selfSay("Have you brought a mystical herbs for me?", cid)
            Topic[talkUser] = 1
        elseif getPlayerStorageValue(cid, storage) == 2 then
            selfSay("Thank you again.", cid)
            Topic[talkUser] = 0
        end
    elseif Topic[talkUser] == 1 then
        if msgcontains(msg, "yes") then
            if getPlayerItemCount(cid, 11245) >= 3 then
                doPlayerRemoveItem(cid, 11245, 3)
                selfSay("Here is your reward", cid)
                doPlayerAddItem(cid, 2160, 30)
                setPlayerStorageValue(cid, storage, 2)
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You have been rewarded with the barbarian addon and 30 cc!")
            else
                selfSay("Still haven\'t found them? Keep looking in earth elementals land.", cid)
            end
        else
            selfSay("Still haven\'t found them? Keep looking in earth elementals land.", cid)
        end
        Topic[talkUser] = 0
    end
    return TRUE
end

npcHandler:setMessage(MESSAGE_WALKAWAY, "Good bye.")
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)