local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local talkState = {}
local quest = 9070
local reward = 70000
	
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	if(not npcHandler:isFocused(cid)) then
		return false
	elseif msgcontains(msg, "yes") and talkState[talkUser] == 1 then
		npcHandler:say("Awesome! I'll reward you greatly {ok}?", cid)
		talkState[talkUser] = 2
	elseif msgcontains(msg, "ok") and talkState[talkUser] == 2 then
		npcHandler:say("Okay, Hurry up!", cid)
		setPlayerStorageValue(cid, quest, 2)
		talkState[talkUser] = 0
	elseif msgcontains(msg, "mission") then
		local str = getPlayerStorageValue(cid, quest)
		local ppos1 = {x=536, y=151, z=8}
		local ppos2 = {x=539, y=154, z=8}
		local ppos3 = {x=542, y=157, z=8}
		local ppos4 = {x=540, y=157, z=8}
		local ppos5 = {x=539, y=158, z=8}
		local ppos6 = {x=541, y=154, z=8}
		if(str < 2) then
			npcHandler:say("Great! This stinking trolls, again in my basement. Please kill 5 stinking trolls!", cid) 
			-- wtf? why nie wjebalem tego w tablice i nie puscilem tego w for? Affowo to wyglada
				doSummonCreature("stinking troll", ppos1)
				doSummonCreature("stinking troll", ppos2)
				doSummonCreature("stinking troll", ppos3)
				doSummonCreature("stinking troll", ppos4)
				doSummonCreature("stinking troll", ppos5)
				doSummonCreature("stinking troll", ppos6)
			setPlayerStorageValue(cid, quest, 2)
			talkState[talkUser] = 1
			return true
		elseif(str == 2) then
			npcHandler:say("Please come back for a reward.", cid)
		elseif(str == 3) then
			npcHandler:say("5 stinking trolls, already? Oh God thank you! As I promised, here's your reward.", cid)
			doPlayerAddItem(cid, 2160, 1)
			doPlayerAddExp(cid, 10000)
			setPlayerStorageValue(cid, quest, 4)
		elseif(str == 4) then
			npcHandler:say("Thank you so much again!", cid)
		end
		talkState[talkUser] = 0
	end
	return TRUE
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())