local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local talkState = {}
local quest = 26301
	
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	local str = getPlayerStorageValue(cid, quest)
	if(not npcHandler:isFocused(cid)) then
		return false
	elseif msgcontains(msg, "misja") then
		if(str == -1) then
			npcHandler:say("W mojej piwnicy roji sie od Troli. Zabij 5 troli z mojej piwnicy, a w zamian dostaniesz 200 gp, 300 expa i 10 mikstur zycia. {ok}?", cid)
			talkState[talkUser] = 2
		elseif(str == 1) then
			npcHandler:say("Idz zabij 5 troli w w mojej piwnicy i wroc do mnie, dostaniesz 200 gp, 300 expa i 10 mikstur zycia.", cid)
		elseif(str == 2) then
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Otrzymales 200gp, 300 exp i 20x small health potion.")
			doPlayerAddExperience(cid, 300)
			doPlayerAddMoney(cid, 200)
			doPlayerAddItem(cid, 8704, 10)
			setPlayerStorageValue(cid, quest, 3)
			npcHandler:say("Wielkie dzieki!! Z wyspy poczatkujacych mozesz wyjsc dopiero po ukonczeniu 10 poziomu. Jesli brakuje Ci jeszcze troche doswiadczenia do tego levela, polecam Ci wyjsc poza miasto i zdobyc kilka poziomow na silniejszych potworach.", cid)
			npcHandler:say("Kiedy bedziesz mial malo zycia polecam uzywac mikstur, ktore dostales ode mnie. Zaznaczam Ci na mapie wyjscia z miasta. Mikstury mozesz kupic w miejsu oznaczona flaga na mapie.", cid)
			doPlayerAddMapMark(cid, {x=2113-1282,y=1919-1422,z=7}, 9, "Potions.")
			doPlayerAddMapMark(cid, {x=2122-1282,y=1924-1422,z=7}, 9, "Potions.")
			doPlayerAddMapMark(cid, {x=2065-1282,y=1893-1422,z=7}, 17, "Zachodnie wyjscie z miasta.")
			doPlayerAddMapMark(cid, {x=2112-1282,y=1926-1422,z=7}, 15, "Poludniowe wyjscie z miasta (snake, wolf, bear, troll)")
			doPlayerAddMapMark(cid, {x=2129-1282,y=1920-1422,z=7}, 15, "Poludniowe wyjscie z miasta (snake, wolf, bear, troll)")
		elseif(str == 3) then
			npcHandler:say("Juz wykonales u mnie misje. Wielkie dzieki!", cid)
		end
	elseif msgcontains(msg, "ok") and str == -1 and talkState[talkUser] == 2 then
		npcHandler:say("W takim razie do pracy! Jak skonczysz powiadom mnie o tym.", cid)
		setPlayerStorageValue(cid, quest, 1)
		talkState[talkUser] = 0
	else
		npcHandler:say("Nie wiem o czym mowisz ... Napisz {misja}, aby dowiedziec sie czegos o misji.", cid)
		talkState[talkUser] = 0
	end
	return TRUE
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())