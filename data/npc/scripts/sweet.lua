local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end
function creatureSayCallback(cid, type, msg)
if(not npcHandler:isFocused(cid)) then
return false
end
local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
if(msgcontains(msg, 'help')) then
selfSay('You can here change some items for "hunting horn", "spool of yarn", "huge chunk of crude iron", "pieces of draconian steel", "warrior\'s sweat", "magic sulphur", "enchanted chicken wing", "piece of royal steel", "pieces of hell steel", "engraved crossbow", "fighting spirit", "infernal bolt".', cid)
end

if(msgcontains(msg, 'huge chunk of crude iron')) then
	selfSay('I need a 3 Bast Skirts and Giant Sword, to give you the huge chunk of crude iron.', cid)
	talkState[talkUser] = 1
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 1) then
	if(doPlayerRemoveItem(cid, 3983, 3) == TRUE) then
		doPlayerRemoveItem(cid, 2393, 1)
		doPlayerAddItem(cid, 5892, 1)
		selfSay('Here you are, this is you huge chunk of crude iron.', cid)
	else
		selfSay('Sorry, you don\'t have enough items.', cid)
	end
	return true
end
if(msgcontains(msg, 'hunting horn')) then
	selfSay('I need a {Post horn} and 5x {Rope belt} to give you the hunting horn.', cid)
	talkState[talkUser] = 1
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 1) then
	if(doPlayerRemoveItem(cid, 3983, 3) == TRUE) then
		doPlayerRemoveItem(cid, 2393, 1)
		doPlayerAddItem(cid, 5892, 1)
		selfSay('Here you are, this is you huge chunk of crude iron.', cid)
	else
		selfSay('Sorry, you don\'t have enough items.', cid)
	end
	return true
end
----------------------------------------
if(msgcontains(msg, 'warrior\'s sweat')) then
selfSay('I need 4 warrior helmets, to give you the warrior\'s sweat.', cid)
talkState[talkUser] = 2
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 2) then
if(doPlayerRemoveItem(cid, 2475, 4) == TRUE) then
doPlayerAddItem(cid, 5885, 1)
selfSay('Here you are, this is you warrior\'s sweat.', cid)
else
selfSay('Sorry, you don\'t have enough items.', cid)
end
return true
end
----------------------------------------
if(msgcontains(msg, 'infernal bolt')) then
selfSay('I need a Soul Orb, to give you the 2 Infernal Bolts.', cid)
talkState[talkUser] = 4
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 4) then
if(doPlayerRemoveItem(cid, 5944, 1) == TRUE) then
doPlayerAddItem(cid, 6529, 2)
selfSay('Here you are, this is you infernal bolt.', cid)
else
selfSay('Sorry, you don\'t have enough items.', cid)
end
return true
end
----------------------------------------
if(msgcontains(msg, 'fighting spirit')) then
selfSay('I need a 2 Royal Helmets, to give you the Fighting Spirit.', cid)
talkState[talkUser] = 6
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 6) then
if(doPlayerRemoveItem(cid, 2498, 2) == TRUE) then
doPlayerAddItem(cid, 5884, 1)
selfSay('Here you are, this is you fighting spirit.', cid)
else
selfSay('Sorry, you don\'t have enough items.', cid)
end
return true
end
----------------------------------------
if(msgcontains(msg, 'piece of royal steel')) then
selfSay('I need a 3 Bast Skirts and Crown Armor, to give you the piece of royal steel.', cid)
talkState[talkUser] = 7
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 7) then
if(doPlayerRemoveItem(cid, 3983, 3) == TRUE) then
doPlayerRemoveItem(cid, 2487, 1)
doPlayerAddItem(cid, 5887, 1)
selfSay('Here you are, this is you piece of royal steel.', cid)
else
selfSay('Sorry, you don\'t have enough items.', cid)
end
return true
end
----------------------------------------
if(msgcontains(msg, 'pieces of draconian steel')) then
selfSay('I need a 3 Bast Skirts and Dragon Shield, to give you the pieces of draconian steel.', cid)
talkState[talkUser] = 8
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 8) then
if(doPlayerRemoveItem(cid, 3983, 3) == TRUE) then
doPlayerRemoveItem(cid, 2516, 1)
doPlayerAddItem(cid, 5889, 1)
selfSay('Here you are, this is you pieces of draconian steel.', cid)
else
selfSay('Sorry, you don\'t have enough items.', cid)
end
return true
end
----------------------------------------
if(msgcontains(msg, 'pieces of hell steel')) then
selfSay('I need a 3 Bast Skirts and Devil Helmet, to give you the pieces of hell steel.', cid)
talkState[talkUser] = 9
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 9) then
if(doPlayerRemoveItem(cid, 3983, 3) == TRUE) then
doPlayerRemoveItem(cid, 2462, 1)
doPlayerAddItem(cid, 5888, 1)
selfSay('Here you are, this is you pieces of hell steel.', cid)
else
selfSay('Sorry, you don\'t have enough items.', cid)
end
return true
end
----------------------------------------
if(msgcontains(msg, 'magic sulphur')) then
selfSay('I need a 3 fire swords, to give you the magic sulphur.', cid)
talkState[talkUser] = 11
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 11) then
if(doPlayerRemoveItem(cid, 2392, 3) == TRUE) then
doPlayerAddItem(cid, 5904, 1)
selfSay('Here you are, this is you magic sulphur.', cid)
else
selfSay('Sorry, you don\'t have enough items.', cid)
end
return true
end
----------------------------------------
if(msgcontains(msg, 'engraved crossbow')) then
selfSay('I need a 15 demon horns, to give you the engraved crossbow.', cid)
talkState[talkUser] = 12
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 12) then
if(doPlayerRemoveItem(cid, 5954, 15) == TRUE) then
doPlayerAddItem(cid, 5947, 1)
selfSay('Here you are, this is you engraved crossbow.', cid)
else
selfSay('Sorry, you don\'t have enough items.', cid)
end
return true
end
----------------------------------------
if(msgcontains(msg, 'enchanted chicken wing')) then
selfSay('I need a 1 boots of haste, to give you the enchanted chicken wing.', cid)
talkState[talkUser] = 13
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 13) then
if(doPlayerRemoveItem(cid, 2195, 1) == TRUE) then
doPlayerAddItem(cid, 5891, 1)
selfSay('Here you are, this is you enchanted chicken wing.', cid)
else
selfSay('Sorry, you don\'t have enough items.', cid)
end
return true
end
----------------------------------------
if(msgcontains(msg, 'spool of yarn')) then
selfSay('I need a 10 Giant Spider Silks, to give you the Spools Of Yarn.', cid)
talkState[talkUser] = 10
elseif(msgcontains(msg, 'yes') and talkState[talkUser] == 10) then
if(doPlayerRemoveItem(cid, 5879, 10) == TRUE) then
doPlayerAddItem(cid, 5886, 1)
selfSay('Here you are, this is you spool of yarn.', cid)
else
selfSay('Sorry, you don\'t have items.', cid)
end
elseif(msgcontains(msg, 'no') and isInArray({1}, talkState[talkUser]) == TRUE) then
talkState[talkUser] = 0
selfSay('Ok then.', cid)
end
return true
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new()) 