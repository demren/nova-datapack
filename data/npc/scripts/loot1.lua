local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid) 			end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid) 			end
function onCreatureSay(cid, type, msg) 			npcHandler:onCreatureSay(cid, type, msg) 		end
function onThink() 					npcHandler:onThink() 					end

-- Don't forget npcHandler = npcHandler in the parameters. It is required for all StdModule functions!
keywordHandler:addKeyword({'helmets'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'I buy chain (35 gp), steel (345 gp), legion  (70 gp), viking (65 gp) and brass (40 gp).'})
keywordHandler:addKeyword({'boots'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'I buy leather boots (5 gp).'})
keywordHandler:addKeyword({'armors'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'I buy plate (400 gp), brass (200 gp), chain (100 gp), scale (950 gp).'})
keywordHandler:addKeyword({'legs'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'I buy plate (500 gp), chain legs (130 gp), and brass (80 gp).'})
keywordHandler:addKeyword({'shields'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'I buy plate (65 gp), copper (75 gp), battle (170 gp), dragon (12000 gp), dwarven (100 gp) and studded shields (52 gp).'})
keywordHandler:addKeyword({'swords'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'I buy katana (50 gp), wyvern fang (4500 gp), serpent (4500 gp), spike (4800 gp) , broad swords (710 gp), short swords (30 gp), sabres (25gp) and swords (25gp).'})
keywordHandler:addKeyword({'axes'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'I buy double (200gp), battle axes (100gp), halberds (310 gp), hatchets (20gp) and axe (30 gp).'})
keywordHandler:addKeyword({'clubs'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'I buy mace (35 gp), dragonbone staff (8000 gp), dragon hammer (6000 gp) and battle hammers (160 gp).'})
keywordHandler:addKeyword({'wands'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'I buy wand of inferno (6000 gp).'})


npcHandler:addModule(FocusModule:new())
