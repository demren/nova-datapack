local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local talkState = {}
local quest = 13007
local items = {{5897, 50}, {5896, 50}}
local women = 148
local men = 144
local addon = 1
local addon_name = "druid"
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end

function creatureSayCallback(cid, type, msg)
	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	if(not npcHandler:isFocused(cid)) then
		return false
	elseif msgcontains(msg, addon_name) then
		txt = ""
		for k, v in ipairs(items) do
			txt = txt..v[2].." "..getItemNameById(v[1])..(k == #items and "" or ", ")
		end
		npcHandler:say("Great! Did you bring me "..txt.." for "..(addon == 1 and "first" or "second").." "..addon_name.." addon? {ok}?.", cid)
		talkState[talkUser] = 2
	elseif ((msgcontains(msg, "ok")) or msgcontains(msg, "yes")) and talkState[talkUser] == 2 then
		if getPlayerStorageValue(cid, quest) == 1 then
			npcHandler:say("You have arleady done this mission.", cid)
		else
			bool = true
			for k, v in ipairs(items) do
				if not (getPlayerItemCount(cid, v[1]) >= v[2]) then
					bool = false
					break
				end
			end
			if bool then
				for k, v in ipairs(items) do
					doPlayerRemoveItem(cid, v[1], v[2])
				end
				npcHandler:say("Okay, Hurry up!", cid)
				setPlayerStorageValue(cid, quest, 1)
				addAddonQuest(cid)
				doPlayerAddOutfit(cid, getPlayerSex(cid) == 0 and women or men, addon)
			else
				npcHandler:say("Please come back when you be have items.", cid)
			end
		end
		talkState[talkUser] = 0
	elseif(str == 4) then
		npcHandler:say("You have done enough for me, thanks again!", cid)
	end
return TRUE
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())