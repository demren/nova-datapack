  function onKill(cid, target, lastHit)
local questCreatures =
{
		["dworc fleshhunter"] = 		{questStarted = 1541, questStorage = 63021, creatureStorage = 15401, killsRequired = 200, raceName = "Dworces"},
		["dworc venomsniper"] = 		{questStarted = 1541, questStorage = 63021, creatureStorage = 15402, killsRequired = 200, raceName = "Dworces"},
		["dworc voodoomaster"] = 		{questStarted = 1541, questStorage = 63021, creatureStorage = 15403, killsRequired = 200, raceName = "Dworces"},
		["kongra"] = 					{questStarted = 1542, questStorage = 63022, creatureStorage = 15404, killsRequired = 300, raceName = "Apes"},
		["merlkin"] = 					{questStarted = 1542, questStorage = 63022, creatureStorage = 15405, killsRequired = 300, raceName = "Apes"},
		["sibang"] = 					{questStarted = 1542, questStorage = 63022, creatureStorage = 15406, killsRequired = 300, raceName = "Apes"},
		["lizard abomination"] = 		{questStarted = 1543, questStorage = 63023, creatureStorage = 15407, killsRequired = 350, raceName = "Lizards"},
		["lizard chosen"] = 			{questStarted = 1543, questStorage = 63023, creatureStorage = 15408, killsRequired = 350, raceName = "Lizards"},
		["lizard dragon priest"] = 		{questStarted = 1543, questStorage = 63023, creatureStorage = 15409, killsRequired = 350, raceName = "Lizards"},
		["lizard high guard"] = 		{questStarted = 1543, questStorage = 63023, creatureStorage = 15410, killsRequired = 350, raceName = "Lizards"},
		["lizard legionnaire"] = 		{questStarted = 1543, questStorage = 63023, creatureStorage = 15421, killsRequired = 350, raceName = "Lizards"},
		["lizard sentinel"] = 			{questStarted = 1543, questStorage = 63023, creatureStorage = 15422, killsRequired = 350, raceName = "Lizards"},
		["lizard snakecharmer"] = 		{questStarted = 1543, questStorage = 63023, creatureStorage = 15423, killsRequired = 350, raceName = "Lizards"},
		["lizard templar"] = 			{questStarted = 1543, questStorage = 63023, creatureStorage = 15424, killsRequired = 350, raceName = "Lizards"},
		["lizard zaogun"] = 			{questStarted = 1543, questStorage = 63023, creatureStorage = 15425, killsRequired = 350, raceName = "Lizards"},
		["bonelord"] = 					{questStarted = 1544, questStorage = 63024, creatureStorage = 15426, killsRequired = 80,  raceName = "Bonelords"},
		["slug"] = 						{questStarted = 1545, questStorage = 63025, creatureStorage = 15427, killsRequired = 200, raceName = "Slugs"},
		["crawler"] = 					{questStarted = 1546, questStorage = 63026, creatureStorage = 15428, killsRequired = 200, raceName = "Crawlers"},
		["deepling guard"] = 			{questStarted = 1547, questStorage = 63027, creatureStorage = 15429, killsRequired = 200, raceName = "Deepling monsters"},
		["deepling spellsinger"] = 		{questStarted = 1547, questStorage = 63027, creatureStorage = 15430, killsRequired = 200, raceName = "Deepling monsters"},
		["deepling tyrant"] = 			{questStarted = 1547, questStorage = 63027, creatureStorage = 15431, killsRequired = 200, raceName = "Deepling monsters"},
		["deepling warrior"] = 			{questStarted = 1547, questStorage = 63027, creatureStorage = 15432, killsRequired = 200, raceName = "Deepling monsters"},
		["shaman"] = 					{questStarted = 1548, questStorage = 63028, creatureStorage = 15433, killsRequired = 250, raceName = "Shamans"},
		["elder shaman"] = 				{questStarted = 1548, questStorage = 63028, creatureStorage = 15434, killsRequired = 250, raceName = "Shamans"},
		["askarak demon"] = 			{questStarted = 1549, questStorage = 63029, creatureStorage = 15435, killsRequired = 200, raceName = "Askarak Demons"},
		["askarak lord"] = 				{questStarted = 1549, questStorage = 63029, creatureStorage = 15436, killsRequired = 200, raceName = "Askarak Demons"},
		["askarak prince"] = 			{questStarted = 1549, questStorage = 63029, creatureStorage = 15437, killsRequired = 200, raceName = "Askarak Demons"},
		["mutated demon"] = 			{questStarted = 1550, questStorage = 63030, creatureStorage = 15438, killsRequired = 400, raceName = "Mutated Demons"},
		["war golem"] = 				{questStarted = 1551, questStorage = 63031, creatureStorage = 15439, killsRequired = 450, raceName = "War golems"},
		["dragonling"] = 				{questStarted = 1552, questStorage = 63032, creatureStorage = 15440, killsRequired = 200, raceName = "Dragonlings"},
		["basilisk"] = 					{questStarted = 1553, questStorage = 63033, creatureStorage = 15441, killsRequired = 150, raceName = "Basilisks"},
		["basilisk lord"] = 			{questStarted = 1553, questStorage = 63033, creatureStorage = 15442, killsRequired = 150, raceName = "Basilisks"},
		["stampor"] = 					{questStarted = 1554, questStorage = 63034, creatureStorage = 15443, killsRequired = 300, raceName = "Stampors"}
}

local creature = questCreatures[string.lower(getCreatureName(target))]
        if creature then
                if(isPlayer(target) == true) then
                        return true
                end
                if getPlayerStorageValue(cid, creature.questStarted) == 1 then
                        if getPlayerStorageValue(cid, creature.questStorage) < creature.killsRequired then
                                if getPlayerStorageValue(cid, creature.questStorage) < 0 then
                                        doPlayerSetStorageValue(cid, creature.questStorage, 0)
                                end

                                if getPlayerStorageValue(cid, creature.creatureStorage) < 0 then
                                        doPlayerSetStorageValue(cid, creature.creatureStorage, 0)
                                end
                                doPlayerSetStorageValue(cid, creature.questStorage, getPlayerStorageValue(cid, creature.questStorage) + 1)
                                doPlayerSetStorageValue(cid, creature.creatureStorage, getPlayerStorageValue(cid, creature.creatureStorage) + 1)
                                doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "" .. getPlayerStorageValue(cid, creature.creatureStorage) .. " " .. getCreatureName(target) .. " defeated. Total [" .. getPlayerStorageValue(cid, creature.questStorage) .. "/" .. creature.killsRequired .. "] " .. creature.raceName .. ".")
                        end
                end
        end
        return true
end 