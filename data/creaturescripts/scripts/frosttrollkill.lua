local monsters = {
	--name = storage
	["frost troll"] = 9368
}

function onKill(cid, target)
	local monster = monsters[getCreatureName(target):lower()]
	if(isPlayer(target) == FALSE and monster and getPlayerStorageValue(cid, 9367) == 2) then
		if getPlayerStorageValue(cid, monster) < 30 then 
			local killedMonsters = getPlayerStorageValue(cid, monster)
            if(killedMonsters == -1) then
                killedMonsters = 1
			end
			setPlayerStorageValue(cid, monster, killedMonsters + 1)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed " .. killedMonsters .. " of 30 frost trolls.")
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed enough frost trolls. You should talk to Amand about a reward.")
			setPlayerStorageValue(cid, 9367, 3)
		end
	end
	return TRUE
end