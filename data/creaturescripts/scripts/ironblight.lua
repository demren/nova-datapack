local storage1 = 44778
local storage2 = 44775
local monsters_ilosc={"ironblight", 100}
local monsters = {
	--name = storage
	["ironblight"] = storage1
}

function onKill(cid, target)
	local monster = monsters[getCreatureName(target):lower()]
	if(isPlayer(target) == FALSE and monster and getPlayerStorageValue(cid, storage2) == 2) then
		if getPlayerStorageValue(cid, monster) < monsters_ilosc[2] then 
			local killedMonsters = getPlayerStorageValue(cid, monster)
            if(killedMonsters == -1) then
                killedMonsters = 1
			end
			setPlayerStorageValue(cid, monster, killedMonsters + 1)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed " .. killedMonsters .. " of "..monsters_ilosc[2].." "..monsters_ilosc[1]..".")
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed enough "..monsters_ilosc[1]..". You should talk to Justin about a reward.")
			setPlayerStorageValue(cid, storage2, 3)
		end
	end
	return TRUE
end