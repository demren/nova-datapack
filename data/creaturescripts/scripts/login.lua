local config = {
	loginMessage = "Welcome to " .. configManager.getString(configKeys.SERVER_NAME) .. "!",
	useFragHandler = true
}

function onLogin(cid)
--namelocks
	local namelock = db.storeQuery("SELECT count(player_id) FROM `player_namelocks` WHERE `player_id` = '"..getPlayerGUID(cid).."';").getDataInt('count(player_id)')
	if(namelock > 0) then
		doPlayerPopupFYI(cid, "You got namelock. You have to change nick on the website to play. Log in on your account and change name.")
		doTeleportThing(cid,getTownTemplePosition(getPlayerTown(cid)))
		addEvent(doRemoveCreature,5000,cid)
	end
	doRefreshPlayerTasksByGUID(getPlayerGUID(cid))
	setPlayerQuests(cid)
	local slotItem = getPlayerSlotItem(cid, CONST_SLOT_HEAD)
	if(slotItem.uid ~= 0 and slotItem.itemid == 12541) then
		doTransformItem(slotItem.uid, 5461)
	end
	if(getPlayerLastLoginSaved(cid) < 1346536936 and getPlayerStorageValue(cid,1444) < 1) then -- stary killinginthenameof
		setPlayerStorageValue(cid,1444,1)
		local s=getPlayerStorageValue(cid, 64521)
		s=s-1
		if(s > 0) then
			for i=1, s do
				setPlayerStorageValue(cid, 1500+i,2)
				print ("USTAWILEM GRACZOWI `"..getPlayerName(cid).."` STORAGE `"..(1500+i).."` NA WARTOSC `2`")
				if(i == 15) then
					break -- dla bezpieczenstwa
				end
			end
		end
	end	
	doPlayerOpenChannel(cid, 5) -- trade
	doPlayerOpenChannel(cid, 7) -- help
	if(getPlayerGuildId(cid) > 0) then
		doPlayerOpenChannel(cid, 0) -- guil 
	end

	if(getPlayerStorageValue(cid, 9993)< os.time() and getPlayerStorageValue(cid, 9993) > 0) then
		if(getPlayerMount(cid,22)) then
			doPlayerRemoveMount(cid, 22)
		end
		if(getPlayerMount(cid,25)) then
			doPlayerRemoveMount(cid,25 )
		end
		if(getPlayerMount(cid,26)) then
			doPlayerRemoveMount(cid, 26)
		end
		setPlayerStorageValue(cid, 9993,0)
		doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "Your horse contract has ended. Once you dismount, you cannot ride the rented horse anymore.")
	end
	local attackSpeed=1500
	if(tonumber(getPlayerStorageValue(cid, 26003)) == 1) then
		setPlayerStorageValue(cid, 26003,0)
		local skillTime = os.time() - getPlayerStorageValue(cid,26004)
		if(skillTime <= BLOCKED_TIME) then
			doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "You must be logged out for more than 3 minutes to start offline training.")
		else
			local gap=0
			if(skillTime > getPlayerStorageValue(cid,26001)) then
				gap = skillTime-getPlayerStorageValue(cid,26001)
				skillTime=getPlayerStorageValue(cid,26001)
			end
			skillTime=math.min(skillTime,SKILL_TIME)
			gap = math.min(gap,SKILL_TIME)
			if(gap > 0) then
				setPlayerStorageValue(cid,26001,gap)
			else
				setPlayerStorageValue(cid,26001,getPlayerStorageValue(cid,26001)-skillTime)
			end
			local skill_id = getPlayerStorageValue(cid, 26002)
			if(skill_id == 7) then
				local mana=0
				local mana_tbl = {
					[0] = 1,
					[1] = 4,
					[2] = 4,
					[3] = 3,
					[4] = 2,
					[5] = 12,
					[6] = 12,
					[7] = 6,
					[8] = 4
				}
				mana = skillTime * mana_tbl[getPlayerVocation(cid)]
				doPlayerAddSpentMana(cid, mana)
			else
				doPlayerAddSkillTry(cid, skill_id,math.floor(skillTime*(attackSpeed/1000)/2))
			end
			doPlayerAddSkillTry(cid, 5,math.floor(skillTime*(attackSpeed/1000)))
			setPlayerStorageValue(cid,26004,os.time())
			doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "During your absence you trained for "..(math.floor((skillTime/60)/60) > 0 and (math.floor((skillTime/60)/60).." hour"..(math.floor((skillTime/60)/60) == 1 and "" or "s").." and ") or "")..math.floor((skillTime/60) % 60).." minutes.")
		end
	end
	-- if(isAfker(cid)) then
		-- logout:
		-- doTeleportThing(cid, {x=(math.random(10,20)), y=(math.random(10,20)), z=(math.random(0,10))})
		-- no logout:
		-- doTeleportThing(cid, {x=(math.random(21,31)), y=(math.random(10,20)), z=(math.random(0,10))}, 0, 1)
	-- end AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	if(isAfker(cid)) then
		-- if((TOSPAWN and math.random(1,2)==1) and getGlobalStorageValue(3719) < os.time()) then
			-- setGlobalStorageValue(3719,os.time()+(1*60))
			-- if(math.random(1,5) == 1) then
				-- local ilehp=math.random(10,getCreatureHealth(cid))
				-- doCreatureAddHealth(cid,-ilehp)
			-- end
			-- doTeleportThing(cid,POZ_TEMPLE_FIRST_SPAWN)
			-- setPlayerStorageValue(cid,3718,1)
			-- agf_destroyStorages(cid)
			-- TOSPAWN=0
		-- else
			setPlayerStorageValue(cid,3718,0)
			-- logout:
			-- doTeleportThing(cid, {x=(math.random(10,20)), y=(math.random(10,20)), z=(math.random(0,10))})
			-- no logout:
			doTeleportThing(cid, {x=(math.random(21,31)), y=(math.random(10,20)), z=(math.random(0,10))}, 0, 1)
		-- end
	end
if(getPlayerPremiumDays(cid) < 3 and getPlayerLevel(cid) >= 80) then
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You need premium! You can buy it from NPC Promoter in depot.")
elseif(getPlayerLevel(cid) < 80) then
	while(getPlayerPremiumDays(cid) < 3) do
		doPlayerAddPremiumDays(cid, 1)
	end
end
	-- if isRookie(cid) and getPlayerTown(cid) ~= 6 then
		-- doPlayerSetTown(cid, 6)
		-- doTeleportThing(cid, getTownTemplePosition(6))
	-- end
	local storage = 52740
	if (getPlayerStorageValue(cid, storage) > 0) then
		if (getPlayerStorageValue(cid, storage) > os.time()) then
			addEvent(doTeleportThing, (getPlayerStorageValue(cid, storage) - os.time())*1000, cid, {x=486, y=162, z=7})
		else
			doTeleportThing(cid, {x=486, y=162, z=7})
			setPlayerStorageValue(cid, storage, 0)
		end
	end
	if getPlayerStorageValue(cid, 9845) == 1 or getPlayerStorageValue(cid, 9846) >= os.time() then
	 setPlayerStamina(cid, 42 * 60)
	end
	local write = true
	if getPlayerStorageValue(cid,9113) < 1 then
		doPlayerAddItem(cid, 2175, 1)
		if isKnight(cid) then
			doTeleportThing(cid, {x=486, y=155, z=10})
			write = false
		end
		setPlayerStorageValue(cid,9113,1)
	end
	if(getPlayerStorageValue(cid, 4321) < 2 and (not isAfker(cid)) and write and isInRange(getPlayerPosition(cid),{x=485,y=153,z=4},{x=491,y=159,z=4})) then
		addEvent(doCreatureSay,1000,getCreatureByName('greg'), "Hey, "..getPlayerName(cid).."! I have a mission for you.",1)
	end
	setPlayerStorageValue(cid, 6565, 0)
	local loss = 10--getConfigValue('deathLostPercent')
	if(loss ~= nil) then
		doPlayerSetLossPercent(cid, PLAYERLOSS_EXPERIENCE, loss * 10)
	end
	if getPlayerLevel(cid) < 80 then
			for i=1, 5 do
				if not getPlayerBlessing(cid, i) then
					doPlayerAddBlessing(cid, i)
				end
			end
			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You received all blessed automatically.")
	end
	local accountManager = getPlayerAccountManager(cid)
	-- if(accountManager == MANAGER_NONE) then
		-- local lastLogin, str = getPlayerLastLoginSaved(cid), config.loginMessage
		-- if(lastLogin > 0) then
			-- doPlayerSendTextMessage(cid, MESSAGE_STATUS_DEFAULT, str)
			-- str = "Your last visit was on " .. os.date("%a %b %d %X %Y", lastLogin) .. "."
		-- else
			-- ile_wyjebac_ma_pacca = getPlayerPremiumDays(cid)-3
			-- ile_wyjebac_ma_pacca=ile_wyjebac_ma_pacca>0 and ile_wyjebac_ma_pacca or 0
			-- doPlayerRemovePremiumDays(cid, ile_wyjebac_ma_pacca)
			-- str = str .. " Please choose your outfit."
			-- doPlayerSendOutfitWindow(cid)
		-- end

		-- doPlayerSendTextMessage(cid, MESSAGE_STATUS_DEFAULT, str)
	-- if(accountManager == MANAGER_NAMELOCK) then
		-- doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "Hello, it appears that your character has been namelocked, what would you like as your new name?")
	-- end
	-- elseif(accountManager == MANAGER_ACCOUNT) then
		-- doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "Hello, type 'account' to manage your account and if you want to start over then type 'cancel'.")
	-- else
		-- doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "Hello, type 'account' to create an account or type 'recover' to recover an account.")
	-- end

	if(not isPlayerGhost(cid)) then
		doSendMagicEffect(getCreaturePosition(cid), CONST_ME_TELEPORT)
	end
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "If you have an idea related to our server, or you found a bug send us a message using command !report message. For example \n!report I found a bug in here.\nYour name and position with your message will be sent to us.")
	-- if(getPlayerLevel(cid) >= 80 and false) then
		-- if(getPlayerPvp(cid)) then
			-- if(getPlayerPvpCC(cid) > 0) then
				-- doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "Your PvP mode is enabled. To disable PVP use command: !setpvp off")
			-- else
				-- doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "Your PvP mode is enabled. In order to disable PVP visit our sms shop.")
			-- end
		-- else
			-- if(getPlayerPvpCC(cid) > 0) then
				-- doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "Your PvP mode is disable. To enable PVP use command: !setpvp on")
			-- else
				-- doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "Your PvP mode is disable. In order to enable PVP visit our sms shop.")
			-- end
		-- end
	-- end
	-- registerCreatureEvent(cid, "Mail")
	--registerCreatureEvent(cid, "GuildMotd")
	registerCreatureEvent(cid, "Idle")
	if(config.useFragHandler) then
		registerCreatureEvent(cid, "SkullCheck")
	end
	registerCreatureEvent(cid, "broadcastadvance")
	if(isHalloween()) then
		registerCreatureEvent(cid, "halloween")
	end
	registerCreatureEvent(cid, "AdvanceSave")
		registerCreatureEvent(cid, "minokill")
		registerCreatureEvent(cid, "ziemniaki")
		registerCreatureEvent(cid, "konrad")
		registerCreatureEvent(cid, "matthew")
		registerCreatureEvent(cid, "justin")
		registerCreatureEvent(cid, "earl")
		registerCreatureEvent(cid, "hero")
	registerCreatureEvent(cid, "inquisitionPortals")
	registerCreatureEvent(cid, "top_boss")
	registerCreatureEvent(cid, "TopFrags")
	registerCreatureEvent(cid, "KillingInTheNameOf")
	registerCreatureEvent(cid, "KillingInTheNameOf_nefaros")
	registerCreatureEvent(cid, "KillingInTheNameOf_port")
	registerCreatureEvent(cid, "ArenaKill")
	registerCreatureEvent(cid, "monsterkill")
	registerCreatureEvent(cid, "dwarfkill")
	registerCreatureEvent(cid, "elfkill")
	registerCreatureEvent(cid, "attr")
	registerCreatureEvent(cid, "herokill")
	registerCreatureEvent(cid, "swampkill")
	registerCreatureEvent(cid, "ironblight")
	registerCreatureEvent(cid, "frosttrollkill")
	registerCreatureEvent(cid, "trollkill")
	registerCreatureEvent(cid, "ghastly")
	registerCreatureEvent(cid, "bonusexp")
	registerCreatureEvent(cid, "upgrader")
	registerCreatureEvent(cid, "lifeleech")
	registerCreatureEvent(cid, "deathshield")
	registerCreatureEvent(cid, "criticalstrike")
	registerCreatureEvent(cid, "creatureratyy")
	registerCreatureEvent(cid, "y_house")
	registerCreatureEvent(cid, "jugg")
	registerCreatureEvent(cid, "pojemnik_dusz")
	registerCreatureEvent(cid, "creaturespiders")
	registerCreatureEvent(cid, "ZombieAttack")
	registerCreatureEvent(cid, "demonOakDeath")
	registerCreatureEvent(cid, "demonOakLogout")
	registerCreatureEvent(cid, "santa")
	registerCreatureEvent(cid, "PlayerDeath")
	registerCreatureEvent(cid, "frank")
	registerCreatureEvent(cid, "smonkill")
	registerCreatureEvent(cid, "smlook")
	registerCreatureEvent(cid, "smstatschange")
	registerCreatureEvent(cid, "smadvance")
	sayHelloSM(cid)

	
	
		-- registerCreatureEvent(cid, "pvp")

	registerCreatureEvent(cid, "top_boss")
    -- if he did not make full arena 1 he must start from zero
    if getPlayerStorageValue(cid, 42309) < 1 then
        for i = 42300, 42309 do
            setPlayerStorageValue(cid, i, 0)
        end
    end
    -- if he did not make full arena 2 he must start from zero
    if getPlayerStorageValue(cid, 42319) < 1 then
        for i = 42310, 42319 do
            setPlayerStorageValue(cid, i, 0)
        end
    end
    -- if he did not make full arena 3 he must start from zero
    if getPlayerStorageValue(cid, 42329) < 1 then
        for i = 42320, 42329 do
            setPlayerStorageValue(cid, i, 0)
        end
    end
    if getPlayerStorageValue(cid, 42355) == -1 then
        setPlayerStorageValue(cid, 42355, 0) -- did not arena level
    end
    setPlayerStorageValue(cid, 42350, 0) -- time to kick 0
    setPlayerStorageValue(cid, 42352, 0) -- is not in arena  
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, getPlayerOfflineTrainingTime(cid,"string") )
	return true
end
