local monsters = {
	--name = storage
	["minotaur"] = 5015,
	["minotaur guard"] = 5015,
	["minotaur mage"] = 5015,
	["minotaur archer"] = 5015,
	["the horned fox"] = 5015
}

function onKill(cid, target)
	local monster = monsters[getCreatureName(target):lower()]
	if(isPlayer(target) == FALSE and monster and getPlayerStorageValue(cid, 5005) == 2) then
		if getPlayerStorageValue(cid, monster) < 200 then 
			local killedMonsters = getPlayerStorageValue(cid, monster)
            if(killedMonsters == -1) then
                killedMonsters = 1
			end
			setPlayerStorageValue(cid, monster, killedMonsters + 1)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed " .. killedMonsters .. " of 200 minotaurs.")
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed enough minotaurs. You should talk to Paul about a reward.")
			setPlayerStorageValue(cid, 5005, 3)
		end
	end
	return TRUE
end