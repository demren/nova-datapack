-- getPlayerSMsByGUID(GUID)
-- setSMValueplus1(cid, SM_ID, VALUE_ID
-- setSMValueplusn(cid, SM_ID, VALUE_ID, VALUE)

function onKill(cid, target)
	if(not isPlayer(cid)) then
		return true
	end
	local sm_id = getPlayerSMsByGUID(getPlayerGUID(cid))
	if(isInArray2(sm_id, 235)) then --3976
		if(isInArray2(BOSSES,getCreatureName(target):lower())) then
			setSMValueplus1(cid,235,1)
		end
	end
	if(isInArray2(sm_id, 335)) then --3976
		if(isInArray2(BOSSES,getCreatureName(target):lower())) then
			setSMValueplus1(cid,335,1)
		end
	end
	if(isInArray2(sm_id, 329)) then --3976
		if(isInArray2(BOSSES,getCreatureName(target):lower())) then
			setSMValueplus1(cid,329,1)
		end
	end
	if(isPlayer(target)) then
		if(isInArray2(sm_id, 328)) then
			setSMValueplus1(cid,328,1)
		end
	end
	if(isInArray2(sm_id, 101)) then
		if(getCreatureName(target):lower() == 'troll') then
			setSMValueplus1(cid,101,1)
			print ("CHUCJCUHCU")
		end
	end
	if(isInArray2(sm_id, 338)) then
		if(getCreatureName(target):lower() == 'wisp') then
			setSMValueplus1(cid,338,1)
		end
	end
	if(isInArray2(sm_id, 105)) then
		if(isMonster(target)) then
			setSMValueplus1(cid,105,1)
		end
	end
	if(isMonster(target)) then
		if(isInArray2(sm_id, 114)) then
			if(getMonsterInfo(getCreatureName(target)).race == "undead") then
				setSMValueplus1(cid,114,1)
			end
		end
		if(isInArray2(sm_id, 102)) then
			if(getCreatureName(target):lower() == 'troll') then
				setSMValueplus1(cid,102,1)
			end
			if(getCreatureName(target):lower() == 'swamp troll') then
				setSMValueplus1(cid,102,2)
			end
			if(getCreatureName(target):lower() == 'frost troll') then
				setSMValueplus1(cid,102,3)
			end
		end
		if(isInArray2(sm_id, 131)) then
			if(getCreatureName(target):lower() == 'dwarf') then
				setSMValueplus1(cid,131,1)
			end
			if(getCreatureName(target):lower() == 'rotworm') then
				setSMValueplus1(cid,131,2)
			end
		end
		if(isInArray2(sm_id, 142)) then
			if(getCreatureName(target):lower() == 'bear') then
				setSMValueplus1(cid,142,1)
			end
			if(getCreatureName(target):lower() == 'polar bear') then
				setSMValueplus1(cid,142,2)
			end
		end
	end
	if(isMonster(target)) then
		if(isInArray2(sm_id, 127)) then
			if(getCreatureName(target):lower()=='minotaur') then
				setSMValueplus1(cid,127,1)
			end
		end
		if(isInArray2(sm_id, 330)) then
			if(getCreatureName(target):lower()=='mutated demon') then
				setSMValueplus1(cid,330,1)
			end
		end
		if(isInArray2(sm_id, 132)) then
			if(getCreatureName(target):lower()=='elf') then
				setSMValueplus1(cid,132,1)
			end
		end
		if(isInArray2(sm_id, 109)) then
			if((getCreatureName(target)):lower()=='cyclops') then
				setSMValueplus1(cid,109,1)
			end
		end
		if(isInArray2(sm_id, 201)) then
			if((getCreatureName(target)):lower()=='dragon') then
				setSMValueplus1(cid,201,1)
			end
		end
		if(isInArray2(sm_id, 243)) then
			if((getCreatureName(target)):lower()=='witch') then
				setSMValueplus1(cid,243,1)
			end
		end
		if(isInArray2(sm_id, 227)) then--Pharaon
			if((getCreatureName(target)):lower()=='demon') then
				setSMValueplus1(cid,227,1)
			end
		end
		if(isInArray2(sm_id, 301)) then
			if((getCreatureName(target)):lower()=='demon') then
				setSMValueplus1(cid,301,1)
			end
		end
		if(isInArray2(sm_id, 327)) then
			if((getCreatureName(target)):lower()=='jaul') then
				setSMValueplus1(cid,327,1)
			end
		end
		if(isInArray2(sm_id, 320)) then
			if((getCreatureName(target)):lower()=='ferumbras') then
				setSMValueplus1(cid,320,1)
			end
		end
		if(isInArray2(sm_id, 321)) then
			if((getCreatureName(target)):lower()=='weakened juggernaut') then
				setSMValueplus1(cid,321,1)
			end
		end
		if(isInArray2(sm_id, 314)) then
			if((getCreatureName(target)):lower()=='undead dragon') then
				setSMValueplus1(cid,314,1)
			end
		end
		if(isInArray2(sm_id, 202)) then
			if(getCreatureName(target):lower() == 'dragon') then
				setSMValueplus1(cid,202,1)
			end
			if(getCreatureName(target):lower() == 'dragon lord') then
				setSMValueplus1(cid,202,2)
			end
			if(getCreatureName(target):lower() == 'frost dragon') then
				setSMValueplus1(cid,202,3)
			end
		end
		if(isInArray2(sm_id, 302)) then
			if(getCreatureName(target):lower() == 'hydra') then
				setSMValueplus1(cid,302,1)
			end
			if(getCreatureName(target):lower() == 'dragon lord') then
				setSMValueplus1(cid,302,2)
			end
			if(getCreatureName(target):lower() == 'dragonling') then
				setSMValueplus1(cid,302,3)
			end
		end
		if(isInArray2(sm_id, 316)) then
			if(getCreatureName(target):lower() == 'yielothax') then
				setSMValueplus1(cid,316,1)
			end
			if(getCreatureName(target):lower() == 'grim reaper') then
				setSMValueplus1(cid,316,2)
			end
			if(getCreatureName(target):lower() == 'armadile') then
				setSMValueplus1(cid,316,3)
			end
		end
		if(isInArray2(sm_id, 317)) then
			if(getCreatureName(target):lower() == 'water elemental') then
				setSMValueplus1(cid,317,1)
			end
			if(getCreatureName(target):lower() == 'magma crawler') then
				setSMValueplus1(cid,317,2)
			end
			if(getCreatureName(target):lower() == 'stampor') then
				setSMValueplus1(cid,317,3)
			end
		end
		if(isInArray2(sm_id, 318)) then
			if(getCreatureName(target):lower() == 'undead cavebear') then
				setSMValueplus1(cid,318,1)
			end
			if(getCreatureName(target):lower() == 'boar') then
				setSMValueplus1(cid,318,2)
			end
			if(getCreatureName(target):lower() == 'warlock') then
				setSMValueplus1(cid,318,3)
			end
		end
		if(isInArray2(sm_id, 319)) then
			if(getCreatureName(target):lower() == 'behemoth') then
				setSMValueplus1(cid,319,1)
			end
			if(getCreatureName(target):lower() == 'bonelord') then
				setSMValueplus1(cid,319,2)
			end
		end
		if(isInArray2(sm_id, 333)) then
			if(getCreatureName(target):lower() == 'shaman') then
				setSMValueplus1(cid,333,1)
			end
			if(getCreatureName(target):lower() == 'pharaon') then
				setSMValueplus1(cid,333,2)
			end
		end
		if(isInArray2(sm_id, 216)) then
			if(getCreatureName(target):lower() == 'elephant') then
				setSMValueplus1(cid,216,1)
			end
			if(getCreatureName(target):lower() == 'crocodile') then
				setSMValueplus1(cid,216,2)
			end
			if(getCreatureName(target):lower() == 'panda') then
				setSMValueplus1(cid,216,3)
			end
		end
		if(isInArray2(sm_id, 205)) then
			setSMValueplus1(cid,205,1)
		end
		if(isInArray2(sm_id, 305)) then
			setSMValueplus1(cid,305,1)
		end
	end
return true
end
function onLook(cid, thing, position, lookDistance)
	local sm_id = getPlayerSMsByGUID(getPlayerGUID(cid))
	if(isInArray2(sm_id, 120)) then --3976
		if(thing.itemid == 3976 and thing.type==10) then
			setSMValueplus1(cid,120,1)
		end
	end
	if(isInArray2(sm_id, 322)) then --3976 red drag scales
		if(thing.itemid == 5882 and thing.type==10) then
			setSMValueplus1(cid,322,1)
		end
	end
	if(isInArray2(sm_id, 342)) then --3976 sugar kurwa oat
		if(thing.itemid == 13939) then
			setSMValueplus1(cid,342,1)
		end
	end
	if(isInArray2(sm_id, 323)) then --3976 green drag scales 2492
		if(thing.itemid == 5920 and thing.type==10) then
			setSMValueplus1(cid,323,1)
		end
	end
	if(isInArray2(sm_id, 334)) then --3976 dsm
		if(thing.itemid == 2492) then
			setSMValueplus1(cid,334,1)
		end
	end
	if(isInArray2(sm_id, 324)) then --3976 scarab coints
		if(thing.itemid == 2159 and thing.type==100) then
			setSMValueplus1(cid,324,1)
		end
	end
	if(isInArray2(sm_id, 220)) then --3976 WORM MADAFAKA
		if(thing.itemid == 3976 and thing.type==3) then
			setSMValueplus1(cid,220,1)
		end
	end
	if(isInArray2(sm_id, 221)) then --3976 WORM MADAFAKA
		if(thing.itemid == 10571 and thing.type==20) then
			setSMValueplus1(cid,221,1)
		end
	end
	if(isInArray2(sm_id, 114)) then --3976
		if(thing.itemid == 2160) then
			setSMValueplus1(cid,114,1)
		end
	end
	if(isInArray2(sm_id, 234)) then --3976
		if(thing.itemid == 2392) then --fire sword
			setSMValueplus1(cid,234,1)
		end
	end
	if(isInArray2(sm_id, 224)) then --2159
		if(thing.itemid == 2159) then --fire sword
			setSMValueplus1(cid,224,1)
		end
	end
	if(isInArray2(sm_id, 121)) then --3976
		if(thing.itemid == 2666 and thing.type==10) then
			setSMValueplus1(cid,121,1)
		end
	end
	if(isInArray2(sm_id, 122)) then --3976
		if(thing.itemid == 2667 and thing.type==10) then
			setSMValueplus1(cid,122,1)
		end
	end
	if(isInArray2(sm_id, 123)) then --3976
		if(getItemNameById(thing.itemid) == "snow") then
			setSMValueplus1(cid,123,1)
		end
	end
	if(isInArray2(sm_id, 124)) then --3976
		if(isCreature(thing.uid)) then
			if((getCreatureName(thing.uid)):lower() == "riona") then
				setSMValueplus1(cid,124,1)
			end
		end
	end
	if(isInArray2(sm_id, 233)) then --3976 214
		if(isCreature(thing.uid)) then
			if((getCreatureName(thing.uid)):lower() == "shaman" or
			(getCreatureName(thing.uid)):lower() == "elder shaman" or
			(getCreatureName(thing.uid)):lower() == "pharaon" or
			(getCreatureName(thing.uid)):lower() == "pharaon boss") then
				setSMValueplus1(cid,214,1)
			end
		end
	end
	if(isInArray2(sm_id, 214)) then --3976 
		if(isCreature(thing.uid)) then
			if((getCreatureName(thing.uid)):lower() == "deepling scout" or
			(getCreatureName(thing.uid)):lower() == "deepling worker" or
			(getCreatureName(thing.uid)):lower() == "deepling guard" or
			(getCreatureName(thing.uid)):lower() == "deepling warrior" or
			(getCreatureName(thing.uid)):lower() == "deepling tyrant") then
				setSMValueplus1(cid,233,1)
			end
		end
	end
	if(isInArray2(sm_id, 229)) then --3976
		if(isCreature(thing.uid)) then
			if((getCreatureName(thing.uid)):lower() == "captain natanael") then
				setSMValueplus1(cid,229,1)
			end
		end
	end
	if(isInArray2(sm_id, 230)) then --3976
		if(isPlayer(thing.uid)) then
			setSMValueplus1(cid,230,1)
		end
	end
	if(isInArray2(sm_id, 222)) then --3976
		if(isCreature(thing.uid)) then
			if((getCreatureName(thing.uid)):lower() == "husam udin") then
				setSMValueplus1(cid,222,1)
			end
		end
	end
	if(isInArray2(sm_id, 223)) then --3976
		if(isCreature(thing.uid)) then
			if((getCreatureName(thing.uid)):lower() == "zuri") then
				setSMValueplus1(cid,223,1)
			end
		end
	end
	if(isInArray2(sm_id, 130)) then --3976
		if(thing.itemid == 2261) then
			setSMValueplus1(cid,130,1)
		end
	end
return true
end

function onStatsChange(cid, attacker, type, combat, value)
	if(isPlayer(cid)) then
		local sm_ids_cid = getPlayerSMsByGUID(getPlayerGUID(cid))
		if(isInArray2(sm_ids_cid, 111) and getCreatureCondition(cid, CONDITION_POISON)) then
			setSMValueplus1(cid,111,1)
		end
		if(isInArray2(sm_ids_cid, 112) and getCreatureCondition(cid, CONDITION_FIRE)) then
			setSMValueplus1(cid,112,1)
		end
		if(isInArray2(sm_ids_cid, 113) and combat == COMBAT_ENERGYDAMAGE) then
			setSMValueplus1(cid,113,1)
		end
		if(isInArray2(sm_ids_cid, 133) and combat == COMBAT_EARTHDAMAGE ) then
			setSMValueplus1(cid,133,1)
		end
		if(isInArray2(sm_ids_cid, 208) and combat == COMBAT_EARTHDAMAGE ) then
			setSMValueplus1(cid,208,1)
		end
		if(isInArray2(sm_ids_cid, 308) and combat == COMBAT_EARTHDAMAGE ) then
			setSMValueplus1(cid,308,1)
		end
		if(isInArray2(sm_ids_cid, 134) and combat == COMBAT_ICEDAMAGE ) then
			setSMValueplus1(cid,134,1)
		end
		if(STATSCHANGE_HEALTHLOSS == type) then
			if(isInArray2(sm_ids_cid, 107)) then
				setSMValueplusn(cid,107,1,value)
			end
			if(isInArray2(sm_ids_cid, 207)) then
				setSMValueplusn(cid,207,1,value)
			end
			if(isInArray2(sm_ids_cid, 307)) then
				setSMValueplusn(cid,307,1,value)
			end
			if(isInArray2(sm_ids_cid, 137) and attacker) then
				if(getCreatureName(attacker):lower() == 'trainer' and isMonster(attacker)) then
					setSMValueplusn(cid,137,1,value)
				end
			end
			if(isInArray2(sm_ids_cid, 108) and combat==COMBAT_FIREDAMAGE and value>=40) then
				setSMValueplusn(cid,108,1,1)
			end
			if(isInArray2(sm_ids_cid, 213) and combat==COMBAT_ICEDAMAGE and value>=200) then
				setSMValueplusn(cid,213,1,1)
			end
			if(isInArray2(sm_ids_cid, 313) and combat==COMBAT_ICEDAMAGE and value>=300) then
				setSMValueplusn(cid,313,1,1)
			end
			if(isInArray2(sm_ids_cid, 211) and combat==COMBAT_EARTHDAMAGE and value>=100) then
				setSMValueplusn(cid,211,1,1)
			end
			if(isInArray2(sm_ids_cid, 311) and combat==COMBAT_DEATHDAMAGE  and value>=300) then
				setSMValueplusn(cid,311,1,1)
			end
			if(isInArray2(sm_ids_cid, 212) and value>=300) then
				setSMValueplusn(cid,212,1,1)
			end
			if(isInArray2(sm_ids_cid, 312) and value>=600) then
				setSMValueplusn(cid,312,1,1)
			end
			if(attacker) then
				if(isInArray2(sm_ids_cid, 231) and isPlayer(attacker)) then
					setSMValueplusn(cid,231,1,1)
				end
				if(isInArray2(sm_ids_cid, 331) and isPlayer(attacker)) then
					setSMValueplusn(cid,331,1,1)
				end
			end
			if(isInArray2(sm_ids_cid, 138) and combat==COMBAT_POISONDAMAGE) then
				setSMValueplusn(cid,138,1,value)
			end
			if(isInArray2(sm_ids_cid, 140) and combat==COMBAT_FIREDAMAGE) then
				setSMValueplusn(cid,140,1,value)
			end
			if(isInArray2(sm_ids_cid, 141) and combat==COMBAT_DROWNDAMAGE) then
				setSMValueplusn(cid,141,1,1)
			end
			if(isInArray2(sm_ids_cid, 135) and value==30) then
				setSMValueplusn(cid,135,1,1)
			end
		end
	end
	local sm_ids_attacker = attacker and (isPlayer(attacker) and getPlayerSMsByGUID(getPlayerGUID(attacker)) or {}) or {}
	if(STATSCHANGE_HEALTHLOSS == type) then
		if(isInArray2(sm_ids_attacker, 106)) then
			setSMValueplusn(attacker,106,1,value)
		end
		if(isInArray2(sm_ids_attacker, 206)) then
			setSMValueplusn(attacker,206,1,value)
		end
		if(isInArray2(sm_ids_attacker, 306)) then
			setSMValueplusn(attacker,306,1,value)
		end
		if(isInArray2(sm_ids_attacker, 232) and isPlayer(cid)) then
			setSMValueplusn(attacker,232,1,1)
		end
		if(isInArray2(sm_ids_attacker, 332) and isPlayer(cid) and value >= 200) then
			setSMValueplusn(attacker,332,1,1)
		end
		if(getCreatureName(cid):lower() == 'trainer' and isMonster(cid)) then
			if(isInArray2(sm_ids_attacker, 136)) then
				setSMValueplusn(attacker,136,1,value)
			end
		end
	end
return true
end
function onAdvance(cid, skill, oldlevel, newlevel)
	local sm_id = getPlayerSMsByGUID(getPlayerGUID(cid))
	if(isInArray2(sm_id, 128)) then
		if(skill == SKILL__LEVEL) then
			setSMValueplus1(cid,128,1)
		end
	end
	if(isInArray2(sm_id, 210)) then
		if(skill == SKILL__LEVEL) then
			setSMValueplus1(cid,210,1)
		end
	end
	if(isInArray2(sm_id, 310)) then
		if(skill == SKILL__LEVEL) then
			setSMValueplus1(cid,310,1)
		end
	end
	if(isInArray2(sm_id, 110)) then--209
		if(skill == SKILL_FISHING) then
			setSMValueplus1(cid,110,1)
		elseif(skill == SKILL_AXE) then
			setSMValueplus1(cid,110,2)
		elseif(skill == SKILL_FIST) then
			setSMValueplus1(cid,110,3)
		end
	end
	if(isInArray2(sm_id, 209)) then
		if(skill == SKILL_FISHING) then
			setSMValueplus1(cid,209,1)
		end
	end
	if(isInArray2(sm_id, 309)) then
		if(skill == SKILL_FISHING) then
			setSMValueplus1(cid,309,1)
		end
	end
return true
end