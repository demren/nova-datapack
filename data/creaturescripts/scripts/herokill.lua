local monsters = {
	--name = storage
	["hero"] = 9472,
	["shield"] = 9472
}

function onKill(cid, target)
	local monster = monsters[getCreatureName(target):lower()]
	if(isPlayer(target) == FALSE and monster and getPlayerStorageValue(cid, 9470) == 2) then
		if getPlayerStorageValue(cid, monster) < 50 then 
			local killedMonsters = getPlayerStorageValue(cid, monster)
            if(killedMonsters == -1) then
                killedMonsters = 1
			end
			setPlayerStorageValue(cid, monster, killedMonsters + 1)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed " .. killedMonsters .. " of 50 heroes.")
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed enough heroes. You should talk to Nozaj about a reward.")
			setPlayerStorageValue(cid, 9470, 3)
		end
	end
	return TRUE
end