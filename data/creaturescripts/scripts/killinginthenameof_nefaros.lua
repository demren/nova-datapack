  function onKill(cid, target, lastHit)
local questCreatures =
{
		["tortoise"] = 					{questStarted = 1521, questStorage = 64021, creatureStorage = 15201, killsRequired = 200, raceName = "Tortoises"},
		["thornback tortoise"] = 		{questStarted = 1521, questStorage = 64021, creatureStorage = 15202, killsRequired = 200, raceName = "Tortoises"},
		["scarab"] = 					{questStarted = 1522, questStorage = 64022, creatureStorage = 15203, killsRequired = 200, raceName = "Scarabs"},
		["mummy"] = 					{questStarted = 1523, questStorage = 64023, creatureStorage = 15204, killsRequired = 150, raceName = "Mummies"},
		["vampire"] = 					{questStarted = 1524, questStorage = 64024, creatureStorage = 15205, killsRequired = 80,  raceName = "Vampires"},
		["bonebeast"] = 				{questStarted = 1525, questStorage = 64025, creatureStorage = 15206, killsRequired = 100, raceName = "Bonebeasts"},
		["wyvern"] = 					{questStarted = 1526, questStorage = 64026, creatureStorage = 15207, killsRequired = 200, raceName = "Wyverns"},
		["nomad"] = 					{questStarted = 1527, questStorage = 64027, creatureStorage = 15208, killsRequired = 50, raceName = "Nomads"},
		["pirate buccaneer"] = 			{questStarted = 1528, questStorage = 64028, creatureStorage = 15209, killsRequired = 200, raceName = "Pirates"},
		["pirate corsair"] = 			{questStarted = 1528, questStorage = 64028, creatureStorage = 15210, killsRequired = 200, raceName = "Pirates"},
		["pirate cutthroat"] = 			{questStarted = 1528, questStorage = 64028, creatureStorage = 15211, killsRequired = 200, raceName = "Pirates"},
		["pirate ghost"] = 				{questStarted = 1528, questStorage = 64028, creatureStorage = 15212, killsRequired = 200, raceName = "Pirates"},
		["pirate marauder"] = 			{questStarted = 1528, questStorage = 64028, creatureStorage = 15213, killsRequired = 200, raceName = "Pirates"},
		["pirate skeleton"] = 			{questStarted = 1528, questStorage = 64028, creatureStorage = 15214, killsRequired = 200, raceName = "Pirates"},
		["tomb servant"] = 				{questStarted = 1529, questStorage = 64029, creatureStorage = 15215, killsRequired = 200, raceName = "Tomb Servants"},
		["sandstone scorpion"] = 		{questStarted = 1530, questStorage = 64030, creatureStorage = 15216, killsRequired = 300, raceName = "Sandstone Scorpions"},
		["golden servant"] = 			{questStarted = 1531, questStorage = 64031, creatureStorage = 15217, killsRequired = 300, raceName = "Golden Servants"},
		["lava golem"] = 				{questStarted = 1532, questStorage = 64032, creatureStorage = 15218, killsRequired = 450, raceName = "Lava Golems"},
		["armadile"] = 					{questStarted = 1533, questStorage = 64033, creatureStorage = 15219, killsRequired = 666, raceName = "Armadile"}
}

local creature = questCreatures[string.lower(getCreatureName(target))]
        if creature then
                if(isPlayer(target) == true) then
                        return true
                end
                if getPlayerStorageValue(cid, creature.questStarted) == 1 then
                        if getPlayerStorageValue(cid, creature.questStorage) < creature.killsRequired then
                                if getPlayerStorageValue(cid, creature.questStorage) < 0 then
                                        doPlayerSetStorageValue(cid, creature.questStorage, 0)
                                end

                                if getPlayerStorageValue(cid, creature.creatureStorage) < 0 then
                                        doPlayerSetStorageValue(cid, creature.creatureStorage, 0)
                                end
                                doPlayerSetStorageValue(cid, creature.questStorage, getPlayerStorageValue(cid, creature.questStorage) + 1)
                                doPlayerSetStorageValue(cid, creature.creatureStorage, getPlayerStorageValue(cid, creature.creatureStorage) + 1)
                                doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "" .. getPlayerStorageValue(cid, creature.creatureStorage) .. " " .. getCreatureName(target) .. " defeated. Total [" .. getPlayerStorageValue(cid, creature.questStorage) .. "/" .. creature.killsRequired .. "] " .. creature.raceName .. ".")
                        end
                end
        end
        return true
end 