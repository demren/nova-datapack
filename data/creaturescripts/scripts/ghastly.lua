local monsters = {
	--name = storage
	["ghastly dragon"] = 5612
}

function onKill(cid, target)
	local monster = monsters[getCreatureName(target):lower()]
	if(isPlayer(target) == FALSE and monster and getPlayerStorageValue(cid, 8102) == 2) then
		if getPlayerStorageValue(cid, monster) < 850 then 
			local killedMonsters = getPlayerStorageValue(cid, monster)
            if(killedMonsters == -1) then
                killedMonsters = 1
			end
			setPlayerStorageValue(cid, monster, killedMonsters + 1)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed " .. killedMonsters .. " of 850 ghastly dragons.")
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed enough ghastly dragons. You should talk to Jason about a reward.")
			setPlayerStorageValue(cid, 8102, 3)
		end
	end
	return TRUE
end