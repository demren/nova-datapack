function onAdvance(cid, skill, oldlevel, newlevel)
	local pos = getPlayerPosition(cid)
	local skill_n = skill
	if(skill == SKILL__LEVEL) then
		doWriteLogFile("/usr/forgottenserver/LOG_WBIJANIA_POZIOMOW.txt", getPlayerName(cid).." FROM "..oldlevel.." TO "..newlevel.." IN {x="..pos.x..", y="..pos.y..", z="..pos.z.."} SKILL: LEVEL")
	end
	if math.mod(newlevel, 10) ~= 0 then
		return true
	end
	if math.mod(newlevel, 100) == 0 and skill == SKILL__LEVEL then
		if getPlayerStorageValue(cid, 66118) < newlevel then 
			--doBroadcastMessage("Player " ..getCreatureName(cid) .. " advanced to level " ..newlevel..".",MESSAGE_EVENT_ADVANCE) 
			setPlayerStorageValue(cid, 66118, newlevel)
		end
	end
	if (math.mod(newlevel, 10) == 0 and skill == SKILL__LEVEL) then
		if(getPlayerStorageValue(cid, 66118) < newlevel) then 
			if newlevel <= 50 then
				doPlayerAddItem(cid, 2160, (newlevel <= 20 and 2 or 1))
				local tibia = ""
				if newlevel % 10 == 1 then
					tibia = "st"
				elseif newlevel % 10 == 2 then
					tibia = "nd"
				elseif newlevel % 10 == 3 then
					tibia = "rd"
				else
					tibia = "th"
				end
				doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Congrats! You have reached "..newlevel..""..tibia.." level! As a bonus you will receive "..(newlevel <= 20 and 2 or 1).." cc.")
			end
			setPlayerStorageValue(cid, 66118, newlevel)
		end
		local exp = getPlayerExperience(cid)
		local top = db.storeQuery("SELECT COUNT(`experience`) AS count FROM `players` WHERE `experience` >= '"..exp.."' AND `group_id` < '3';").getDataInt("count")
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You are number "..(top+1).." in top exp ranking.") 
	end
return true
end