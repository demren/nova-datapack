-- writeln by Dragonas
-- config
local szansa = 30 -- szansa w % na zlocenie itemka
local max_ilosc = 3 -- ile moze wypasc maksymalnie itemkow
local ile = 50 -- ile itemkow trzeba zebrac
local id = 8838 -- id itemka
local storage = 9970 -- storage
local monsters = {
	--name = storage
	["bandit"] = 9972
}
function onKill(cid, target)
	local monster = monsters[getCreatureName(target):lower()]
	if(isPlayer(target) == FALSE and monster and getPlayerStorageValue(cid, storage) == 2) then
		if getPlayerStorageValue(cid, monster) < ile then 
			local killedMonsters = getPlayerStorageValue(cid, monster)
            if(killedMonsters == -1) then
                killedMonsters = 0
			end
			rand = math.random(0,100)
			if rand <= szansa then
				rand2 = math.random(1,max_ilosc)
				if killedMonsters >= (ile - max_ilosc) then
					rand2 = 1
				end
				setPlayerStorageValue(cid, monster, killedMonsters + rand2)
				killedMonsters = getPlayerStorageValue(cid, monster)
				if killedMonsters == ile then
					doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have found enough potatoes. Now go to NPC for a reward.")
					doPlayerAddItem(cid, id, rand2)
					setPlayerStorageValue(cid, storage, 3)
				else
					doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have collected " .. killedMonsters .. " of "..ile.." potatoes.")
					doPlayerAddItem(cid, id, rand2)
				end
			else
				doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You haven't found any potatoes.")
			end
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have "..ile.." potatoes, now go to Renqs!")
			setPlayerStorageValue(cid, storage, 3)
		end
	end
	return TRUE
end