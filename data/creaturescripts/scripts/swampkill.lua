local monsters = {
	--name = storage
	["swamp troll"] = 4320
}

function onKill(cid, target)
	local monster = monsters[getCreatureName(target):lower()]
	if(isPlayer(target) == FALSE and monster and getPlayerStorageValue(cid, 4321) == 2) then
		if getPlayerStorageValue(cid, monster) < 50 then 
			local killedMonsters = getPlayerStorageValue(cid, monster)
            if(killedMonsters == -1) then
                killedMonsters = 1
			end
			setPlayerStorageValue(cid, monster, killedMonsters + 1)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed " .. killedMonsters .. " of 50 swamp trolls.")
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed enough swamp trolls. You should talk to Greg about a reward.")
			setPlayerStorageValue(cid, 4321, 3)
		end
	end
	return TRUE
end