local config = {
	idleWarning = 14 * 60 * 1000,
	idleKick = 15 * 60 * 1000
}

function onThink(cid, interval)

	local idleTime = getPlayerIdleTime(cid) + interval
	doPlayerSetIdleTime(cid, idleTime)
	local players = getPlayersOnline()
	if(#players >= 180) then
		if(isAfker(cid)) then
			if(config.idleKick > 0 and idleTime > config.idleKick) then
				doRemoveCreature(cid)
				return true
			end
		end
	end

	if(getTileInfo(getCreaturePosition(cid)).nologout or getCreatureNoMove(cid) or
		getPlayerCustomFlagValue(cid, PLAYERCUSTOMFLAG_ALLOWIDLE)) then
		return true
	end

	if(config.idleKick > 0 and idleTime > config.idleKick) then
		if(not isAfker(cid)) then
			doRemoveCreature(cid)
		end
	elseif(config.idleWarning > 0 and idleTime == config.idleWarning) then
		local message = "You have been idle for " .. math.ceil(config.idleWarning / 60000) .. " minutes"
		if(config.idleKick > 0) then
			message = message .. ", you will be disconnected in "
			local diff = math.ceil((config.idleWarning - config.idleKick) / 60000)
			if(diff > 1) then
				message = message .. diff .. " minutes"
			else
				message = message .. "one minute"
			end

			message = message .. " if you are still idle"
		end

		doPlayerSendTextMessage(cid, MESSAGE_STATUS_WARNING, message .. ".")
	end

	return true
end
