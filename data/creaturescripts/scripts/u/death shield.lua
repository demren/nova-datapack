
		function onStatsChange(cid, attacker, type, combat, value)
		if (not attacker) or (not cid) then return 1 end
		if isMonster(cid) then return 1 end
		if not (type == STATSCHANGE_HEALTHLOSS) then 
		 return 1
		end

		local a = getPlayerSlotItem(cid, 6)
		local b = getPlayerSlotItem(cid, 5)
		local c = getPlayerSlotItem(cid, 4)
		local d = getPlayerSlotItem(cid, 1)
		local e = getPlayerSlotItem(cid, 7)
		local f = getPlayerSlotItem(cid, 8)
		DS = 0
		if a.uid ~= 0 and isShield(a.uid) then
			DS = tonumber(getDS(a.uid)) + DS
			updateTime(a.uid)
		end
		if b.uid ~= 0 and isShield(b.uid) then
			DS = tonumber(getDS(b.uid)) + DS
			updateTime(b.uid)
		end
		if c.uid ~= 0 and isArmor(c) then
		 DS = tonumber(getDS(c.uid)) + DS
		 updateTime(c.uid)
		end
		if d.uid ~= 0 and isArmor(d) then
			DS = tonumber(getDS(d.uid)) + DS
			updateTime(d.uid)
		end
		if e.uid ~= 0 and isArmor(e) then
			DS = tonumber(getDS(e.uid)) + DS
			updateTime(e.uid)
		end
		if f.uid ~= 0 and isArmor(f) then
			DS = tonumber(getDS(f.uid)) + DS
			updateTime(f.uid)
		end

		DS = math.min(DS, MAX_DS)
		local dmg = math.floor(value * (DS / 100))

		if DS == 0 or dmg == 0 then 
		 return 1
		end
		if (not attacker) or (not cid) then
			return 1
		end
		if (attacker==0) or (not cid==0) then
			return 1
		end
		 doTargetCombatHealth(cid, attacker, COMBAT_DEATHDAMAGE, -dmg, -dmg, CONST_ME_MORTAREA)
		return true
		end
		