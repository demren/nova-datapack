local config = {
	message = "You now have 3 minutes to exit this room through the teleporter.",
	timeToRemove = 180, -- seconds
	teleportId = 1387,
	bosses = {
["White dragon"] = { {  x = 4617, y = 4839, z = 8 }, { x = 4661, y = 4850, z = 8 }, { stackpos = 1 } }
}
}
local function removal(position)
	if getThingfromPos(position).itemid == config.teleportId then
		doRemoveItem(getThingfromPos(position).uid)
	end
	return TRUE
end

function onDeath(cid, corpse, killer)
	local position = getCreaturePosition(cid)
	for name, pos in pairs(config.bosses) do
		if name == getCreatureName(cid) then
			teleport = doCreateTeleport(config.teleportId, pos[1], pos[2])
			doSendMagicEffect(pos[2], CONST_ME_TELEPORT)
			doCreatureSay(cid, config.message, TALKTYPE_ORANGE_1)
			addEvent(removal, config.timeToRemove * 1000, pos[2])
		end
	end
	return TRUE
end
