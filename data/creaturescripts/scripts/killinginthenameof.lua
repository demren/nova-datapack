  function onKill(cid, target, lastHit)
local questCreatures =
{
        ["troll"] = 						{questStarted = 1501, questStorage = 65001, creatureStorage = 15001, killsRequired = 30, raceName = "Trolls"},
        ["frost troll"] = 					{questStarted = 1501, questStorage = 65001, creatureStorage = 15002, killsRequired = 30, raceName = "Trolls"},
        ["furious troll"] = 				{questStarted = 1501, questStorage = 65001, creatureStorage = 15003, killsRequired = 30, raceName = "Trolls"},
        ["island troll"] = 					{questStarted = 1501, questStorage = 65001, creatureStorage = 15004, killsRequired = 30, raceName = "Trolls"},
        ["swamp troll"] = 					{questStarted = 1501, questStorage = 65001, creatureStorage = 15005, killsRequired = 30, raceName = "Trolls"},
        ["troll champion"] = 				{questStarted = 1501, questStorage = 65001, creatureStorage = 15006, killsRequired = 30, raceName = "Trolls"},
        ["troll legionnaire"] = 			{questStarted = 1501, questStorage = 65001, creatureStorage = 15007, killsRequired = 30, raceName = "Trolls"},

        ["rotworm"] = 						{questStarted = 1502, questStorage = 65002, creatureStorage = 15008, killsRequired = 50, raceName = "Rotworms"},
        ["carrion worm"] = 					{questStarted = 1502, questStorage = 65002, creatureStorage = 15009, killsRequired = 50, raceName = "Rotworms"},

		["cyclops"] = 						{questStarted = 1503, questStorage = 65003, creatureStorage = 15010, killsRequired = 80, raceName = "Cyclops"},
		["cyclops drone"] = 				{questStarted = 1503, questStorage = 65003, creatureStorage = 15011, killsRequired = 80, raceName = "Cyclops"},
		["cyclops smith"] = 				{questStarted = 1503, questStorage = 65003, creatureStorage = 15012, killsRequired = 80, raceName = "Cyclops"},
		
		["dragon"] = 						{questStarted = 1504, questStorage = 65004, creatureStorage = 15013, killsRequired = 100, raceName = "Dragons"},
		["dragon hatchling"] = 				{questStarted = 1504, questStorage = 65004, creatureStorage = 15014, killsRequired = 100, raceName = "Dragons"},
		["dragon lord"] = 					{questStarted = 1504, questStorage = 65004, creatureStorage = 15015, killsRequired = 100, raceName = "Dragons"},
		["dragon lord hatchling"] = 		{questStarted = 1504, questStorage = 65004, creatureStorage = 15016, killsRequired = 100, raceName = "Dragons"},
		
        ["quara predator scout"] = 			{questStarted = 1505, questStorage = 65005, creatureStorage = 15017, killsRequired = 100, raceName = "Quaras"},
        ["quara constrictor scout"] = 		{questStarted = 1505, questStorage = 65005, creatureStorage = 15018, killsRequired = 100, raceName = "Quaras"},
        ["quara hydromancer scout"] = 		{questStarted = 1505, questStorage = 65005, creatureStorage = 15019, killsRequired = 100, raceName = "Quaras"},
        ["quara mantassin scout"] = 		{questStarted = 1505, questStorage = 65005, creatureStorage = 15020, killsRequired = 100, raceName = "Quaras"},
        ["quara pincher scout"] = 			{questStarted = 1505, questStorage = 65005, creatureStorage = 15021, killsRequired = 100, raceName = "Quaras"},
        ["quara predator"] = 				{questStarted = 1505, questStorage = 65005, creatureStorage = 15022, killsRequired = 100, raceName = "Quaras"},
        ["quara constrictor"] = 			{questStarted = 1505, questStorage = 65005, creatureStorage = 15023, killsRequired = 100, raceName = "Quaras"},
        ["quara hydromancer"] = 			{questStarted = 1505, questStorage = 65005, creatureStorage = 15024, killsRequired = 100, raceName = "Quaras"},
        ["quara mantassin"] = 				{questStarted = 1505, questStorage = 65005, creatureStorage = 15025, killsRequired = 100, raceName = "Quaras"},
        ["quara pincher"] = 				{questStarted = 1505, questStorage = 65005, creatureStorage = 15026, killsRequired = 100, raceName = "Quaras"},

        ["giant spider"] = 					{questStarted = 1506, questStorage = 65006, creatureStorage = 15027, killsRequired = 150, raceName = "Giant Spiders"},

        ["hydra"] = 						{questStarted = 1507, questStorage = 65007, creatureStorage = 15028, killsRequired = 200, raceName = "Hydras"},

        ["serpent spawn"] = 				{questStarted = 1508, questStorage = 65008, creatureStorage = 15029, killsRequired = 200, raceName = "Serpents Spawn"},
		
        ["behemoth"] = 						{questStarted = 1509, questStorage = 65009, creatureStorage = 15030, killsRequired = 300, raceName = "Behemoths"},

        ["medusa"] = 						{questStarted = 1510, questStorage = 65010, creatureStorage = 15031, killsRequired = 250, raceName = "Medusas"},

        ["demon"] = 						{questStarted = 1511, questStorage = 65011, creatureStorage = 15032, killsRequired = 666, raceName = "Demons"}
}

local creature = questCreatures[string.lower(getCreatureName(target))]
        if creature then
                if(isPlayer(target) == true) then
                        return true
                end
                if getPlayerStorageValue(cid, creature.questStarted) == 1 then
                        if getPlayerStorageValue(cid, creature.questStorage) < creature.killsRequired then
                                if getPlayerStorageValue(cid, creature.questStorage) < 0 then
                                        doPlayerSetStorageValue(cid, creature.questStorage, 0)
                                end

                                if getPlayerStorageValue(cid, creature.creatureStorage) < 0 then
                                        doPlayerSetStorageValue(cid, creature.creatureStorage, 0)
                                end
                                doPlayerSetStorageValue(cid, creature.questStorage, getPlayerStorageValue(cid, creature.questStorage) + 1)
                                doPlayerSetStorageValue(cid, creature.creatureStorage, getPlayerStorageValue(cid, creature.creatureStorage) + 1)
                                doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "" .. getPlayerStorageValue(cid, creature.creatureStorage) .. " " .. getCreatureName(target) .. " defeated. Total [" .. getPlayerStorageValue(cid, creature.questStorage) .. "/" .. creature.killsRequired .. "] " .. creature.raceName .. ".")
                        end
                end
        end
        return true
end 