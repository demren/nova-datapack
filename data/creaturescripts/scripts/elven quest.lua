local monsters = {
	--name = storage
	["elf"] = 70011,
	["elf scout"] = 70011,
	["elf arcanist"] = 70011
}

function onKill(cid, target)
	local monster = monsters[getCreatureName(target):lower()]
	if(isPlayer(target) == FALSE and monster and getPlayerStorageValue(cid, 70001) == 2) then
		if getPlayerStorageValue(cid, monster) < 200 then 
			local killedMonsters = getPlayerStorageValue(cid, monster)
            if(killedMonsters == -1) then
                killedMonsters = 1
			end
			setPlayerStorageValue(cid, monster, killedMonsters + 1)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed " .. killedMonsters .. " of 200 elves.")
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed enough elves. You should talk to Hunbar about a reward.")
			setPlayerStorageValue(cid, 70001, 3)
		end
	end
	return TRUE
end