local monsters = {
	--name = storage
	["dragon lord"] = 5012
}

function onKill(cid, target)
	local monster = monsters[getCreatureName(target):lower()]
	if(isPlayer(target) == FALSE and monster and getPlayerStorageValue(cid, 5002) == 2) then
		if getPlayerStorageValue(cid, monster) < 200 then 
			local killedMonsters = getPlayerStorageValue(cid, monster)
            if(killedMonsters == -1) then
                killedMonsters = 1
			end
			setPlayerStorageValue(cid, monster, killedMonsters + 1)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed " .. killedMonsters .. " of 200 dragon lords.")
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed enough dragon lords. You should talk to Martin about a reward.")
			setPlayerStorageValue(cid, 5002, 3)
		end
	end
	return TRUE
end