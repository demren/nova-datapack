local monsters = {
	--name = storage
	["dwarf"] = 70012,
	["dwarf soldier"] = 70012,
	["dwarf guard"] = 70012,
	["dwarf geomancer"] = 70012
}

function onKill(cid, target)
	local monster = monsters[getCreatureName(target):lower()]
	if(isPlayer(target) == FALSE and monster and getPlayerStorageValue(cid, 70002) == 2) then
		if getPlayerStorageValue(cid, monster) < 200 then 
			local killedMonsters = getPlayerStorageValue(cid, monster)
            if(killedMonsters == -1) then
                killedMonsters = 1
			end
			setPlayerStorageValue(cid, monster, killedMonsters + 1)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed " .. killedMonsters .. " of 200 dwarves.")
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed enough dwarves. You should talk to Laurewen about a reward.")
			setPlayerStorageValue(cid, 70002, 3)
		end
	end
	return TRUE
end