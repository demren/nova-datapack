function onKill(cid, target)
if(not isHalloween()) then
	return true
end
--[[
zapytanie:
CREATE TABLE `ots2`.`halloween` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`player_id` INT( 11 ) NOT NULL DEFAULT '0',
`points` INT( 11 ) NOT NULL DEFAULT '0',
PRIMARY KEY ( `id` )
) ENGINE = MYISAM;
--]]
	local monster = string.lower(getCreatureName(target))
	punkty = {['pumpkin'] = true}
	if(not punkty[monster]) then
		return true
	end
	if(not isMonster(target)) then
		return true
	end
	if(not isPlayer(cid)) then
		return true
	end
	if(math.random(1,250) ~= 3 and monster == 'pumpkin') then
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Sorry, maybe next time.")
		return true
	else
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Ohh No! Do not kill more of them!!!!!")
		doCreateMonster("the mutated pumpkin", getCreaturePosition(cid), false)
		doBroadcastMessage("Player "..getPlayerName(cid).." has killed another pumpkin. You cannot kill them. The mutated pumpkin attacks the city to take revenge on players.", MESSAGE_EVENT_ADVANCE)
	end
	return TRUE
end