function onKill(cid, target)
	local monster = string.lower(getCreatureName(target))
	if not isInArray(BOSSES, monster) then
		return true
	end
	if not isMonster(target) then
		return true
	end
	if not isPlayer(cid) then
		return true
	end
	if(string.lower(getCreatureName(target)) == 'bazir') then
		setGlobalStorageValue(8635, 0)
	end
	if(RAID_BOSSES[string.lower(getCreatureName(target))]) then
		for _, r in ipairs(RAIDS) do
			if(r.name == RAID_BOSSES[string.lower(getCreatureName(target))]) then
				if (creatureIsInArray(cid,r.from ,r.to)) then
					setGlobalStorageValue(r.storage_block,0)
				else
				end
				break
			end
		end
	end

	if(db.storeQuery("SELECT count(id) FROM `killers_top_boss` WHERE `player_id` = '"..getPlayerGUID(cid).."' AND `monster` = '"..monster.."'").getDataInt("count(id)") > 0)then
		db.query("UPDATE `killers_top_boss` SET `count` = `count`+1 WHERE `player_id` = '"..getPlayerGUID(cid).."' AND `monster` = '"..monster.."' LIMIT 1 ;")
		local count = db.storeQuery("SELECT `count` FROM `killers_top_boss` WHERE `player_id` = '"..getPlayerGUID(cid).."' AND `monster` = '"..monster.."'").getDataInt("count")
		local tibia=''
		if count % 10 == 1 then
			tibia = "st"
		elseif count % 10 == 2 then
			tibia = "nd"
		elseif count % 10 == 3 then
			tibia = "rd"
		else
			tibia = "th"
		end
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You took a part in killing monster "..monster.." for the "..count..""..tibia.." time!")
	else
		db.query("INSERT INTO `killers_top_boss` (`player_id` ,`monster` ,`count`)VALUES ('"..getPlayerGUID(cid).."', '"..monster.."', '1');")
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You took a part in killing monster "..monster.." for the 1st time!")
	end
	return TRUE
end