local monsters = {
	--name = storage
	["stinking troll"] = 9072,
	["troll champion"] = 9072
}

function onKill(cid, target)
	local monster = monsters[getCreatureName(target):lower()]
	if(isPlayer(target) == FALSE and monster and getPlayerStorageValue(cid, 9070) == 2) then
		if getPlayerStorageValue(cid, monster) < 5 then 
			local killedMonsters = getPlayerStorageValue(cid, monster)
            if(killedMonsters == -1) then
                killedMonsters = 1
			end
			setPlayerStorageValue(cid, monster, killedMonsters + 1)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed " .. killedMonsters .. " of 5 stinking trolls.")
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have killed enough stinking trolls. You should talk to Thyrion about a reward.")
			setPlayerStorageValue(cid, 9070, 3)
		end
	end
	return TRUE
end