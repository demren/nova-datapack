function onKill(cid, target)
	local monster = string.lower(getCreatureName(target))
	if (isMonster(target) and monster == "frank" and getPlayerStorageValue(cid, 12398) == 1 and getPlayerStorageValue(cid, 12399) ~= 1) then
		-- setPlayerStorageValue(cid, 12398, 2)
		setPlayerStorageValue(cid, 12399, 1)
		doPlayerAddItem(cid, 8976, 1)
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Frank is dead now, it means that you are stronger than him. From now on you can summon Hard Zarians and hunt on bigger respawn of Weakened Juggernauts... Frank will probably be reborn...")
	else
		return true
	end
	return TRUE
end