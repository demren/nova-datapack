function onUse(cid, item, fromPosition, itemEx, toPosition)
	local text = getItemText(item.uid)
	if(not (text)) then
		return true
	end
	local t = string.explode(text,';')
	local typ = t[1]
	if(not(t[1]) or not(t[2])) then
		return true
	end
	local proc = tonumber(t[2])
	if(proc > 10) then -- blokada na wszelaki wypadeczq
		return true
	end
	if(not(isNumeric(proc))) then
		return true
	end
	local file = io.open("/var/www/panel_logs/upgrade.txt", "a+")
	local blocked_ids = getBlockedIds()
	if(isInArray(blocked_ids, itemEx.itemid)) then
		doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "This item is an exception. You can't upgrade it. You can upgrade shield with a crystal.")
		return true
	end
	if (isArmor(itemEx) or isShield(itemEx.uid)) == TRUE and (not (isShield(itemEx.uid) and (typ == 'LL' or typ == 'EX' or typ == 'CS'))) then
		getDS(itemEx.uid)
		if typ == 'DS' then
			if(file ~= nil) then
				file:write(getPlayerName(cid)..".ds.10#\n")
			end
			updateTime(itemEx.uid)
			setDS(itemEx.uid, proc)
		elseif typ == 'EX' then
			if(file ~= nil) then
				file:write(getPlayerName(cid)..".ex.10#\n")
			end
			updateTime(itemEx.uid)
			setEX(itemEx.uid, proc)
		else
			doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "You can't upgrade armor with this crystal.")
			return 1
		end
		doRemoveItem(item.uid, 1)
		doSendMagicEffect(toPosition, CONST_ME_MAGIC_BLUE)
	elseif (isWeapon(itemEx.uid) or isBow(itemEx.uid) or isShield(itemEx.uid)) == TRUE then
		getLL(itemEx.uid)
		if typ == 'LL' then
			if(file ~= nil) then
				file:write(getPlayerName(cid)..".ll.10#\n")
			end
			updateTime(itemEx.uid)
			setLL(itemEx.uid, proc)
		elseif typ == 'EX' then
			if(file ~= nil) then
				file:write(getPlayerName(cid)..".ex.10#\n")
			end
			updateTime(itemEx.uid)
			setEX(itemEx.uid, proc)
		elseif typ == 'CS' then
			if(file ~= nil) then
				file:write(getPlayerName(cid)..".cs.10#\n")
			end
			updateTime(itemEx.uid)
			setCS(itemEx.uid, proc)
		else
			doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "You can't upgrade weapon with this crystal.") return 1 end
			doRemoveItem(item.uid, 1)
			doSendMagicEffect(toPosition, CONST_ME_MAGIC_BLUE)
	else
		return 1
	end
	file:close()
return 1
end