local days = 30
local levelToBeVIP = 1

function onUse(cid, item, fromPosition, itemEx, toPosition)
        if getPlayerLevel(cid) >= levelToBeVIP then
                if isPlayerVip(cid) == FALSE then
                        doCreatureSay(cid, "CONGRATULATIONS! You are now a VIP for "..days.." days! You can now enter the VIP-area and use unique features!. ", TALKTYPE_ORANGE_1)
                end
                doPlayerAddVipDays(cid, days+1)
                doRemoveItem(item.uid, 1)
        else
                doPlayerSendCancel(cid, "You need level "..levelToBeVIP.." to be a VIP player.")
        end     
        return TRUE
end