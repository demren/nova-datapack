function onUse(cid, item, frompos, item2, topos)
		local winpos
		local kasa1Pos={x=topos.x-1,y=topos.y,z=topos.z,stackpos=255}
		local kasa1=getThingfromPos(kasa1Pos)
		local kasa2Pos={x=topos.x+1,y=topos.y,z=topos.z,stackpos=255}
		local kasa2=getThingfromPos(kasa2Pos)
		if(math.min(kasa1.itemid,kasa2.itemid,kasa1.type,kasa2.type)==0) then
			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"To start, both players have to put same items in the same amount.")
			return true
		end
		if(kasa1.itemid == kasa2.itemid and kasa1.type == kasa2.type) then
			local type=kasa1.type*2
			local itemid=kasa1.itemid
			doRemoveItem(kasa1.uid,kasa1.type)
			doRemoveItem(kasa2.uid,kasa2.type)
			local los=math.random(1,2)
			if(los == 1) then
				winpos=kasa1Pos
			end
			if(los == 2) then
				winpos=kasa2Pos
			end
			doSendMagicEffect(winpos,35)
			doCreateItem(itemid,type,winpos)
		else
			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"To start, both players have to put same items in the same amount.")
			return true
		end
	return true
end