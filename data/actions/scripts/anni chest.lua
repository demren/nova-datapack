function onUse(cid, item, frompos, item2, topos)
	local function aff(cid, id)
		if getPlayerStorageValue(cid, 8773) ~= 1 then
			doPlayerAddItem(cid, id, 1)
			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You have found a "..getItemNameById(id)..".")
			setPlayerStorageValue(cid, 8773, 1)
			addQuest(cid)
		else
			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "It's empty.")
		end
	return nil
	end
	if item.uid == 5066 then
		aff(cid, 2494)
	elseif item.uid == 5067 then
		aff(cid, 2400)
	elseif item.uid == 5068 then
		aff(cid, 2431)
	elseif item.uid == 5069 then
		aff(cid, 2421)
	end
return true
end