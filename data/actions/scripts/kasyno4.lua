-- Scripted by Emmre --
  -- Edited by Leon Zawodowiec in 75% --

-- 5cc - Szansa 25%
-- 10cc - Szansa 25%
-- 25cc - Szansa 20%
-- 50cc - Szansa 20%


function onUse(cid, item, frompos, item2, topos)
    if(item.uid == 5888)then
        kasapos = {x=518, y=211, z=6, stackpos=255}
        getkasa = getThingfromPos(kasapos)
        winpos = {x=516, y=211, z=6}
        if getkasa.itemid == 2160 and getkasa.type == 5 then
            rand = math.random(1,2)
            if rand == 1 then
                doRemoveItem(getkasa.uid,5)
                doSendMagicEffect(kasapos,46)
                doPlayerSendTextMessage(cid,MESSAGE_STATUS_WARNING,"You have lost 5 Crystal Coins .")
            end
            if rand == 2 then
                doRemoveItem(getkasa.uid,5)
                doCreateItem(2160,10,winpos)
                doSendMagicEffect(winpos,35)
                doSendMagicEffect(frompos,27)
                doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"You have won 5 Crystal Coins .")
            end
        end
        if getkasa.itemid == 2160 and getkasa.type == 10 then
            rand = math.random(1,2)
            if rand == 1 then
                doRemoveItem(getkasa.uid,10)
                doSendMagicEffect(kasapos,46)
                doPlayerSendTextMessage(cid,MESSAGE_STATUS_WARNING,"You have lost 10 Crystal Coins .")
            end
            if rand == 2 then
                doRemoveItem(getkasa.uid,10)
                doCreateItem(2160,20,winpos)
                doSendMagicEffect(winpos,35)
                doSendMagicEffect(frompos,27)
                doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"You have won  10 Crystal Coins .")
            end
        end
        if getkasa.itemid == 2160 and getkasa.type == 25 then
            rand = math.random(1,2)
            if rand == 1 then
                doRemoveItem(getkasa.uid,25)
                doSendMagicEffect(kasapos,46)
                doPlayerSendTextMessage(cid,MESSAGE_STATUS_WARNING,"You have lost 25 Crystal Coins .")
            end
            if rand == 2 then
                doRemoveItem(getkasa.uid,25)
                doCreateItem(2160,50,winpos)
                doSendMagicEffect(winpos,35)
                doSendMagicEffect(frompos,27)
                doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"You have won 20 Crystal Coins  .")
            end
        end
        if getkasa.itemid == 2160 and getkasa.type == 50 then
            rand = math.random(1,2)
            if rand == 1 then
                doRemoveItem(getkasa.uid,50)
                doSendMagicEffect(kasapos,46)
                doPlayerSendTextMessage(cid,MESSAGE_STATUS_WARNING,"You have lost 50 Crystal Coins .")
            end
            if rand == 2 then
                doRemoveItem(getkasa.uid,50)
                doCreateItem(2160,100,winpos)
                doSendMagicEffect(winpos,35)
                doSendMagicEffect(frompos,27)
                doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"You have won  50 Crystal Coins .")
            end
        end
        if getkasa.itemid ~= 2160 then
            doPlayerSendTextMessage(cid,MESSAGE_STATUS_CONSOLE_BLUE,"Put 5, 10, 25 or 50 Crystal Coins if you want to start.")
        end
        if getkasa.itemid == 2160 and getkasa.type ~= 5  or 
           getkasa.itemid == 2160 and getkasa.type ~= 10 or 
           getkasa.itemid == 2160 and getkasa.type ~= 25 or
           getkasa.itemid == 2160 and getkasa.type ~= 50 then
            doPlayerSendTextMessage(cid,MESSAGE_STATUS_CONSOLE_BLUE,"Put 5, 10, 25 or 50 Crystal Coins if you want to start.")
        end 
    end
end  


-- <action uniqueid="5885" script="Casino.lua" />  