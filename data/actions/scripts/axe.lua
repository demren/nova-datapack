function onUse(cid, item, fromPosition, itemEx, toPosition)
	local aids = {35610, 35611, 35612, 35613, 35614}
	for i=1, #aids do
	if(itemEx.actionid == aids[i]) then
			if getPlayerStorageValue(cid, aids[i]) == -1 then
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You destroyed a cocoon.")
				setPlayerStorageValue(cid, 29006, getPlayerStorageValue(cid, 29006) + 1)
				setPlayerStorageValue(cid, aids[i], 0)
				doSummonCreature("Giant Spider", getPlayerPosition(cid))
				doSendMagicEffect(toPosition, CONST_ME_BUGHIT)
			else
				doSendMagicEffect(toPosition, CONST_ME_BLOCKHIT)
			end
	end
	end
end
