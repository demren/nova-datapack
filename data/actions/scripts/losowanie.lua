-- Losowanie
local szansa = 50    -- Procent szans na wygran�
local wygrana = 2    -- Ile razy ilo�� rzetonow si� mno�y przy wygranej. Domy�lnie gracz dostaje 2x wi�cej �eton�w ni� w�o�y�.
function onUse(cid, item, frompos, item2, topos)
if(item.actionid > 100) then
	doSetItemActionId(item.uid,100)
	doSendMagicEffect(frompos,CONST_ME_CRAPS)
	if(math.random(1,100) < szansa) then
		local ileMaDac = math.ceil((item.actionid-100)*wygrana)
		local gdzieTworzy = {x=frompos.x+1, y=frompos.y, z=frompos.z}
		doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"Gratulacje! Wygrales ".. ileMaDac .." zetonow. Idz do Kasjera zeby je wymienic.")
		while(ileMaDac > 100) do
			doCreateItem(2151,100,gdzieTworzy)
			ileMaDac = ileMaDac-100
		end
		if(ileMaDac > 0) then
			doCreateItem(2151,ileMaDac,gdzieTworzy)
			ileMaDac = 0
		end
		doSendMagicEffect(gdzieTworzy,CONST_ME_MAGIC_BLUE)
		doSendMagicEffect(getCreaturePosition(cid),CONST_ME_GIFT_WRAPS)
		doSetItemSpecialDescription(item.uid,"Zawiera 0 zetonow.")
	else
	end
else
	doPlayerSendCancel(cid,"Wrzuc zeton.")
end
return TRUE
end