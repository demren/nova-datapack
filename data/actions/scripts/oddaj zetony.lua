function onUse(cid, item, frompos, item2, topos)
local automat = getThingfromPos({x=frompos.x-1, y=frompos.y, z=frompos.z, stackpos=2})
if(automat.uid > 0 and automat.itemid == 9902) then
	if(automat.actionid-100 > 0) then
		local ileMaDac = automat.actionid-100
		while(ileMaDac > 100) do
			doCreateItem(2151,100,frompos)
			ileMaDac = ileMaDac-100
		end
		if(ileMaDac > 0) then
			doCreateItem(2151,ileMaDac,frompos)
			ileMaDac = 0
		end
		doSetItemActionId(automat.uid,100)
		doSetItemSpecialDescription(automat.uid,"Zawiera 0 zetonow.")
		doSendMagicEffect(frompos,CONST_ME_MAGIC_BLUE)
		doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"Wyjales z automatu ".. automat.actionid-100 .." zetonow.")
	else
		doPlayerSendCancel(cid,"W automacie nie ma juz zetonow.")
	end
else
	return FALSE
end
return TRUE
end