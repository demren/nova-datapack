-- Removing walls with time (By Conde Sapo)

function onUse(cid, item, pos)

pausa = 2000 -- 360 sec.

-- wall positions - don't change stackpos.
wall0pos = {x=307, y=611, z=9, stackpos=1}
wall1pos = {x=307, y=612, z=9, stackpos=1}
wall2pos = {x=345, y=611, z=9, stackpos=1}
wall3pos = {x=345, y=612, z=9, stackpos=1}


if item.itemid == 1945 then
doTransformItem(item.uid,1946)
wall0 = getThingfromPos(wall0pos)
if wall0.itemid ~= 0 then
doRemoveItem(wall0.uid,1)
 	doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"Wall is gone for 6 minutes.")
addEvent(wait1,pausa,wall1pos)
end
else
doTransformItem(item.uid,1945)
end
return 1
end

function wait1(wall1pos)
thing = getThingfromPos(wall1pos)
doRemoveItem(thing.uid,1)
addEvent(wait2,pausa,wall2pos)
end

function wait2(wall2pos)
thing = getThingfromPos(wall2pos)
doRemoveItem(thing.uid,1)
addEvent(wait3,pausa,wall3pos)
end

function wait3(wall3pos)
thing = getThingfromPos(wall3pos)
doRemoveItem(thing.uid,1)
-- if you want more walls , only keep adding addEvent's
end