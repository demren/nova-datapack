local PLANTING_DELAY = 5 --seconds
local EXPLORER_DELAY = 5000 --milliseconds
local MAX_HIT = 200
local MIN_HIT = 70
local PVP = true -- true for PVP, false for Non-PVP
local PLAYERS = {}
function isInArray(table, valor)
for i,j in pairs(table) do
if (j == valor) then
return i
end
end
return 0
end
function explosion(info)
area = {
{0,0,0,0,0},
{0,1,1,1,0},
{0,1,1,1,0},
{0,1,1,1,0},
{0,0,0,0,0},
}
hitpos = {x=info.pos.x, y=info.pos.y, z=info.pos.z, stackpos=253}
center = {}
center.y = math.floor(table.getn(area)/2)+1
for i in ipairs(area) do
center.x = math.floor(table.getn(area[i])/2)+1
for j, v in ipairs(area[i]) do
if (v == 1) then
hitpos.x = info.pos.x + (j - center.x)
hitpos.y = info.pos.y + (i - center.y)
if (getTilePzInfo(hitpos) == 0) then
victim = getThingfromPos(hitpos)
effect = 4
if ((j == center.x) and (i == center.y)) then
hitpoints = MAX_HIT
effect = 5
else
hitpoints = math.random(MIN_HIT,MAX_HIT)
end
if (isPlayer(victim.uid) == 1) then
if (PVP == true) then
doPlayerSendTextMessage(victim.uid,20,"You have lost "..hitpoints.." hitpoints by "..getPlayerName(info.player).."'s plastic bomb")
else
hitpoints = 0
end
end
if (isCreature(victim.uid) == 1) then
doPlayerAddHealth(victim.uid,-hitpoints)
end
doSendMagicEffect(hitpos,effect)
end
end
end
end
PLAYERS[isInArray(PLAYERS, info.player)] = 0
return 1
end
function planting(info)
if info.num == PLANTING_DELAY then
doPlayerSendTextMessage(info.player,MESSAGE_EVENT_ADVANCE,"Plastic Bomb successfully planted!")
addEvent(explosion,EXPLOSION_DELAY,info)
else
info.num = info.num + 1
doPlayerSendTextMessage(info.player,MESSAGE_EVENT_ADVANCE,info.num.."...")
addEvent(planting,1000,info)
end
return 1
end
function onUse(cid, item, frompos, item2, topos)
position = getThingPos(item.uid)
if (getTilePzInfo(position) == 0) then
if (isInArray(PLAYERS, cid) == 0) then
table.insert(PLAYERS, cid)
doSendMagicEffect(frompos,3)
info = {player = cid, pos = position, num = 1}
doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"Planting the bomb...")
doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"1...")
doRemoveItem(item.uid,1)
addEvent(planting,1000,info)
else
doPlayerSendCancel(cid,"You can only plant one bomb at the same time.")
end
else
doPlayerSendCancel(cid,"You can not plant this bomb in a PZ!")
end
return 1
end