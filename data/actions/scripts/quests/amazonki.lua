function onUse(cid, item, frompos, item2, topos)
  	-- Desert q 1
  	if item.uid == 50000 then
  		queststatus = getPlayerStorageValue(cid,50000)
  		if queststatus == -1 then
  			if getPlayerLevel(cid) >= 8 then
  				doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"You have found your reward.")
  				doPlayerAddItem(cid,2214,1)
				doPlayerAddItem(cid,2327,1)
				doPlayerAddItem(cid,2163,1)
  				setPlayerStorageValue(cid,50000,1)
  			else
  				doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"You need level 8 to get prize.")
  			end
  		else
  			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"It is empty.")
  		end
  	end
end