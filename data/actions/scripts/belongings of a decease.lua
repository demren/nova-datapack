function onUse(cid, item, frompos, item2, topos)
	local items = {
-- 1'000 == 100% | 1 == 0.1%
		{2238,50}, -- worn leather boots
		{2237,50}, -- Dirty Cape
		{2115,50}, -- broken piggy bank
		{2695,50}, -- egg
		{5894,50}, -- Bat Wing
		{5902,50}, -- Honeycomb
		{8859,50}, -- Spider Fangs
		{3976,50}, -- worm
		{10606,50}, -- Bunch of Troll Hair
		{2148,50}, -- gp
		{5890,50}, -- Chicken Feather
		{5899,50}, -- Turtle Shell
		{2787,50}, -- White Mushroom
		{2566,50}, -- knife
		{2661,50}, -- scarf
		{2199,50}, -- garlic necklace
		{2143,50}, -- white peaqr
		{2110,50}, -- doll
		{10563,50}, -- Book of Prayers
		{2114,50}, -- Piggy Bank
		{5879,30}, -- spider silk
		{5895,30}, -- fish fin
		{5880,30}, -- Iron Ore
		{13926,10}, -- Plague Bell
		{13925,10}, -- Plague Mask
		{13508,10} -- Slug Drug
	}
	local sum=0
	for _,itm in ipairs(items) do
		sum = sum + itm[2]
	end
	local rand = math.random(1,sum)
	sum = 0
	for _,itm in ipairs(items) do
		sum = sum + itm[2]
		if(rand <=sum) then
			doPlayerAddItem(cid, itm[1], 1)
			doRemoveItem(item.uid)
			return true
		end
	end
	print('CRITICAL ERROR ! ! ! <action itemid="13670" event="script" value="belongings of a decease.lua"/>"')
	return true
end