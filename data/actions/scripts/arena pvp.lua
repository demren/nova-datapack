function onUse(cid, item, frompos, item2, topos)
	local p={p1={x = 428, y = 67, z = 9},p2={x = 430, y = 67, z = 9}}
	local arena = {p1={x = 429, y = 70, z = 11},p2={x = 429, y = 84, z = 11}}
	if(item.uid == 28374) then
		pid1 = getThingfromPos({x=p.p1.x, y=p.p1.y, z=p.p1.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid
		pid2 = getThingfromPos({x=p.p2.x, y=p.p2.y, z=p.p2.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid
	else
		return true
	end
	if(isPlayer(pid1) and isPlayer(pid2)) then
		doTeleportThing(pid1, arena.p1)
		doTeleportThing(pid2, arena.p2)
		doPlayerSendTextMessage(pid1, MESSAGE_STATUS_CONSOLE_BLUE,"Fight!!!")
		doPlayerSendTextMessage(pid2, MESSAGE_STATUS_CONSOLE_BLUE,"Fight!!!")
		doTransformItem(item.uid,item.itemid == 1945 and 1946 or 1945)
	elseif((isPlayer(pid1) and not isPlayer(pid2)) or (not isPlayer(pid1) and isPlayer(pid2))) then
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE,"need two people!!!")
	else
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE,"need two people!!!")
	end
return true
end