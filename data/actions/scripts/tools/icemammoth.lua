function onUse(cid, item, frompos, item2, topos)
local chance1 = math.random(1,3) -- 66%
local chance2 = math.random(1,2) -- 50%
local chance3 = math.random(1,4) -- 25%
local chance4 = math.random(1,6) -- 17%
local chance5 = math.random(1,8) -- 13%
local effect = 3

	if item2.itemid == 7441 then
		if chance1 == 1 or chance1 == 2 then
			doSendMagicEffect(topos,effect)
			doTransformItem(item2.uid,7442)
		else
			doSendMagicEffect(topos,effect)
			doRemoveItem(item2.uid,1)
			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, 'You broke the ice cube.')
		end
	elseif item2.itemid == 7442 then
		if chance2 == 1 then
			doSendMagicEffect(topos,effect)
			doTransformItem(item2.uid,7444)
		else
			doSendMagicEffect(topos,effect)
			doRemoveItem(item2.uid,1)
			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, 'You broke the ice cube.')
		end
	elseif item2.itemid == 7444 then
		if chance3 == 1 then
			doSendMagicEffect(topos,effect)
			doTransformItem(item2.uid,7445)
		else
			doSendMagicEffect(topos,effect)
			doRemoveItem(item2.uid,1)
			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, 'You broke the ice cube.')
		end
	elseif item2.itemid == 7445 then
		if chance4 == 1 then
			doSendMagicEffect(topos,effect)
			doTransformItem(item2.uid,7446)
		else
			doSendMagicEffect(topos,effect)
			doRemoveItem(item2.uid,1)
			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, 'You broke the ice cube.')
		end
		else
			doSendMagicEffect(getPlayerPosition(cid),2)
	end
end