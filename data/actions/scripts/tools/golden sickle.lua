function onUse(cid, item, fromPosition, itemEx, toPosition)
	local aids = {45610, 45611, 45612, 45613, 45614}
	local nowtime = os.time()
	local minutestowait = 4 * 60
	for i=1, #aids do
	if getPlayerStorageValue(cid, aids[i]-100) == 0 then
		setPlayerStorageValue(cid, aids[i], os.time() + (minutestowait * 60))
		setPlayerStorageValue(cid, aids[i]-100, 1)
	end
	if(itemEx.actionid == aids[i]) then
			if os.time() > getPlayerStorageValue(cid, aids[i]) then
				doPlayerAddItem(cid, 11245, 1)
				setPlayerStorageValue(cid, aids[i]-100, 0)
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You have cut a mystic herb.")
			elseif getPlayerStorageValue(cid, aids[i]-100) == -1 then
				doPlayerAddItem(cid, 11245, 1)
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You have cut a mystic herb.")
			else
				doSendMagicEffect(toPosition, CONST_ME_BLOCKHIT)
				doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,  "You can cut mystic herb once for ".. minutestowait / 60 .." hours.")
			end
	end
	end
end
