function onUse(cid, item, frompos, item2, topos)
local random_item = math.random(1,100)
-- szansy
local chain = 40
local brass = 20
local scale = 9
local knight = 5
local dragon = 5
local mpa = 1

if random_item <= chain then
	random_item = 2464 -- chain armor
elseif random_item <= (chain + brass) and random_item > chain then
	random_item = 2465 -- brass armor
elseif random_item <= (chain + brass + scale) and random_item > (chain + brass) then
	random_item = 2483 -- scale armor
elseif random_item <= (chain + brass + scale + knight) and random_item > (chain + brass + scale) then
	random_item = 2476 -- knight armor
elseif random_item <= (chain + brass + scale + knight + dragon) and random_item > (chain + brass + scale + knight) then
	random_item = 2492 -- dragon scale mail
elseif random_item <= (chain + brass + scale + knight + dragon + mpa) and random_item > (chain + brass + scale + knight + dragon) then
	random_item = 2472 -- magic plate armor
else
	random_item = 0
end
	local effect = 3
	if (item2.itemid == 9808) or (item2.itemid == 9809) or (item2.itemid == 9810) then
		if random_item ~= 0 then
			doSendMagicEffect(topos,effect)
			doTransformItem(item2.uid,random_item)
			doRemoveItem(item.uid, 1)
		else
			doSendMagicEffect(topos,effect)
			doRemoveItem(item2.uid,1)
			doRemoveItem(item.uid, 1)
			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, 'You broke the rusty armor.')
		end
	else
			doSendMagicEffect(getPlayerPosition(cid),2)
	end
end