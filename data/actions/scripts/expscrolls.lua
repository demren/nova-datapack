function onUse(cid, item, fromPosition, itemEx, toPosition)
	local mult = 1
	local scrolls = {
	[18517]={50,99,312900,375400},
	[18518]={100,149,588700,987100},
	[18519]={200,299,2453200,4018700},
	[18520]={300,499,5797100,7392100},
	[18521]={500,1500,12029600,14375400}
	}
	if(scrolls[item.itemid]) then
		if(getPlayerLevel(cid) >= scrolls[item.itemid][1] and getPlayerLevel(cid) <= scrolls[item.itemid][2]) then
			doCreatureSay(itemEx.uid, "!!!", TALKTYPE_ORANGE_1, false, tid)
			local Exp = math.random(scrolls[item.itemid][3],scrolls[item.itemid][4])*mult
			doPlayerAddExperience(cid,Exp)
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "You receive "..Exp.." Experience.")
			doRemoveItem(item.uid, 1)
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "This scroll is only for players with level "..scrolls[item.itemid][1].."-"..scrolls[item.itemid][2]..".")
		end
	end
return true
end