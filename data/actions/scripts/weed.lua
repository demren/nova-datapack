function onUse(cid, item, fromPosition, itemEx, toPosition)
	doPlayerAddMana(itemEx.uid, math.ceil(math.random(500, 2500)))
	doCreatureSay(itemEx.uid, "Aahhh :)", TALKTYPE_ORANGE_1, false, tid)
	doRemoveItem(item.uid, 1)
	local sm_id = getPlayerSMsByGUID(getPlayerGUID(cid))
	if(isInArray2(sm_id, 236)) then
		setSMValueplus1(cid,236,1)
	end
	if(isInArray2(sm_id, 336)) then
		setSMValueplus1(cid,336,1)
	end
return true
end