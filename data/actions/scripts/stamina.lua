-- Stamina Restarter by Leon Zawodowiec / Mag Egzorcysta --

function onUse(cid, item, frompos, item2, topos)
local config = {
	staminafull = 42*60,
	effect = 53, 
	text1 = "Stamina has been refilled !", 
	text2 = "Your Stamina is full !" , 
	text3 = "pojebalo ? !report plaz!" 
				}
				
local player = getPlayerPosition(cid)
local playerstamina = getPlayerStamina(cid)
local playerfood = true

	if playerfood ~= 0 then
		if playerstamina ~= config.staminafull then
			setPlayerStamina(cid, config.staminafull)
			doRemoveItem(item.uid, 1)
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,config.text1)
			doSendMagicEffect(player, config.effect)
		else
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,config.text2)
		end
	else
		doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,config.text3)
	end
return true
end