function onUse(cid, item, frompos, item2, topos)
		local function isValueInArray(key, array)
			for k, v in pairs(array) do
				if k == key then
					return true
				end
			end
			return false
		end
	local a = {
				-- storage/uid = items
				[12001] = {{2516, 1}},
				[12002] = {{2414, 1}},
				[12003] = {{, 1}},
				[12004] = {{2160, 10}},
				[12005] = {{2160, 10}},
				[12006] = {{2520, 1}},
				[12007] = {{7730, 1}},
				[12008] = {{2407, 1}},
				[12009] = {{2430, 1}},
				[12010] = {{2476, 1}},
				[12011] = {{, 1}},
				[12012] = {{, 1}},
				[12013] = {{, 1}},
				[12014] = {{, 1}},
				[12015] = {{, 1}},
				[12016] = {{, 1}},
				[12017] = {{, 1}},
				[12018] = {{, 1}},
				[12019] = {{, 1}},

				}
	if not isValueInArray(item.uid, a) then
		return false
	end
	local co = a[item.uid]
	local txt = ""
 	queststatus = getPlayerStorageValue(cid,item.uid)
 	if queststatus == -1 then
		for k, v in ipairs(co) do
			txt = txt..v[2].."x "..getItemNameById(v[1])..(#v ~= k and ", " or " ")
			doPlayerAddItem(cid,v[1],v[2])
		end
		doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"You have found "..txt..".")
		setPlayerStorageValue(cid,item.uid,1)
 	else
		doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"It is empty.")
 	end
	return true
end 