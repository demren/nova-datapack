--Script by Karpio
--Script version: 3.0
function onUse(cid, item, fromPosition, itemEx, toPosition)
--Konfiguracja
local config = {
    uid = 11234, --Uniqueid drzwi
    ile = 1, --ile osob moze byc za drzwiami
    gdzie = "lewo", --w ktora strone przechodzi gracz 
    dir = {["lewo"] = 1, ["prawo"] = 3, ["gora"] = 2, ["dol"] = 0}, --Nie ruszamy
    czas = 90, --Po ilu sekundach nas wyrzuci
    dokad = getCreaturePosition(cid), --Nie ruszamy
    item = getThingPos(item.uid) -- nie ruszamy
    }
--//Konfiguracja
        if(item.uid == config.uid) then
            if(item.actionid == (config.ile + 100)) then
                doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "Only ".. config.ile .." people may be behind doors at the same time.")
                return TRUE
            end
            if(getDirectionTo(getCreaturePosition(cid), toPosition) == config.dir[config.gdzie]) then
                doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You may not get back up to "..timeString(config.czas)..".")
                return TRUE
            end
            if(item.actionid == 0) then
                doSetItemActionId(item.uid, 101)
            else
                doSetItemActionId(item.uid, item.actionid + 1)
            end
            doTransformItem(item.uid, item.itemid + 1)
            doTeleportThing(cid, toPosition, TRUE)
            zacznijCzekac(function()
                czekaj(300)
            if(config.gdzie == "lewo") then
                doMoveCreature(cid, 3)
            elseif(config.gdzie == "prawo") then
                doMoveCreature(cid, 1)
            elseif(config.gdzie == "gora") then
                doMoveCreature(cid, 0)
            elseif(config.gdzie == "dol") then
                doMoveCreature(cid, 2)
            end
                doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You have "..timeString(config.czas)..".")
            if(config.czas > 60) then
                czekaj((config.czas - 60) * 1000)
                doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You have only one minute.")
                czekaj(60 * 1000)
                local item = getThingFromPos(config.item)
                doTeleportThing(cid, config.dokad)
                doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "The end of time.")
                doSendMagicEffect(config.dokad, 10)
                doSetItemActionId(item.uid, item.actionid - 1)
                return TRUE
            end
                czekaj(config.czas * 1000)
                local item = getThingFromPos(config.item)
                doTeleportThing(cid, config.dokad)
                doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "The end of time.")
                doSendMagicEffect(config.dokad, 10)
                doSetItemActionId(item.uid, item.actionid - 1)
                return TRUE
            end)
            return TRUE
        end
    return TRUE
end  