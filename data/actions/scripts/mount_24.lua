function onUse(cid, item, frompos, item2, topos)
	local mount_id = 24
	if not (getPlayerMount(cid, mount_id)) then
		doPlayerAddMount(cid, mount_id)
		doRemoveItem(item.uid, 1)
		doSendMagicEffect(topos, 27)
		soam(cid, "mount shadow draptor")
	else
		doPlayerSendTextMessage(cid,MESSAGE_STATUS_CONSOLE_BLUE,"You already mounted this creature.")
	end
return true
end