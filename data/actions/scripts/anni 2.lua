function onUse(cid, item, frompos, item2, topos)
local iuid = 8772
local from = {x=671, y=277, z=8}
local to = {x=682, y= 277, z=8}
local mob_pos = {
					{x=686, y=277, z=8},
					{x=687, y=277, z=8},
					{x=682, y=275, z=8},
					{x=684, y=275, z=8},
					{x=683, y=279, z=8},
					{x=685, y=279, z=8}
				}
local function wez_pidy(poz)
	tbl = {}
	for i=0, 3 do
		uuid = getThingfromPos({x=poz.x+i, y=poz.y, z=poz.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid
		if isPlayer(uuid) then
			tbl[#tbl+1] = uuid
		else
			return {}
		end
	end
return tbl
end
if item.uid == iuid then
	pidy = wez_pidy(from)
	if getGlobalStorageValue(iuid) > (os.time()) then
		doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"This quest can be done once every day.")
		return true
	end
	if #pidy == 0 then
		doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"You need 4 players to start this quest.")
		return true
	end
	for i, pid in ipairs(pidy) do
		uuid = getThingfromPos({x=to.x+(i-1), y=to.y, z=to.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid
		if isMonster(uuid) then
			doRemoveCreature(uuid)
		end
		doTeleportThing(pid, {x=to.x+(i-1), y=to.y, z=to.z})
		doPlayerSendTextMessage(pid,MESSAGE_EVENT_ADVANCE,"Fight!")
	end
	for _, p in ipairs(mob_pos) do
		uuid = getThingfromPos({x=p.x, y=p.y, z=p.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid
		if isMonster(uuid) then
			doRemoveCreature(uuid)
		end
		doSummonCreature("Orshabaal", p)
	end
	setGlobalStorageValue(iuid, (os.time() + (24*60*60)))
end
return true
end