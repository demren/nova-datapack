function onUse(cid, item, frompos, item2, topos)

	if isInArray({8768, 8769, 8770}, item.uid) then
		queststatus = getPlayerStorageValue(cid,8768)
		if queststatus == -1 then
			doSummonCreature("Orshabaal", getPlayerPosition(cid))
		doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"Fight! If you win I'll give you a weapon.")
			setPlayerStorageValue(cid, 8768, 1)
		elseif queststatus == 1 then
			id = item.uid == 8768 and 7411 or (item.uid == 8769 and 8929 or 2438)
			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"You have found a "..getItemNameById(id)..".")
			doPlayerAddItem(cid,id,1)
			addQuest(cid)
			setPlayerStorageValue(cid,8768,2)
		elseif queststatus == 2 then
			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"It is empty.")
		end
	end
	
return true
end