function onUse(cid, item, frompos, item2, topos)

	if item.uid == 8764 then
		queststatus = getPlayerStorageValue(cid,8764)
		if queststatus == -1 then
			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"You have found a demon shield.")
			doPlayerAddItem(cid,2520,1)
			addQuest(cid)
			setPlayerStorageValue(cid,8764,1)
		else
			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"It is empty.")
		end
	else
		return 0
	end
	return true
end 