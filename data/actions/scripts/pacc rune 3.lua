function onUse(cid, item, fromPosition, itemEx, toPosition)
	if getPlayerPremiumDays(cid) + 3 < 30 then
		doPlayerAddPremiumDays(cid, 3)
		doRemoveItem(item.uid, 1)
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You received 3 days of premium.")
	else
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Now you have " .. getPlayerPremiumDays(cid) .. " premium days. You can\'t have more than 30 premium days")
	return TRUE
	end
end