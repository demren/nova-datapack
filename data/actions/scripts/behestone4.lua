function onUse(cid, item, frompos, item2, topos)
        wall = {x=697, y=192, z=8, stackpos=1}
        getwall = getThingfromPos(wall)

        if item.uid == 3004 and item.itemid == 1945 then
                doRemoveItem(getwall.uid,1)
                doTransformItem(item.uid,item.itemid+1)
        elseif item.uid == 3004 and item.itemid == 1946 then
                doCreateItem(1304,1,wall)
                doTransformItem(item.uid,item.itemid-1)    
		    doSendMagicEffect(wall, 2)     
        else
                doPlayerSendCancel(cid,"Sorry, not possible.")
        end

        return 1
end