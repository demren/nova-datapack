function onUse(cid, item, frompos, item2, topos)
	if(item2.uid == 28000) then -- dostarczanie dusz do skrzyni
		if(item.itemid == 9737) then
		local text = getItemText(item.uid)
			if(text) then
				local dane = {}
				text = string.explode(text, ';')
				for _, inf in pairs(text) do
					if(inf == '') then
					else
						txt = string.explode(inf, '=')
						if(txt[1] ~= 'guid') then
							txt[1] = tonumber(txt[1])
						end
						dane[txt[1]] = tonumber(txt[2])
					end
				end
				if(dane[2]) then
					setPlayerStorageValue(cid, 27999, (getPlayerStorageValue(cid, 27999) == -1 and 0 or getPlayerStorageValue(cid, 27999)) + tonumber(dane[2]))
				end
				if(getPlayerStorageValue(cid, 27999) >=100) then
					if(getPlayerStorageValue(cid, 3473) == 2) then
						setPlayerStorageValue(cid, 3473,3)
					end
				end
				if(dane['guid'] ~= getPlayerGUID(cid)) then
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE,"This container belongs to "..getPlayerNameByGUID(dane['guid'])..". You can't use it")
					return true
				end
				local razem = 0
				for k,v in pairs(dane) do
					if(k ~= 'guid') then
						razem = razem + v*DUSZE[k][4]
					end
				end
				doSetItemText(item.uid, "guid="..getPlayerGUID(cid)..";")
				if(razem > 0) then
					doPlayerAddExperience(cid, razem)
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE,"You will receive "..razem.." exp!")
					doTransformItem(item2.uid, 11402)
					addEvent(doTransformItem, 3000, item2.uid, 11401)
				else
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE,"This container is empty")
				end
			end
		end
		return true
	end
	local bool = false
	-- sprawdzanie id z deda:
	local id_duszy = getItemText(item2.uid)
	if(id_duszy) then
		id_duszy = string.explode(id_duszy,'=')
		if(id_duszy[1] == 'hos') then
			id_duszy = tonumber(id_duszy[2])
		end
	else
		return true
	end
	-- isnije takie w constants?
	if(DUSZE[id_duszy]) then
	else
		return true
	end
	-- czy ded sie nie postarzal:
	for k, dusza_tbl in pairs(DUSZE) do
		if(item2.itemid == dusza_tbl[3]) then
			bool = true
		end
	end
	if(bool == false) then
		return true
	end
	-- start skryptu
	if(item.itemid == 9737) then
		local text = getItemText(item.uid)
		if(text) then
			local dane = {}
			text = string.explode(text, ';')
			for _, inf in pairs(text) do
				if(inf == '') then
				else
					txt = string.explode(inf, '=')
					if(txt[1] ~= 'guid') then
						txt[1] = tonumber(txt[1])
					end
					dane[txt[1]] = txt[2]
				end
			end
			if(tonumber(dane['guid']) == getPlayerGUID(cid)) then
				doSetItemText(item2.uid,'')
				local effect = CONST_ME_GROUNDSHAKER
				if(math.random(0,100) <= DUSZE[id_duszy][2]) then
					if(dane[id_duszy]) then
						dane[id_duszy] = dane[id_duszy]+1
					else
						dane[id_duszy]=1
					end
					local updated_text = ''
					for k,v in pairs(dane) do
						updated_text = updated_text..k..'='..v..';'
					end
					doSetItemText(item.uid,updated_text)
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE,"Bravo, there are already "..dane[id_duszy].." souls of this creature.")
				else
					effect = CONST_ME_POFF
				end
				doSendMagicEffect(topos, effect)
			else
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE,"This container belongs to "..getPlayerNameByGUID(dane['guid'])..". You can't use it")
			end
		end
	end
return true
end