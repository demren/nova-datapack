function onUse(cid, item, fromPosition, itemEx, toPosition)
	if getPlayerStorageValue(cid, 9845) == 1 or getPlayerStorageValue(cid, 9846) >= os.time() then
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You already have full stamina ON.") 
		return true
	end
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Now you will have full stamina for next 24 hours!") 
	setPlayerStorageValue(cid, 9846, os.time()+24*60*60)
	doRemoveItem(item.uid, 1)
	return TRUE
end