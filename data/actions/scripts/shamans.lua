function onUse(cid, item, frompos, item2, topos)
local function odrub_scianki()
	local pozs= {
	{x = 2805, y = 654, z = 9, stackpos=1},
	{x = 2801, y = 649, z = 9, stackpos=1},
	{x = 2802, y = 649, z = 9, stackpos=1},
	{x = 2803, y = 649, z = 9, stackpos=1},
	{x = 2804, y = 649, z = 9, stackpos=1},
	{x = 2804, y = 651, z = 9, stackpos=1},
	{x = 2803, y = 651, z = 9, stackpos=1},
	{x = 2802, y = 651, z = 9, stackpos=1},
	{x = 2801, y = 651, z = 9, stackpos=1},
	{x = 2800, y = 651, z = 9, stackpos=1}}
	local p2 = {
		{x = 2804, y = 650, z = 9},
		{x = 2806, y = 654, z = 9}
	}
	for _,p in ipairs(p2) do
		if(isCreature(getThingfromPos({x=p.x, y=p.y, z=p.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid)) then
			doTeleportThing(getThingfromPos({x=p.x, y=p.y, z=p.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid,{x = 2799, y = 651, z = 9})
		end
	end
	for _,p in ipairs(pozs) do
		if(isCreature(getThingfromPos({x=p.x, y=p.y, z=p.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid)) then
			doTeleportThing(getThingfromPos({x=p.x, y=p.y, z=p.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid,{x = 2799, y = 651, z = 9})
		end
		doTileAddItemEx({x=p.x, y=p.y, z=p.z}, doCreateItemEx(1116,1))
		doItemSetAttribute(4566, "aid", 4573)
	end
	return true
end
pos1 = {x = 2805, y = 654, z = 9, stackpos=1}
pos2 = {x = 2801, y = 649, z = 9, stackpos=1}
pos3 = {x = 2802, y = 649, z = 9, stackpos=1}
pos4 = {x = 2803, y = 649, z = 9, stackpos=1}
pos5 = {x = 2804, y = 649, z = 9, stackpos=1}
pos6 = {x = 2804, y = 651, z = 9, stackpos=1}
pos7 = {x = 2803, y = 651, z = 9, stackpos=1}
pos8 = {x = 2802, y = 651, z = 9, stackpos=1}
pos9 = {x = 2801, y = 651, z = 9, stackpos=1}
pos10 = {x = 2800, y = 651, z = 9, stackpos=1}
mpos1 = {x = 2800, y = 653, z = 9}
mpos2 = {x = 2800, y = 655, z = 9}
mpos3 = {x = 2802, y = 655, z = 9}
mpos4 = {x = 2803, y = 654, z = 9}
mpos5 = {x = 2801, y = 655, z = 9}
getgate1 = getThingfromPos(pos1)
getgate2 = getThingfromPos(pos2)
getgate3 = getThingfromPos(pos3)
getgate4 = getThingfromPos(pos4)
getgate5 = getThingfromPos(pos5)
getgate6 = getThingfromPos(pos6)
getgate7 = getThingfromPos(pos7)
getgate8 = getThingfromPos(pos8)
getgate9 = getThingfromPos(pos9)
getgate10 = getThingfromPos(pos10)
item.uid = 4566
if item.actionid == 4573 and item.itemid == 1945 and getgate1.itemid == 1116 then
doRemoveItem(getgate1.uid,1)
doTransformItem(item.uid,item.itemid+1)
doItemSetAttribute(item.uid, "aid", 4574)
end
if item.actionid == 4574 and item.itemid == 1946 and getgate2.itemid == 1116 then
doRemoveItem(getgate2.uid,1)
    doSummonCreature("Shaman", mpos5)
doTransformItem(item.uid,item.itemid-1)
doItemSetAttribute(item.uid, "aid",  4575)
end
if item.actionid == 4575 and item.itemid == 1945 and getgate3.itemid == 1116 then
doRemoveItem(getgate3.uid,1)
    doSummonCreature("Shaman", mpos3)
doTransformItem(item.uid,item.itemid+1)
doItemSetAttribute(item.uid, "aid", 4576)
end
if item.actionid == 4576 and item.itemid == 1946 and getgate4.itemid == 1116 then
doRemoveItem(getgate4.uid,1)
    doSummonCreature("Basilisk", mpos3)
    doSummonCreature("Shaman", mpos2)
doTransformItem(item.uid,item.itemid-1)
doItemSetAttribute(item.uid, "aid", 4577)
end
if item.actionid == 4577 and item.itemid == 1945 and getgate5.itemid == 1116 then
doRemoveItem(getgate5.uid,1)
    doSummonCreature("Elder Shaman", mpos1)
doTransformItem(item.uid,item.itemid+1)
doItemSetAttribute(item.uid, "aid", 4578)
end
if item.actionid == 4578 and item.itemid == 1946 and getgate6.itemid == 1116 then
doRemoveItem(getgate6.uid,1)
    doSummonCreature("Elder shaman", mpos5)
    doSummonCreature("Shaman", mpos2)
doTransformItem(item.uid,item.itemid-1)
doItemSetAttribute(item.uid, "aid", 4579)
end
if item.actionid == 4579 and item.itemid == 1945 and getgate7.itemid == 1116 then
doRemoveItem(getgate7.uid,1)
    doSummonCreature("Medusa", mpos4)
    doSummonCreature("Elder shaman", mpos2)
    doSummonCreature("Shaman", mpos3)
doTransformItem(item.uid,item.itemid+1)
doItemSetAttribute(item.uid, "aid", 4580)
end
if item.actionid == 4580 and item.itemid == 1946 and getgate8.itemid == 1116 then
doRemoveItem(getgate8.uid,1)
    doSummonCreature("Elder shaman", mpos1)
    doSummonCreature("Elder shaman", mpos2)
    doSummonCreature("Shaman", mpos3)
    doSummonCreature("Medusa", mpos4)
    doSummonCreature("Medusa", mpos5)
doTransformItem(item.uid,item.itemid-1)
doItemSetAttribute(item.uid, "aid", 4581)
end
if item.actionid == 4581 and item.itemid == 1945 and getgate9.itemid == 1116 then
doRemoveItem(getgate9.uid,1)
    doSummonCreature("Elder shaman", mpos1)
    doSummonCreature("Elder shaman", mpos5)
    doSummonCreature("Shaman", mpos3)
    doSummonCreature("Hydra", mpos2)
doTransformItem(item.uid,item.itemid+1)
doItemSetAttribute(item.uid, "aid", 4582)
end
if item.actionid == 4582 and item.itemid == 1946 and getgate10.itemid == 1116 then
	doRemoveItem(getgate10.uid,1)
	if(math.random(1,50) == 1) then
		doSummonCreature("potens flaminis", mpos5)
	else
		doSummonCreature("Elder shaman", mpos5)
		doSummonCreature("Elder shaman", mpos2)
		doSummonCreature("Elder shaman", mpos4)
	end
	doTransformItem(item.uid,item.itemid-1)
	doItemSetAttribute(item.uid, "aid", 4583)
	addEvent(odrub_scianki,3*60*1000)
else
doPlayerSendCancel(cid,"Sorry not possible.")
end
  return 1
  end  