function onUse(cid, item, frompos, item2, topos)
	local pid=0
	for i=-1, 1 do
		for j=-1, 1 do
			local pos = {x=getPlayerPosition(cid).x+i,y=getPlayerPosition(cid).y+j,z=getPlayerPosition(cid).z,stackpos=STACKPOS_TOP_MOVABLE_ITEM_OR_CREATURE}
			doSendMagicEffect(pos,12)
			if(isCreature(getThingFromPos(pos).uid) and pid == 0) then
				if(isMonster(getThingFromPos(pos).uid)) then
					if(getCreatureOutfit(getThingFromPos(pos).uid).lookType == 425) then
						pid=getThingFromPos(pos).uid
					end
				elseif(isPlayer(getThingFromPos(pos).uid)) then
					if(getPlayerStorageValue(getThingFromPos(pos).uid,9295)>os.time()) then
						pid=getThingFromPos(pos).uid
					end
				end
			end
		end
	end
	if(isCreature(pid)) then
		if(getCreatureOutfit(pid).lookType == 425 or (isPlayer(pid) and getPlayerStorageValue(pid,9295)>os.time())) then
			if(isMonster(pid)) then
				doCreatureSay(pid, "Thank You !", TALKTYPE_MONSTER)
				doRemoveCreature(pid)
				doRemoveItem(item.uid)
				setPlayerStorageValue(cid, 9292, (getPlayerStorageValue(cid, 9292)== -1 and 0 or getPlayerStorageValue(cid, 9292))+1)
				doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You already cured "..getPlayerStorageValue(cid, 9292).." citizens from swamp disease. Ottokar will reward you for sure.")
			elseif(isPlayer(pid)) then
				if(getCreatureOutfit(pid).lookType == 425) then
					doSetMonsterOutfit(cid, 'stalker', (getPlayerStorageValue(pid,9295)-os.time())*1000)
				end
				if(not cid == pid) then
					doSendText(getPlayerPosition(pid),"Thank you!")
				end
				setPlayerStorageValue(cid, 9292, (getPlayerStorageValue(cid, 9292)== -1 and 0 or getPlayerStorageValue(cid, 9292))+1)
				doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You already cured "..getPlayerStorageValue(cid, 9292).." citizens from swamp disease. Ottokar will reward you for sure.")
				doPlayerSendTextMessage(pid, MESSAGE_STATUS_CONSOLE_BLUE, "You have been cured.")
				doRemoveItem(item.uid)
				setPlayerStorageValue(pid,9295,0)
			end
		end
	end
	return true
end