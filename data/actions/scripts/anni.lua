function onUse(cid, item, frompos, item2, topos)
local iuid = 8772
local from = {x=1026, y=863, z=12}
local to = {x=1014, y=895, z=12}
local mob_pos = {
					{x=1014, y=893, z=12},
					{x=1016, y=893, z=12},
					{x=1018, y=895, z=12},
					{x=1019, y=895, z=12},
					{x=1015, y=897, z=12},
					{x=1017, y=897, z=12}
				}
local function wez_pidy(poz)
	tbl = {}
	for i=0, 3 do
		uuid = getThingfromPos({x=poz.x+i, y=poz.y, z=poz.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid
		if isPlayer(uuid) then
			tbl[#tbl+1] = uuid
		else
			return {}
		end
	end
return tbl
end
if item.uid == iuid then
	pidy = wez_pidy(from)
	if getGlobalStorageValue(iuid) > (os.time()) then
		doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,"This quest can be done once every hour.")
		return true
	end
	if #pidy == 0 then
		doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"You need 4 players to start this quest.")
		return true
	end
	for i, pid in ipairs(pidy) do
		uuid = getThingfromPos({x=to.x+(i-1), y=to.y, z=to.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid
		if isMonster(uuid) then
			doRemoveCreature(uuid)
		end
		doTeleportThing(pid, {x=to.x+(i-1), y=to.y, z=to.z})
		doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,"Fight!")
	end
	for _, p in ipairs(mob_pos) do
		uuid = getThingfromPos({x=p.x, y=p.y, z=p.z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE}).uid
		if isMonster(uuid) then
			doRemoveCreature(uuid)
		end
		doSummonCreature("demon", p)
	end
	setGlobalStorageValue(iuid, (os.time() + (1*60*60)))
end
return true
end