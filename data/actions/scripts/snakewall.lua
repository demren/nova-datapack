function onUse(cid, item, frompos, item2, topos)
-- Position: [X: ] [Y: 611] [Z: 9].
local x1pos = {x = 2690, y = 650, z = 9}
local npos = {x = 2680, y = 650, z = 9}
local queststatus = getPlayerStorageValue(cid,3891)
if item.uid == 3891 then
		if queststatus < 0 then
			doSummonCreature("the noxious spawn", x1pos)
			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"Fight!")
			setPlayerStorageValue(cid,3891,1)
			addInnyQuest(cid)
		else
			doTeleportThing(cid, npos)
		end
	else
		return 0
	end
return 1
end