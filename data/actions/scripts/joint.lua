local function randomTbl(tbl)
    return tbl[math.random(#tbl)]
end
local combat = createCombatObject()
local arr = {
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
}
local area = createCombatArea(arr)
function callback(cid, pos)
    if(isPlayer(cid) ~= true) then
        stopEvents(CONFIG_JOINT.events)
        return true
    end
    if(math.random(100) < CONFIG_JOINT.chanceToMakeSay) then
        addEvent(doCreatureSay, math.random(100,900), cid, randomTbl(CONFIG_JOINT.frazes), TALKTYPE_ORANGE_1, pos)
    end
    if(math.random(100) < CONFIG_JOINT.chanceToSendEffect) then
        addEvent(doSendMagicEffect, math.random(100,400), pos, randomTbl(CONFIG_JOINT.effects))
    end
    if(math.random(100) < CONFIG_JOINT.chanceToPlaceMonster) then
        local mon = doCreateMonster("monster", pos, FALSE)
        if(isCreature(mon) ~= TRUE) then return false end
        doSetMonsterOutfit(mon, randomTbl(CONFIG_JOINT.monsters), 2500)
        addEvent(doRemoveCreature, math.random(550,2304), mon)
    end
end
setCombatArea(combat, area)
setCombatCallback(combat, CALLBACK_PARAM_TARGETTILE, "callback")
function onUse(cid, item, fromPosition, itemEx, toPosition)
    for i = 1, 10 do
        table.insert(CONFIG_JOINT.events, addEvent(sendCombat, i*1000+i*100, cid, combat, numberToVariant(cid)))
    end
    if(math.random(100) < CONFIG_JOINT.szansa_na_uzaleznienie and isSubordinated(cid) ~= TRUE) then
        setPlayerSubordinate(cid)
        doPlayerSendTextMessage(cid, 20, "Ojj, chyba za duzo trawki! Uwazaj, latwo sie uzaleznic!")
        registerCreatureEvent(cid, "uzaleznienie")
    end
    if(CONFIG_JOINT.usuwac_joint == true) then
        doRemoveItem(item.uid, 1)
    end
    return TRUE
end