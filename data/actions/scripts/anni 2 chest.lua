function onUse(cid, item, frompos, item2, topos)
	local function aff(cid, id)
		if getPlayerStorageValue(cid, 8773) ~= 1 then
			doPlayerAddItem(cid, id, 1)
			addQuest(cid)
			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "You have found a "..getItemNameById(id)..".")
			setPlayerStorageValue(cid, 8773, 1)
		else
			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR, "It's empty.")
		end
	return nil
	end
	if item.uid == 8773 then
		aff(cid, 7390)
	elseif item.uid == 8774 then
		aff(cid, 7434)
	elseif item.uid == 8775 then
		aff(cid, 7429)
	elseif item.uid == 8777 then
		aff(cid, 10167)
	end
return true
end