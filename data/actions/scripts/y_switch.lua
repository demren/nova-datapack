function onUse(cid, item, frompos, item2, topos)
function dPSC(cid,txt) return doPlayerSendCancel(cid, txt) end

local cena = 150
if(isInArray(GROUNDS,item.uid-10000)) then
	cena=150
elseif(isInArray(LADDRES,item.uid-10000)) then
	cena=1000
elseif(isInArray(WALLS,item.uid-10000)) then
	cena=200
end
if(getPlayerMoney(cid) >=cena) then
	doPlayerRemoveMoney(cid, cena)
	newuid = doPlayerAddItem(cid, 12284, 1)
	doItemSetAttribute(newuid, "text", item.uid-10000)
	doTransformItem(item.uid,item.itemid == 1945 and 1946 or 1945)
else
		dPSC(cid, "You don't have enough money. This item costs "..cena.." gp.")
end
return true
end