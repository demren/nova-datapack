-- Script by Leon Zawodowiec --

function onUse(cid, item, fromPosition, itemEx, toPosition)
local cidPosition = getCreaturePosition(cid)
local nick = getCreatureName(cid)
local drzwi = item.itemid
local tp = getThingPos(item.uid)
local gmstatus = getPlayerGroupId(cid)
local player = 2 -- group id kt�re mo�e przj��, ka�de wy�sze te� przejdzie!

if item.itemid == drzwi then
	if gmstatus >= player then
		if cidPosition.x > toPosition.x then
			doTeleportThing(cid, {x=toPosition.x-1,y=toPosition.y,z=toPosition.z}, TRUE)
			doCreatureSetLookDirection(cid, 3)
			doCreatureSay(cid, "Welcome in congress hall, ".. nick ..".", TALKTYPE_ORANGE_1)	
			
		else
			doTeleportThing(cid, {x=toPosition.x+1,y=toPosition.y,z=toPosition.z}, TRUE)
			doCreatureSetLookDirection(cid, 1)			
			doCreatureSay(cid, "Bye, ".. nick ..".", TALKTYPE_ORANGE_1)					
		end
	else
		doCreatureSay(cid, "Only GM's can enter here.", TALKTYPE_ORANGE_1)
		return 0
	end
end
end