function onUse(cid, item, frompos, item2, topos)

local config = {
timeToRemove = 20, -- czas po jakim czasie sciana si� pojawia
cancelmesage = "Przepraszam, Niemozliwe!", -- wiadomo�� gdy nie mo�na postawi� lub usun�� �ciany
sciana = 1546, -- id �ciany kt�ra ma si� pojawia�
gatepos = {x=5084, y=5697, z=13, stackpos=1}, -- pozycia sciany
}

local params = {pos = config.gatepos, id = 1546}
getgate = getThingfromPos(config.gatepos)


if getgate.itemid == config.sciana and item.itemid == 1945 then
doRemoveItem(getgate.uid,1)
doTransformItem(item.uid,1945)

elseif getgate.itemid == 0 and item.itemid == 1945 then
addEvent(doWall, config.timeToRemove * 1000, params)
doTransformItem(item.uid,1946)

elseif getgate.itemid == 0 and item.itemid == 1946 then
doTransformItem(item.uid,1945)
addEvent(doWall, config.timeToRemove * 1000, params)

elseif getgate.itemid == config.sciana and item.itemid == 1946 then
doRemoveItem(getgate.uid,1)
doTransformItem(item.uid,1945)

else
doPlayerSendCancel(cid,config.cancelmesage)
end
return TRUE
end

function doWall(params)
local item = doCreateItem(1546,1,params.pos)
return TRUE
end 