local add = 200
local add2 = 100
function onUse(cid, item, fromPosition, itemEx, toPosition)
		if getPlayerStorageValue(cid, 33456) == 2 then
			doCreatureSay(cid, "You can use this rune only once!", TALKTYPE_ORANGE_1)
		elseif getPlayerStorageValue(cid, 33456) == 1 then
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You received " .. add2 .. " attribute points") 
			setPlayerStorageValue(cid, 66600, getPlayerStorageValue(cid, 66600) + add2)
			setPlayerStorageValue(cid, 33456, 2)
			doRemoveItem(item.uid, 1)
		return TRUE
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You received " .. add .. " attribute points") 
			setPlayerStorageValue(cid, 66600, getPlayerStorageValue(cid, 66600) + add)
			setPlayerStorageValue(cid, 33456, 2)
			doRemoveItem(item.uid, 1)
		return TRUE
		end
end