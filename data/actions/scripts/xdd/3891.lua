function onUse(cid, item, frompos, item2, topos)

local x1pos = {x=245, y=611, z=9}
local npos = {x=236, y=611, z=9}
local queststatus = getPlayerStorageValue(cid,3891)
if item.uid == 3891 then
		if queststatus < 0 then
			doSummonCreature("The Noxious Spawn", x1pos)
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"Fight!")
			addQuest(cid)
			setPlayerStorageValue(cid,3891,1)
		else
			doTeleportThing(cid, npos)
		end
	else
		return 0
	end
return 1
end