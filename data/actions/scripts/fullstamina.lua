function onUse(cid, item, fromPosition, itemEx, toPosition)
	if getPlayerStorageValue(cid, 9845) == 1 then
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Now you will always have full stamina!") 
		return true
	end
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Now you will always have full stamina!") 
	setPlayerStorageValue(cid, 9845, 1)
	doRemoveItem(item.uid, 1)
	return TRUE
end