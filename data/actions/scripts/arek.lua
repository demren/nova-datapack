function onUse(cid, item, frompos, item2, topos)
local monsters = {"basilisk", "basilisk lord", "juggernaut", "weakened juggernaut", "worse defiler", "pharaon boss", "pharaon", "demon", "shaman","elder shaman"}
local id = 7632
	if item.uid == 7659 then
		if getPlayerLevel(cid) >= 150 then
			tim = 7657
			if getPlayerStorageValue(cid, tim) < os.time() then
				if(math.random(1,20) == 1) then
					doSummonCreature("mutated basilisk lord", {x = 2582, y = 656, z = 10})
					doSummonCreature("munster", {x = 478, y = 161, z = 8})
				else
					for i=0, math.random(3,8) do
						local pox = getPlayerPosition(cid)
						doSummonCreature(monsters[math.random(1, #monsters)], {x=pox.x+math.random(0,5), y=pox.y+math.random(-5,5), z=pox.z})
					end
				end
				doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"Fight!")
				if math.random(1,4) == 1 then
					local ile = math.random(2,5)
					minutes = 10
					lvl=getPlayerLevel(cid)
					if(lvl < 200) then
						minutes = 10
					elseif(lvl < 250) then
						minutes = 8
					elseif(lvl < 300) then
						minutes = 7
					elseif(lvl < 350) then
						minutes = 5
					else
						minutes = 3
					end
					setPlayerStorageValue(cid,tim,os.time() + minutes * 60)
					doPlayerAddItem(cid, id, ile)
					doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"Good for you. You earned "..ile.." stone"..(ile > 1 and "s" or "")..".")
				end
			else
				doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"You need to wait "..math.ceil((getPlayerStorageValue(cid, tim) - os.time())/60).." minutes.")
			end
		else
			doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"It's a knight.")
		end
 	else
		doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"It is empty.")
 	end
return true
end 