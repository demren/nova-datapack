local maxTalonow = 1000   -- Ile maksymalnie talon�w mo�na wrzuci� do automatu
local uids = {11000, 11001, 11002, 11003, 11004}  -- UID automat�w
function onUse(cid, item, frompos, item2, topos)
if(frompos.x == 65535) then
	return FALSE
end
local maszyna = getThingfromPos({x=frompos.x-1, y=frompos.y, z=frompos.z, stackpos=2})
if(isInArray(uids, maszyna.uid) == TRUE) then
	if(maszyna.actionid+item.type < maxTalonow+100) then
		doSetItemActionId(maszyna.uid,maszyna.actionid+item.type)
		doSetItemSpecialDescription(maszyna.uid,"Zawiera ".. maszyna.actionid-100+item.type .." zetonow.")
		doPlayerSendTextMessage(cid,MESSAGE_INFO_DESCR,"Wrzuciles do automatu ".. item.type .." zetonow.")
		doRemoveItem(item.uid,item.type)
	else
		doPlayerSendCancel(cid,"Do automatu mozesz wrzucic maksymalnie ".. maxTalonow ..". Nie mozesz juz wrzucic wiecej.")
	end
else
	return FALSE
end
return TRUE
end