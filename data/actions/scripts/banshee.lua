function onUse(cid, item, frompos, item2, topos)

	if item.uid == 5591 then
		queststatus = getPlayerStorageValue(cid,5591)
		if queststatus == -1 then
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"You have found a 100 platinum coins.")
			doPlayerAddItem(cid,2152,100)
			setPlayerStorageValue(cid,5591,1)
		else
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"It is empty.")
		end
	end
	if item.uid == 5592 then
		queststatus = getPlayerStorageValue(cid,5592)
		if queststatus == -1 then
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"You have found a boots of haste.")
			doPlayerAddItem(cid,2195,1)
			setPlayerStorageValue(cid,5592,1)
		else
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"It is empty.")
		end
	end
	if item.uid == 5593 then
		queststatus = getPlayerStorageValue(cid,5593)
		if queststatus == -1 then
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"You have found a giant sword.")
			doPlayerAddItem(cid,2393,1)
			setPlayerStorageValue(cid,5593,1)
		else
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"It is empty.")
		end
	end
	if item.uid == 5594 then
		queststatus = getPlayerStorageValue(cid,5594)
		if queststatus == -1 then
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"You have found a Stone Skin Amulet.")
			doPlayerAddItem(cid,2197,1)
			setPlayerStorageValue(cid,5594,1)
		else
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"It is empty.")
		end
	end
	if item.uid == 5595 then
		queststatus = getPlayerStorageValue(cid,5595)
		if queststatus == -1 then
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"You have found a tower shield.")
			doPlayerAddItem(cid,2528,1)
			setPlayerStorageValue(cid,5595,1)
		else
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"It is empty.")
		end
	end
	if item.uid == 5596 then
		queststatus = getPlayerStorageValue(cid,5596)
		if queststatus == -1 then
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"You have found a Stealth Ring.")
			doPlayerAddItem(cid,2165,1)
			setPlayerStorageValue(cid,5596,1)
		else
			doPlayerSendTextMessage(cid,MESSAGE_EVENT_ADVANCE,"It is empty.")
		end
	else
		return 0
	end

return 1
end 