-- demon helmet by Alfred

function onUse(cid, item, frompos, item2, topos)

   	if item.uid == 1001 then
   		queststatus = getPlayerStorageValue(cid,1001)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,"You have found Demon Helmet.")
   			doPlayerAddItem(cid,2493,1)
			addQuest(cid)
   			setPlayerStorageValue(cid,1001,1)
   		else
   			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,"It is empty.")
   		end
   	elseif item.uid == 1002 then
   		queststatus = getPlayerStorageValue(cid,1002)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,"You have found Steel Boots.")
   			doPlayerAddItem(cid,2645,1)
			addQuest(cid)
   			setPlayerStorageValue(cid,1002,1)
   		else
   			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,"It is empty.")
   		end
   	elseif item.uid == 1003 then
   		queststatus = getPlayerStorageValue(cid,1003)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,"You have found a Demon Shield.")
   			doPlayerAddItem(cid,2520,1)
			addQuest(cid)
   			setPlayerStorageValue(cid,1003,1)
   		else
   			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR,"It is empty.")
   		end
	else
		return 0
   	end

   	return 1
end
