function onSay(cid, words, param)
	local h = getHouseFromPos(getPlayerPosition(cid))
	if(h) then
		local e = getHouseEntry(h)
		doTeleportThing(cid, e)
		doSendMagicEffect(e, CONST_ME_TELEPORT)
	else
		dPSC(cid, "You need to be in your house to use this command.")
	end	
return true
end