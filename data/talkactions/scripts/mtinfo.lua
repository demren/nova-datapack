function onSay(cid, words, param)
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "Say !mt or !minitasks to show your currently tasks to do.")
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "To skip task #1, write: !skip 1 (It costs "..SKIP_POINTS_CONFIG[getPlayerSMLevel(getPlayerGUID(cid))].." points.)")
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "To skip task #2, write: !skip 2 (It costs "..SKIP_POINTS_CONFIG[getPlayerSMLevel(getPlayerGUID(cid))].." points.)")
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "To skip task #3, write: !skip 3 (It costs "..SKIP_POINTS_CONFIG[getPlayerSMLevel(getPlayerGUID(cid))].." points.)")
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "If you'll do all 3 tasks then you'll receive a specials points. For these points you can buy different rewards.")
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "How to change my points for the rewards?")
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "It's easy. Go to our website and open site in right menu called as \"Mini Tasks\" or just open this URL in your browser: "..getConfigValue('url').."?subtopic=minitasks")
	doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "Please note that you must be logged in.")
return TRUE
end