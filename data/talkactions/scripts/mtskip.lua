function onSay(cid, words, param)
	param=tonumber(param)
	if(param ==1 or param ==2 or param==3) then
		local SMs=getPlayerSMsByGUIDinSmartTable(getPlayerGUID(cid))
		local c_SM=getSMs()
		if(SMs[param]['done'] == 0) then
			setExactlyValueOfSM(cid, SMs[param]['sm_id'], 1, c_SM[SMs[param]['sm_id']][2])
			if( SMs[param]['v2'] > 0) then
				setExactlyValueOfSM(cid, SMs[param]['sm_id'], 2, c_SM[SMs[param]['sm_id']][3])
			end
			if( SMs[param]['v3'] > 0) then
				setExactlyValueOfSM(cid, SMs[param]['sm_id'], 3, c_SM[SMs[param]['sm_id']][4])
			end
			local n=math.max(getPlayerSMPoints(getPlayerGUID(cid))-SKIP_POINTS_CONFIG[getPlayerSMLevel(getPlayerGUID(cid))],0)
			db.executeQuery("UPDATE  `players` SET  `sm_points` =  '"..n.."' WHERE  `id` ="..getPlayerGUID(cid).." LIMIT 1 ;")
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Task #"..param.." has been skipped. Your points for now: "..n)
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Task #"..param.." is already done.")
		end
	else
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Wrong parameters. Say: !skip 1, !skip 2 or !skip 3")
	end
return TRUE
end